let express = require('express');
let app = express();

app.use(express.static(__dirname + '/'));

app.get('/', function (req, res) {
  res.send('Development Directory');
});

app.listen(3001, function () {
  console.log('Listening on port 3001!');
});
