require(['model', 'view', 'presenter'], function(_model, _view, _presenter){
    var model = new _model();
    var view = new _view();
    var presenter = new _presenter(view, model);
});
