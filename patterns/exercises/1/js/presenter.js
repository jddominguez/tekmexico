define([], function(){

  var filterPresenter = function(_view, _model){

    var view = _view;
    var model = _model;

    function init(){
      var container = document.getElementById('container');
      container.innerHTML = view.getHtml();
      public.setModel(model.getWords());
      public.computeSearch(view.getSearch())
    }

    var public = {

      setModel: function(){
        view.setModel(model.getWords());
      },

      computeSearch: function(searchfield){
        var pattern;
        searchField.addEventListener('input', function(){
          pattern = searchField.value;
          var re = new RegExp(pattern.replace(/[-[\]{}()*+!<=:?.\/\\^$|#\s,]/g, '\\$&'),'i');
          if(pattern != ''){
            model.resetPositions();
            for (var i = 0; i < model.getWords().length; i++) {
              if(model.getWords()[i].match(re)){
                model.setPositions(i);
              }
            }
            view.setModel(model.getWords(), model.getPositions());
            view.setCount(model.getPositions().length)
          }
          else{
            view.setModel(model.getWords());
            view.setCount(model.getWords().length)
          }
        });
      }
    };

    init();

    return public;
  }

  return filterPresenter;

});
