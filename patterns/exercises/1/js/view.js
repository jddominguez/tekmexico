define([], function () {

  var filterView =  function(){

    var html;

    function initInterface(){
      html = '<h1>Filter</h1><input id="searchField" type="input" name="name" value=""><h2>List (<span id="total">1000</span> cities)</h2><ul id="list"></ul>'
    }

    var public = {
      getHtml: function(){
        return html;
      },

      getPattern: function(){
        return pattern;
      },

      setModel: function(words, positions){
        var list = document.getElementById('list');
        list.innerHTML = '';
        for (var i = 0; i < (positions ? positions.length : words.length); i++) {
          var li = document.createElement('li');
          li.textContent = (positions ? words[positions[i]] : words[i]);
          list.appendChild(li);
        }
      },

      getSearch: function(){
        var searchField = document.getElementById('searchField');
        return searchField;
      },

      setCount(total){
        var count = document.getElementById('total')
        count.textContent = total;
      }

    };

    initInterface();

    return public;
  };

  return filterView;

});
