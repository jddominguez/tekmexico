define([], function () {

  var canvasView =  function(){

    var html;
    var dragging;
    var posX = 0;
    var posY = 0;
    var rect;
    //var currentTool = 'square';

    var public = {
      getHtml: function(){
        return html;
      },

      getControls: function(){
        var controls = document.getElementById('ui-controls');
        return controls;
      },

      paintTool: function(tool){
        var square = document.getElementById('square');
        var circle = document.getElementById('circle');
        var triangle = document.getElementById('triangle');
        square.style.backgroundColor = "#FFF";
        circle.style.backgroundColor = "#FFF";
        triangle.style.backgroundColor = "#FFF";
        if(tool  === "square")
        square.style.backgroundColor = "#999";
        if(tool  === "circle")
        circle.style.backgroundColor = "#999";
        if(tool  === "triangle")
        triangle.style.backgroundColor = "#999";
      },

      getCanvasArea: function(){
        var canvasArea = document.getElementById('canvas');
        return canvasArea;
      },

    };

    function initInterface(){
      html = '<canvas id="canvas" width="640" height="480"></canvas> <div id="ui-controls" class="ui-controls"><div id="square" class="figure-control"><div class="square"></div></div><div id="circle" class="figure-control"><div class="circle"></div></div><div id="triangle" class="figure-control"><div class="triangle"></div></div></div>';
    }

    initInterface();

    return public;

  };

  return canvasView;

});
