define([], function(){

var canvasPresenter = function(_view, _model){

  var view = _view;
  var model = _model;

  function init(){
    var container = document.getElementById('container');
    container.innerHTML = view.getHtml();
    view.paintTool(model.getCurrentTool());
    public.selectControl();
    public.initCanvas();
    console.log("All good!");
  }

  var public = {

    selectControl: function(){
      var controls = view.getControls();
      controls.addEventListener("click", function(e){
        if(e.srcElement.className == 'figure-control')
        model.setCurrentTool(e.srcElement.firstChild.className);
        else
        model.setCurrentTool(e.srcElement.className);
        view.paintTool(model.getCurrentTool());
      });
    },

    initCanvas: function(){
      model.setCanvasArea(view.getCanvasArea());
      model.getCanvasArea().addEventListener("mousedown", public.mouseDownFn);
    },

    mouseDownFn: function(event){

      console.log(model.getAllCanvasShape());

      //not for right click!
      if(event.which === 3){
        return false;
      }
      model.setHighIndex(-1);
      var rect = this.getBoundingClientRect();
      model.setMouse((event.clientX - rect.left) * (this.width / rect.width), (event.clientY - rect.top) * (this.height / rect.height));
      for (i = 0; i < model.getCanvasShapeSize(); i++)
        if(public.hitTest(model.getCanvasShape(i), model.getMouse(0), model.getMouse(1))) {
          model.setHit(true);
          model.setDrag(true);
          if (i > model.gethighIndex()) {
            model.setDragHold(model.getMouse(0) - model.getCanvasShape(i).x, model.getMouse(1) - model.getCanvasShape(i).y)
            model.setHighIndex(i);
            model.setDragIndex(i);
          }
        }

      if(model.getHit() === false){
        model.setDrag(false);
        public.createFigure(model.getCurrentTool(), model.getCanvasArea(), model.getMouse(0), model.getMouse(1));
      }

      model.setHit(false);

      if (model.getDrag() && model.getCanvasShapeSize() != 0) {
        window.addEventListener("mousemove", public.mouseMoveFn, false);
      }
      model.getCanvasArea().removeEventListener("mousedown", public.mouseDownFn, false);
      window.addEventListener("mouseup", public.mouseUpFn, false);

      Paint();

    },


    mouseMoveFn: function(event) {

      var posX;
      var posY;

      var shapeRad = model.getCanvasShape(model.getDragIndex()).rad;
      var minX = shapeRad;
      var maxX = model.getCanvasArea.width - shapeRad;
      var minY = shapeRad;
      var maxY = model.getCanvasArea.height - shapeRad;

      //getting mouse position correctly
      var bRect = model.getCanvasArea().getBoundingClientRect();
      mouseX = (event.clientX - bRect.left)*(model.getCanvasArea().width/bRect.width);
      mouseY = (event.clientY - bRect.top)*(model.getCanvasArea().height/bRect.height);

      model.setMouse(mouseX, mouseY);

      model.setPos(model.getMouse(0) - model.getDragHold(0), model.getMouse(1) - model.getDragHold(1));

      // var x = (model.getPos(0) < minX) ? minX : ((model.getPos(0) > maxX) ? maxX : model.getPos(0));
      // var y = (model.getPos(1) < minY) ? minY : ((model.getPos(1) > maxY ) ? maxY : model.getPos(1));
      //
      // model.setPos(x, y);

      model.updateCanvasShape(model.getDragIndex(), model.getPos(0), model.getPos(1));

      Paint();

    },

    mouseUpFn: function(event) {
      model.getCanvasArea().addEventListener("mousedown", public.mouseDownFn, false);
      window.removeEventListener("mouseup", public.mouseUpFn, false);
      if (model.getDrag()) {
        model.setDrag(false);
        window.removeEventListener("mousemove", public.mouseMoveFn, false);
      }
    },

    hitTest: function(shape, mx, my) {

      if(shape.type === 'circle'){
        var dx = mx - shape.x;
        var dy = my - shape.y;
        return (dx * dx + dy * dy < shape.rad * shape.rad);
      }

      if(shape.type === 'square'){
        var dx = mx - shape.x;
        var dy = my - shape.y;
        console.log(dx + ", " + dy);
        var d = shape.side/2;
        return (dx < d && dy < d && dx > -d && dy > -d);
      }

      if(shape.type === 'triangle')
        return false;

    },

    createFigure: function(type, context, x, y){
      var figure = new Figure(type, x, y);
      model.addCanvasShape(figure);
      Paint();
    },


  };

  var Figure = function(type, x, y){
    this.type = type;
    this.x = x;
    this.y = y;
    if (type === 'circle')
      this.rad = 30;
    if (type === 'square')
      this.side = 60;
  };

  function Paint(){
    var canvas = model.getCanvasArea();
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    for (i = 0; i < model.getCanvasShapeSize(); i++) {
      var currentShape = model.getCanvasShape(i);
      Shape(currentShape.type, currentShape.context, currentShape.x, currentShape.y);
    }
  }

  function Shape(type, context, x, y){
    this.type = type;
    var context = canvas.getContext("2d");

    this.x = x;
    this.y = y;

    context.fillStyle = "#000";

    if (this.type === undefined) {
      return false;
    }

    if (this.type === "square") {
      context.fillRect(x-30, y-30, 60, 60);
    }

    if (this.type === "circle") {
      context.beginPath();
      context.arc(x, y, 30, 0, 2 * Math.PI, false);
      context.closePath();
      context.fill();
    }

    if (type === "triangle"){
      context.beginPath();
      context.moveTo(x-10, y-40);
      context.lineTo(x - 40, y + 10);
      context.lineTo(x + 35, y + 20);
      context.closePath();
      context.fill();
      // var ctx = context;
      // var side = 100;
      // var h = side * (Math.sqrt(3)/2);
      //
      // ctx.translate(x-100, y);
      //
      // ctx.beginPath();
      //
      // ctx.moveTo(0, -h / 2);
      // ctx.lineTo( -side / 2, h / 2);
      // ctx.lineTo(side / 2, h / 2);
      // ctx.lineTo(0, -h / 2);
      //
      // ctx.stroke();
      // ctx.fill();


    }
  }

  init();

  return public;
};

return canvasPresenter;

});
