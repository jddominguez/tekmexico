define([], function () {

  var canvasModel = function (){

  var currentTool = 'triangle';
  var canvasShapes = [];
  var canvasArea;
  var context;
  var dragIndex;
  var highIndex = -1;
  var dragging = false;
  var mouse = [];
  var dragHold = [];
  var pos = [];
  var hit = false;


  var public = {

    setHit: function(val){
      hit = val;
    },

    getHit: function(){
      return hit;
    },

    setHighIndex: function(val){
      highIndex = val;
    },

    gethighIndex: function(){
      return highIndex;
    },

    getCurrentTool:  function(){
      return currentTool;
    },

    setCurrentTool: function(tool){
      currentTool = tool;
    },

    addCanvasShape: function(figure){
      canvasShapes.push(figure);
    },

    updateCanvasShape: function(figure, x, y){
      canvasShapes[figure].x = x;
      canvasShapes[figure].y = y;
    },

    getAllCanvasShape: function(){
      return canvasShapes;
    },

    getCanvasShape: function(i){
      return canvasShapes[i];
    },

    getCanvasShapeSize: function(){
      return canvasShapes.length;
    },

    getDrag: function(){
      return dragging;
    },

    setDrag: function(val){
      dragging = val;
    },

    getCanvasArea: function(){
      return canvasArea;
    },

    setCanvasArea: function(val){
      canvasArea = val;
    },

    setDragHold: function(x,y){
      dragHold[0] = x;
      dragHold[1] = y;
    },

    getDragHold: function(n){
      if(n === 0)
        return dragHold[0];
      if(n === 1)
        return dragHold[1];
      else
        return false;
    },

    setMouse: function(x, y){
      mouse[0] = x;
      mouse[1] = y;
    },

    getMouse: function(n){
      if (n === 0 )
        return mouse[0];
      if (n === 1)
        return mouse[1];
      else
        return false;
    },

    setPos: function(x, y){
      pos[0] = x;
      pos[1] = y;
    },

    getPos: function(n){
      if (n === 0 )
        return pos[0];
      if (n === 1)
        return pos[1];
      else
        return false;
    },

    setDragIndex: function(n){
      dragIndex = n;
    },

    getDragIndex: function(){
      return dragIndex;
    }


  };

  return public;

};

return canvasModel;

});
