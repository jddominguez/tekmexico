define([], function () {

  var editorPresenter = function(_view, _model){

    var view = _view;
    var model = _model;

    function init(){
        var container = document.getElementById('container');
        var controls = document.getElementById('controls');
        container.innerHTML = view.getHtml();
        controls.innerHTML = view.getUi();
        loadSquares();
        public.setControls();
        public.undoRedo();
        public.macroTools();
        console.log("All good!");
    }

    var public = {

      updateEditor: function(macro){
        var editor = view.getEditor();
        var blocks = editor.getElementsByTagName("div");

        if(model.getMacro().length != 0 && macro){
          var j = 0;
          var props = model.getMacro().length;
          var history = model.getMacro();
        }
        else{
          var j = model.getPos();
          var history = model.getHistoryComplete();
          console.log(history);
        }

        for (; j < model.getPos()+1; j++) {

          if(history[j]["borderColor"])
            for (var i = 0; i < blocks.length; i++)
              blocks[i].style.borderColor = history[j]["borderColor"];

          if(history[j]["borderWidth"])
            for (var i = 0; i < blocks.length; i++)
              blocks[i].style.borderWidth = history[j]["borderWidth"] + "px";

          if(history[j]["backgroundColor"])
            for (var i = 0; i < blocks.length; i++)
              blocks[i].style.backgroundColor = history[j]["backgroundColor"];

          if(history[j]["roundEdges"])
            for (var i = 0; i < blocks.length; i++)
              blocks[i].style.borderRadius = history[j]["roundEdges"] + "px";

        }

      },

      macroTools: function(){

        var tools = view.getMacroTools();

        tools[0].addEventListener("click", function(){
          model.setRecording(true);
          public.macroToolsStatus();
        });

        tools[1].addEventListener("click", function(){
          model.setMacro(model.getHistoryComplete(), model.getPos());
          model.setRecording(false);
          public.macroToolsStatus();
        });

        tools[2].addEventListener("click", function(){
          console.log("reset!");
          model.resetMacro();
          model.setRecording(false);
          public.macroToolsStatus();
        });

        tools[3].addEventListener("click", function(){
          console.log("play!");
          public.updateEditor(true);
          public.macroToolsStatus();
        });

      },

      macroToolsStatus: function(){
        var tools = view.getMacroTools();


        if(model.getRecording()){
          tools[0].disabled = true;
          tools[1].disabled = false;
          tools[2].disabled = true;
          tools[3].disabled = true;
        }
        else{
          if(model.getMacro().length === 0){
            tools[0].disabled = false;
            tools[3].disabled = true;
          }
          else{
            tools[0].disabled = true;
            tools[3].disabled = false;
          }
          tools[1].disabled = true;
          tools[2].disabled = false;
        }
      },

      undoRedo: function(){
        var buttons = view.getUndoRedoButtons();
        var tools = view.getTools();

        //UNDO BUTTON
        buttons[0].addEventListener("click", function(){
          console.log("undo!");
          model.decPos();
          var property =  model.getHistory(model.getPos());
          //console.log(property);
          if(property["borderColor"])
            tools[0].value = model.getHistory(model.getPos())["borderColor"];
          if(property["borderWidth"])
            tools[1].value = model.getHistory(model.getPos())["borderWidth"];
          if(property["backgroundColor"])
            tools[2].value = model.getHistory(model.getPos())["backgroundColor"];
          if(property["roundEdges"])
            tools[3].value = model.getHistory(model.getPos())["roundEdges"];
          public.updateHistory();
          console.log(model.getPos());
          public.updateEditor(false);
        });

        //REDO BUTTON
        buttons[1].addEventListener("click", function(){
          console.log("redo!");
          model.incPos();
          var property =  model.getHistory(model.getPos());
          if(property["borderColor"])
            tools[0].value = model.getHistory(model.getPos())["borderColor"];
          if(property["borderWidth"])
            tools[1].value = model.getHistory(model.getPos())["borderWidth"];
          if(property["backgroundColor"])
            tools[2].value = model.getHistory(model.getPos())["backgroundColor"];
          if(property["roundEdges"])
            tools[3].value = model.getHistory(model.getPos())["roundEdges"];
          public.updateHistory();
          public.updateEditor(false);
        });
      },

      setControls: function(){
        var tools = view.getTools();
        var buttons = view.getUndoRedoButtons();

        buttons[0].disabled = true;
        buttons[1].disabled = true;

        var currentValue;

        //BORDER COLOR PICKER
        tools[0].addEventListener("mousedown", function(){
          if(model.getFirstOfTool(0))
            currentValue = this.value;
        });
        tools[0].addEventListener("change", function(){
          if(model.getFirstOfTool(0))
              model.saveHistory({ "borderColor" : currentValue });
          model.setFirstOfTool(0,false);
          model.saveHistory({ "borderColor" : this.value });
          public.updateHistory();
          public.updateEditor(false);
        });

        //BORDER WIDTH SLIDER
        tools[1].addEventListener("mousedown", function(){
          if(model.getFirstOfTool(1))
            currentValue = this.value;
        });
        tools[1].addEventListener("change", function(){
          if(model.getFirstOfTool(1))
            model.saveHistory({ "borderWidth" : currentValue });
          model.setFirstOfTool(1, false);
          model.saveHistory({ "borderWidth" : this.value });
          public.updateHistory();
          public.updateEditor(false);
        });

        //BACKGROUND COLOR
        tools[2].addEventListener("mousedown", function(){
          if(model.getFirstOfTool(2))
            currentValue = this.value;
        });
        tools[2].addEventListener("change", function(){
          if(model.getFirstOfTool(2))
              model.saveHistory({ "backgroundColor" : currentValue });
          model.setFirstOfTool(2,false);
          model.saveHistory({ "backgroundColor" : this.value });
          public.updateHistory();
          public.updateEditor(false);
        });

        //BORDER RADIUS
        tools[3].addEventListener("mousedown", function(){
          if(model.getFirstOfTool(3))
            currentValue = this.value;
        });
        tools[3].addEventListener("change", function(){
          if(model.getFirstOfTool(3))
              model.saveHistory({ "roundEdges" : currentValue });
          model.setFirstOfTool(3,false);
          model.saveHistory({ "roundEdges" : this.value });
          public.updateHistory();
          public.updateEditor(false);
        });

      },


      //UPDATE UNDO REDO BUTTONS
      updateHistory: function(){
        var buttons = view.getUndoRedoButtons();
        if(model.getPos() == 0)
          buttons[0].disabled = true;

        if(model.getPos() > 0)
          buttons[0].disabled = false;

        if(model.getPos() < model.getHistoryAll() - 1)
            buttons[1].disabled = false;

        if(model.getPos() == model.getHistoryAll() - 1)
          buttons[1].disabled = true;
      }
    };

    function dragDrop(drag, drop){
      var drag = document.getElementById(drag);
      var drop = document.getElementById(drop);
      drag.ondragstart = function(e){
        e.dataTransfer.setData("content", e.target.id);
        console.log("Drag Start!");
      }
      drop.ondragover = function(e){
        e.preventDefault();
        console.log("moving!");
      }
      drop.ondrop = function(e){
        var id = e.dataTransfer.getData("content");
        e.target.appendChild(document.getElementById(id));
        console.log("Drag end!");
      }
    }

    function loadSquares(){
      dragDrop("square-1", "editor");
      dragDrop("square-1", "container");
      dragDrop("square-2", "editor");
      dragDrop("square-2", "container");
      dragDrop("square-3", "editor");
      dragDrop("square-3", "container");
    }

    init();

    return public;

  };

  return editorPresenter;

});
