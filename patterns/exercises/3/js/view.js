define([], function () {

  var editorView = function(){

    var html;
    var ui;

    function init(){
      html = '<div id="square-1" class="block" draggable="true"></div><div id="square-2" class="block" draggable="true"></div><div id="square-3" class="block" draggable="true"></div>';
      ui = '<input id="borderC" class="control" type="color" name="">'+
      '<br><input id="borderT" type="range" class="control" name="" min="1" max="20" value="0">'+
      '<br><input id="backgroundC" class="control" type="color" name="">'+
      '<br><input id="roundE" type="range" class="control" name="" min="0" max="20" value="0">'+
      '<hr>'+
      '<br><input id="undo" type="button" name="name" value="Undo"> <input id="redo" type="button" name="name" value="Redo">'+
      '<br>'+
      '<br><input id="record" type="button" name="name" value="Record"> <input id="stop" type="button" name="name" value="Stop" disabled> <input id="reset" type="button" name="name" value="Reset"> <input id="play" type="button" name="name" value="Play" disabled>';
    }

    init();

    var public = {
      getHtml: function(){
        return html;
      },
      getUi: function(){
        return ui;
      },

      getTools: function(){
        var tools = [];
        tools.push(document.getElementById('borderC'));
        tools.push(document.getElementById('borderT'));
        tools.push(document.getElementById('backgroundC'));
        tools.push(document.getElementById('roundE'));
        return tools;
      },

      getMacroTools: function(){
        var tools = [];
        tools.push(document.getElementById('record'))
        tools.push(document.getElementById('stop'))
        tools.push(document.getElementById('reset'))
        tools.push(document.getElementById('play'))
        return tools;
      },

      getEditor: function(){
        var editor = document.getElementById('editor')
        return editor;
      },

      getUndoRedoButtons: function(){
        var tools = [];
        tools.push(document.getElementById('undo'));
        tools.push(document.getElementById('redo'));
        return tools;
      }

    };

    return public;

  };

  return editorView;

});
