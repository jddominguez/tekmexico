define([], function () {

  var editorModel = function (){

    var macro = [];
    var history = [];
    var firstOfTool = [true, true, true, true];
    var recording = false;

    var pos = 0;

    var public = {

      getRecording: function(){
        return recording;
      },

      setRecording: function(val){
        recording = val;
      },

      getPos: function(){
        return pos;
      },

      decPos: function(){
        pos = pos - 1;
      },

      incPos: function(){
        pos = pos + 1;
      },

      setMacro: function(val, pos){
        for (var i = 0; i <= pos; i++) {
          macro.push(val[i])
        }
      },

      getMacro: function(){
        return macro;
      },

      resetMacro: function(){
        macro = [];
      },

      saveHistory: function(action){
        history.push(action);
        pos = history.length-1;
        console.log(history);
      },

      getHistory: function(n){
        return history[n];
      },

      getHistoryAll: function(){
        return history.length;
      },

      getHistoryComplete: function(){
        return history;
      },


      getFirstOfTool: function(n){
        return firstOfTool[n];
      },

      setFirstOfTool: function(n, val){
        firstOfTool[n] = val;
      },



    };

    return public;

  };

  return editorModel;

});
