var log = (function() {
  'use strict';

  var instance;

  log = function(args) {
    if (instance) {
      return instance;
    }
    instance = this;
    console.log("Connection started.");
  };


  return log;
}());


new log();
log();
