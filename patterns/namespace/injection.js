var myApp = myApp || {};

myApp.utils = {};

(function(){

  var val = 5;

  this.getValue = function(){
    return val;
  }

  this.setValue = function(newVal){
    val = newVal;
  }

  this.tools = {};

}).apply(myApp.utils);

(function(){
  this.diagnose = function(){
    return "diagnose";
  }
}).apply(myApp.utils.tools);


console.log(myApp.utils.getValue());
myApp.utils.setValue(25);
console.log(myApp.utils.getValue());

console.log(myApp.utils.tools.diagnose());


var ns = ns || {},
    ns2 = ns2 || {};

var creator = function(val){
  var val = val || 0;
  this.next = function(){
    return val++;
  }
  this.reset = function(){
    val = 0;
  }
}

creator.call(ns);
creator.call(ns2, 500);
