var myApp = {

  getInfo: function(){

  },

  models: {

  },

  views: {
    pages: {}
  },

  collections: {

  }
};

myApp.foo = function(){
  return "bar";
};


myApp.utils = {
  toString: function(){

  },
  export: function(){

  }
};


var myApp = {};

myApp || (myApp = {});



function foo(){
  myApp || (myApp = {});
}

foo();

function foo(myApp){
  myApp || (myApp = {});
}

foo();





var namespace = (function(){


  var privateMethod1 = function(){},
      privateMethod2 = function(){},
      privateProperty1 = "foobar";

      return{

        publicMethod1: privateProperty1,

        properties: {
          publicProperty1: privateProperty1
        },

        utils:{
          publicMethod2: privateMethod2
        }

      }
})();



myConfig = {
  language: "english",
  default: {
    enableLocation: true,
    enableSharing: false,
    maxPhotos: 20
  },
  theme: {
    skin: "a",
    toolbars:{
      index: "ui-navigation-toolbar",
      pages: "ui-custom-toolbar"
    }
  }
};
