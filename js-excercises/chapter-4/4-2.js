function sum(n){
  if(n.length == 1) //If we only have one element, we return it.
    return n[0];
  else
    return n[0] + sum(n.slice(1)); //We call sum() but using the slice method on the array, we iterate adding the next element.
}
console.log(sum([1,2,3,4]));

//Without using array methods.
function sum(arr){
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] instanceof Array)
      sum += sum(arr[i]);
    sum += arr[i];
  }
  return sum;
}
console.log(sum([1,2,3,4]));
