function dataType(){
  for (var i = 0; i < arguments.length; i++) {
    element = typeof(arguments[i]);
    if(arguments[i] instanceof Array)
      console.log("array");
    if(element === 'number'){
      if(arguments[i] % 1 == 0)
      console.log("integer");
      else
      console.log("float");
    }
    else
    console.log(element);
  }
}

dataType(
  1,
  2.1,
  Infinity,
  "2*1",
  [function(){}, 1],
  function(){},
  "Hello",
  ["a","b"],
  [1,2,3,4],
  {a: 1, b: 2},
  {},
  function test(){},
  (function(){}),
  undefined,
  null,
  false,
  true
);
