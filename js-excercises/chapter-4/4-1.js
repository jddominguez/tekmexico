function fib(n){
  if(n <= 2) //If passed param is less or equal than "2" we just return "1".
    return 1;
  else
    return fib(n-1) + fib(n-2);
}
console.log(fib(5));
