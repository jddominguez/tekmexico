function Animal(age){
  if(typeof(age) === 'number' && age % 1 === 0 && age != 0)
    this.age = age;
  else{
    this.age = 0;
    console.log("Error, you must specify an age.");
  }
}

var dog = new Animal(2);
console.log(dog.age);
