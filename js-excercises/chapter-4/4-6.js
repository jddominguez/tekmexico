 function distance(){

   var i = arguments.length;

   function point(a, b){
     return Math.pow((a - b), 2);
   }

   point(arguments[2], arguments[0])


  if(i == 4){
    var d = Math.sqrt(point(arguments[2], arguments[0]) + point(arguments[3], arguments[1]));
    console.log(d);
  }
  else if(i == 6){
    var d = Math.sqrt(point(arguments[3], arguments[0]) + point(arguments[4], arguments[1]) + point(arguments[5], arguments[2]));
    console.log(d);
  }
  else if( (typeof(arguments[0]) && typeof(arguments[1])) == 'object' && arguments[0].length == arguments[1].length && arguments.length == 2){
    var sum = 0;
    for (var i = 0; i < arguments[0].length; i++) {
      sum = Math.pow((arguments[1][i] - arguments[0][i]), 2) + sum;
    }
    console.log(Math.sqrt(sum));
  }
  else
    console.log("Error, we need at least two points Eg: distance(x1, y1, x2, y2, ...) or distance([1, 2], [1, ...)");
}

var x1 = 1, y1 = 2, z1 = 1;
var x2 = 2, y2 = 2, z2 = 4;

var dot1 = [1, 2, 1];
var dot2 = [2, 2, 4];

distance(x1, y1, z1, x2, y2, z2);
distance(dot1, dot2);
