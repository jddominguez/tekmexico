//Using setters and getters
var dog = {
  innerAge: undefined,
  set ["age"](v) {
    if(typeof(v) === 'number' && v % 1 === 0 && v != 0)
      this.innerAge = v;
    else
      this.innerAge = this.innerAge;
  },
  get ["age"]() {
    return this.innerAge;
  }
}

dog.age = 10;
console.log(dog.age);

dog.age = [1];
console.log(dog.age);

dog.age = "1";
console.log(dog.age);

dog.age = "A";
console.log(dog.age);
