function distance(){

  var i = arguments.length;

  function point(a, b){
    return Math.pow((a - b), 2);
  }

  if(i === 4)
    console.log(Math.sqrt(point(arguments[2], arguments[0]) + point(arguments[3], arguments[1])));
  else if(i === 6)
    console.log(Math.sqrt(point(arguments[3], arguments[0]) + point(arguments[4], arguments[1]) + point(arguments[5], arguments[2])));
  else
    console.log("Error, we need at least two points Eg: distance(x1, y1, x2, y2, ...).");
}

var x1 = 0, y1 = 0, z1 = 1;
var x2 = 1, y2 = 1, z2 = 4;

distance(x1, y1, x2, y2);
distance(x1, y1, z1, x2, y2, z2);
distance(x1);
