var Image = function(width, height, name){
  //generate array based on width and height
  function data(){
    var data = new Array(width*height);
    for (var i = 0; i < data.length; i++) {
      data[i] = Math.floor((Math.random() * 10) + 1);
    }
    return data;
  }
  this.data = data();
  this.width = width;
  this.height = height;
  this.name = name;
};

//register method
Image.prototype.pixelData = function(x,y){
  if(this.data[x,y])
    return this.data[x,y];
  else
    return false;
}

var x = new Image(5, 5, "my2x2Image");
console.log(x);
console.log(x.pixelData(0,25));
