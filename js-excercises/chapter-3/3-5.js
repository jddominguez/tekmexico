function Vehicle(brand, model, country){
	this.brand = brand;
	this.model = model;
	this.country = country;
};

Vehicle.prototype.year = function(){
	return this.brand + this.model;
};

Vehicle.prototype.color = function(){
	return this.brand + this.model + this.country;
};

var myCar = new Vehicle("VW","Jetta", "Germany");

function printCarInfo(theCar){
	if(arguments.length > 1 && arguments[1] === true){
		Object.keys(myCar).forEach(function(key,index) {
			console.log(key);
		});
	}
	else{
		for(var property in theCar)
			console.log(property);
	}
}

//If false, will print all the properties accesible by the object
//If true, will print only the properties of the object itself.
printCarInfo(myCar, false);
