function multiply_to_base_13(x,y){
  var z = x*y, m;
  if(z == 1)
    return 1;
  if(z == 0)
    return 0;
  while (z > 1){
    m = Math.floor(z % 13).toString(13).toUpperCase();
    z = Math.floor(z / 13).toString(13).toUpperCase();
    return z + m + "";
  }
}

var x = multiply_to_base_13(9,6);
console.log(x);

//Another method
function converte_to_base_13(n){
    if (n < 0)
    	n = 0xf + n;
    return n.toString(13).toUpperCase();
}
console.log(converte_to_base_13(9*6));

//Expected simple solution
console.log((9*6).toString(13));
