function sumator() {
  var n = 0;
  for (var i = 0; i < arguments.length; i++) {
    n = arguments[i] + n;
  }
  return n;
}

var x = sumator(1, 1, 1) + sumator(1) + sumator();

console.log(x);
