var myLib = {
  math: {
    real: {
      add: function (a, b){ /*code goes here*/},
      sub: function (a, b){ /*code goes here*/},
      mul: function (a, b){ /*code goes here*/}
    },
    complex: {
      Num: function (real, img){/*code goes here*/},
      add: function (a, b){ /*code goes here*/},
      sub: function (a, b){ /*code goes here*/},
      mul: function (a, b){ /*code goes here*/}
    },
    matrix: {
      add: function (a, b){ /*matrix addition*/},
      sub: function (a, b){ /*matrix subtraction*/},
      mul: function (a, b){ /*matrix multiplication*/},
      eye: function (size){ /*identity matrix*/},
      dot: function (m, a){ /*dot product*/},
      times:function(a, b){ /*element-wise multiplication*/}
    }
  }
};


with(myLib){
  var answer, ans;
  with(math){
    with(real){
      answer = sub(add(20,22), mul(2,5));
      ans = add((5,2),-3);
    }
    with(complex){
      ans = sub(new Num(ans, new Num(3,4)));
    }
    with(matrix)
      times(eye(4), ans )
  }
}

var answer = myLib.math.real.sub(myLib.math.real.add (20, 22), myLib.math.real.mul(2,5));
var ans = myLib.math.matrix.times(
  myLib.math.matrix.eye (4),
  myLib.math.complex.sub (
    new myLib.math.complex.Num(
      myLib.math.real.add(5,2),-3),
      new myLib.math.complex.Num (3,4)
    )
  );
