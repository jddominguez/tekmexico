function Shape(edges){
  this.edges = edges;
}

Shape.prototype.display = function(){
  return "display: " + this.edges;
};

Shape.prototype.area = function(){
  return "area: " + this.edges;
};

Shape.prototype.perimeter = function(){
  return "perimeter: " + this.edges;
};

function Quadrilateral(edges){
  Shape.call(this, edges);
}

Quadrilateral.prototype = new Shape();
Quadrilateral.prototype.constructor = Quadrilateral;
function Quadrilateral(edges){
  if (edges == 4)
    this.edges = edges;
  else
    this.edges = "Wrong size!";
}

var square = new Quadrilateral(4);

var triangle = new Shape(3);

console.log(square.perimeter());
console.log(triangle.perimeter());
