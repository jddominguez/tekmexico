var str = "{prop1: 5, prop2: 10, fn1: function(a,b){ return a + b + this.prop1 }, fn2: function(c, d){ return c * d * this.prop2}, prop3: 15 }";

function dataParse(str){
  var object = eval("(" + str + ")");
  return object;
}

var myObject = dataParse(str);

for (var prop in myObject) {
  if (myObject.hasOwnProperty(prop)) {
    console.log(prop);
  }
}

myObject.prop4 = 99;

console.log(myObject.prop1);
console.log(myObject.prop2);
console.log(myObject.fn1(1,2));
console.log(myObject.fn2(4,3));
console.log(myObject.prop3);
console.log(myObject.prop4);


//Use this method
// JSON.parse('{"1": 1, "2": 2, "3": {"4": 4, "5": {"6": 6}}}', function(k, v) {
//   console.log(k); // log the current property name, the last is "".
//   return v;       // return the unchanged property value.
// });
