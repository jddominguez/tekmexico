var floors = 3;
var rooms = 4;

var building = [
  [
    [], [], [], []
  ],
  [
    [], [], [], []
  ],
  [
    [], [], [], []
  ],
];

var User = function(name){
  this.employee = name;
};

var Equipment = function(name, user){
  this.tool = name;
  this.user = user;
}

function registerUser(name, floor, room){
  building[floor][room].push(new User(name));
}

registerUser("John", 0, 0);
registerUser("Paul", 0, 1);
registerUser("Ringo", 0, 2);
registerUser("George", 0, 3);

function registerEquipment(name, user, floor, room){
  building[floor][room].push(new Equipment(name, user));
}

registerEquipment("Laptop", "John", 2, 0);
registerEquipment("Monitor", "Paul", 2, 1);
registerEquipment("Router", "Ringo", 2, 2);
registerEquipment("Printer", "George", 2, 3);

//search functions

function searchBuilding(name, type){
  if (type === "employee"){
    for (var i = 0; i < floors; i++)
      for (var j = 0; j < rooms; j++)
        for (var k = 0; k < building[i][j].length; k++) {
          if( building[i][j][k].employee == name )
            return "Employee " + name + " found on floor: " + i + " and room: " + j;
    }
    return name + " not found!";
  }
  if(type === "equipment"){
    for (var i = 0; i < floors; i++)
      for (var j = 0; j < rooms; j++)
        for (var k = 0; k < building[i][j].length; k++) {
          if( building[i][j][k].tool == name )
            return name + " belonging to " + building[i][j][k].user + " found in floor: " + i + " room " + j;
    }
    return name + " not found!";
  }
}

//Memoize prototype function
Function.prototype.memoize = function(){
  var self = this, cache = {};
  return function( arg ){
    if(arg in cache) {
      console.log('(stored) '+ arg);
      return cache[arg];
    } else {
      console.log('(search) '+ arg);
      return cache[arg] = self( arg );
    }
  }
}

var memo = searchBuilding.memoize();

memo(searchBuilding("Printer", "equipment"));
memo(searchBuilding("John", "employee"));
memo(searchBuilding("Laptop", "equipment"));
memo(searchBuilding("John", "employee"));
memo(searchBuilding("John", "employee"));
memo(searchBuilding("John", "employee"));
memo(searchBuilding("Lisa", "employee"));
memo(searchBuilding("John", "employee"));
memo(searchBuilding("Lisa", "employee"));
memo(searchBuilding("Modem", "equipment"));
memo(searchBuilding("Laptop", "equipment"));
memo(searchBuilding("Modem", "equipment"));
memo(searchBuilding("Modem", "equipment"));
memo(searchBuilding("Printer", "equipment"));
