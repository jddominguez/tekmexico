Function.prototype.memoize = function(){
    var self = this, cache = {};
    return function( arg ){
      if(arg in cache) {
        console.log('Found '+arg);
        return cache[arg];
      } else {
        console.log('Not found '+arg);
        return cache[arg] = self( arg );
      }
    }
  }

  function fooBar(a){}

  var memoFooBar = fooBar.memoize();

  memoFooBar(1);
  memoFooBar(1);
