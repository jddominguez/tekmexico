function Bank(){

  var Client = function(accountNumber, balance){
    this.accountNumber = accountNumber;
    this.balance = balance;
  };

  var customer = {};

  this.createClient = function(accountNumber, balance){
    var client = customer[accountNumber] = new Client(accountNumber, balance);
    console.log("Welcome, you new account number is #" + client.accountNumber + ", with current balance $" + client.balance);

  };

  this.getBalance = function(accountNumber){
    var client = customer[accountNumber];
    if(client)
      console.log("#" + client.accountNumber + "- Your current balance is $" + client.balance);
    else
      console.log("No account found!");
  }

  this.makeDeposit = function(sourceAccount, targetAccount, ammount){
    var source = customer[sourceAccount];
    var target = customer[targetAccount];
    if(target){
      if(target.ammount < ammount)
        console.log("You can't deposit more than $" + target.ammount);
      else{
        target.balance+=ammount;
        source.balance-=ammount;
        console.log("#" + source.accountNumber + " You deposited $" + ammount + " to account #" + target.accountNumber);
      }
    }
    else{
      console.log("This account number is invalid.");
    }
  };

  this.withdrawal = function(accountNumber, ammount){
    var client = customer[accountNumber];
    if(client){
      if(client.balance < ammount)
        console.log("#" + client.accountNumber + "- Insuficient funds!");
      else{
        client.balance -= ammount;
        console.log("#" + client.accountNumber + "- Succesfully withdrawed $" + ammount + ", your new balance is $" + client.balance);
      }
    }
    else{
      console.log("This account number is invalid.");
    }
  };

}


var myBank = new Bank();

myBank.createClient(111, 1000);
myBank.createClient(222, 2000);

myBank.getBalance(111);
myBank.getBalance(222);

//makeDeposit(sourceAccount, targetAccount, ammount)
myBank.makeDeposit(111,222,500);

myBank.getBalance(111);
myBank.getBalance(222);

//myBank.withdrawal(111,1400);
