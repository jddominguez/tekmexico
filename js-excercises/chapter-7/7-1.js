function hexToRgb(color){
  color = color.match(/^#([0-9a-fA-F]{6})$/i);
  if(color){
    color = color[1]
    var r = parseInt(color[0]+color[1], 16);
    var g = parseInt(color[2]+color[3], 16);
    var b = parseInt(color[4]+color[5], 16);
    console.log("rgb("+ r + ", " + g + ", " + b + ")");
  }
  else{
    console.log(false);
  }
}

hexToRgb("#2030ff");
hexToRgb("#ffffff");
hexToRgb("#000000");
hexToRgb("#1234");
hexToRgb("#0");
