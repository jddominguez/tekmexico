function translate(date){

  var mexicanDates = {
    "16/9": function() { //16/09
      return "Mexican Independence Day";
    },
    "20/11": function() { //20/11
      return "Mexican Revolution Day";
    }
  };

  var koreanDates = {
    "3/1": function() { //03/01
      return "Korean Independence Movement Day";
    },
    "8/16": function() { //08/16
      return "Korean Liberation Day";
    }
  };

  //Regular expressions
  var kr = /^([0-9]{4})\/(0[1-9]|1[0-2]|[1-9])\/(0[1-9]|1[0-9]|2[0-9]|3[0-1]|[1-9])$/;
  var mx = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1]|[1-9])\/(0[1-9]|1[0-2]|[1-9])\/([0-9]{4})$/;

  //Korean to Spanish
  if(date.match(kr)){
    var d = date.match(kr);
    var day = parseInt(d[3], 10) + "/" + parseInt(d[2], 10);
    console.log("Korean: " + date + " → " + "Spanish: " + d[3] + "/" + d[2] + "/" + d[1] + " " + (mexicanDates[day]? mexicanDates[day]() : ""));

  }

  //Spanish to Korean
  if(date.match(mx)){
    var d = date.match(mx);
    var day = parseInt(d[2], 10) + "/" + parseInt(d[1], 10);
    console.log("Spanish: " + date + " → " + "Korean: " + d[3] + "/" + d[2] + "/" + d[1] + " " + (koreanDates[day]? koreanDates[day]() : ""));
  }

}

//Test
translate("16/08/2015"); //Holiday
translate("01/03/2015"); //Holiday
translate("02/02/1978");
translate("2015/09/16"); //Holiday
translate("2015/11/20"); //Holiday
translate("2010/2/2");
