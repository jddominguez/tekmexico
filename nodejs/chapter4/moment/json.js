var moment = require('moment');

var date = new Date(Date.UTC(2007, 0, 1));

console.log('Original', date);

//to JSON
var jsonString = date.toJSON();
console.log(jsonString);

//from JSON
console.log('Round tripped', new Date(jsonString));


var foo = {};
var bar = { 'foo': foo };
console.log(JSON.stringify(bar));

foo.toJSON = function(){ return "custom" };
console.log(JSON.stringify(bar));
