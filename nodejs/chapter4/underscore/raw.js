var foo = [1, 10, 50, 200, 900, 90, 40];

var rawResults = [];

for (var i = 0; i < foo.length; i++) {
  if (foo[i] > 100) {
    rawResults.push(foo[i]);
  }
}

console.log(rawResults);

var _  = require('underscore');

var results = _.filter(foo, function(item){ return item > 100 });
console.log(results);
