var foo = [1, 2, 3, 4, 5, 6, 7, 8, 9];

var _ = require('underscore');

var results = _.map(foo, function(item){
  return item * 2;
})

console.log(results);

//es6 way!
[{note:1},{note:2}].map((item) => {
  console.log(item.note);
});
