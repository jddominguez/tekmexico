function Animal(name){
  this.name = name;
}

Animal.prototype.walk = function(destination) {
  console.log(this.name, 'is walking to', destination);
};

var animal = new Animal('Elephant');

animal.walk('New York');


class Creature {
  constructor(name) {
    this.name = name;
  }

  run(speed){
    console.log(this.name, 'is running very', speed);
  }

};

// Creature.prototype.run = function (speed) {
//   console.log(this.name, ' is running very ', speed);
// };

var creature = new Creature('Troll');

creature.run('fast');
