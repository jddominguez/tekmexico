function Foo(){}

console.log(Foo.prototype.constructor === Foo);

function Bar(){}
var bar = new Bar();
console.log(bar.constructor.name);
console.log(bar.constructor == Bar);
