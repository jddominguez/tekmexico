function logRunningOperation(callback){
  setTimeout(callback, 3000);
}
function userClicked(){
  console.log('starting long operation...');
  logRunningOperation(function(){
    console.log('...ending a long operation.');
  });
}
userClicked();
