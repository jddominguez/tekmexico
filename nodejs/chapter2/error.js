try{
  console.log('About to throw an error!');
  throw new Error('Error thrown!');
}
catch(e){
  console.log('Will only execute if an error is thown!');
  console.log('Error caught: ', e.message);
}
finally{
  console.log('Will execute no matter what!');
}

// try{
//   setTimeout(function(){
//     console.log('About to throw an error!');
//     throw new Error('Error thrown!')
//   }, 1000);
// }
// catch(e){
//   console.log('I will never execute!');
// }
// console.log('Im outside the try block!');

// setTimeout(function(){
//   try{
//     console.log('About to throw an error');
//     throw new Error('Error thrown!');
//   }
//   catch(e){
//     console.log('Error caught!');
//   }
// }, 1000);

function getConnection(callback){
  var connection;
  try{
    throw new Error('Connection failed!');
    callback(error, null);
  }
  catch(error){
    callback(error, null);
  }
}

getConnection(function(error, connection){
  if(error){
    console.log('Error:', error.message);
  }
  else{
    console.log('Connection succeeded:', connection);
  }
});
