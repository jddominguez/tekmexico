var foo = {};
foo.__proto__.bar = 123;
console.log(foo.bar);

function foo1(){
}
foo1.prototype.bar = 123;
var bas = new foo1();
console.log(bas.__proto__ === foo1.prototype);
console.log(bas.bar);

function foo2(){
}
foo2.prototype.bar = 123;
var bas = new foo2();
var qux = new foo2();
console.log(bas.bar);
console.log(qux.bar);
foo2.prototype.bar = 456;
console.log(bas.bar);
console.log(qux.bar);

function foo3(){

}
foo3.prototype.bar = 123;
var bas = new foo3();
var qux = new foo3();

bas.bar = 456;
console.log(bas.bar);
console.log(qux.bar);
