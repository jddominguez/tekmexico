var foo = {
  bar: 123,
  bas: function(){
    console.log('inside this.bar is: ', this.bar);
  }
}
console.log('foo.bar is', foo.bar);
foo.bas();

function foo1(){
  console.log('is this called from globals? : ', this === global);
}
foo1();

var foo2 = {
  bar: 123
}

function bas(){
  if(this === global){
    console.log('called from global');
  }
  if(this === foo2){
    console.log('called from foo2');
  }
}

bas();
foo2.bas = bas;
foo2.bas();

function foo3(){
  this.foo = 123;
  console.log('Is this global? : ', this == global);
}

foo3();
console.log(global.foo);

var newFoo = new foo3();
console.log(newFoo.foo);
