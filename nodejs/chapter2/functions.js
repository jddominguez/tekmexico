function functionName(){

}

function foo(){
  return 123;
}
console.log(foo());


function bar(){

}
console.log(bar());


(function(){
  console.log('foo was executed!');
})();

var foo = 123;
if(true){
  var foo = 456;
}
console.log(foo);

var foo = 123;
if(true){
  (function(){
      var foo = 456;
  })();
}
console.log(foo);

var foo1 = function namedFunction(){
  console.log('foo1');
}
foo1();

var foo2 = function(){
  console.log('foo2');
}
foo2();

setTimeout(function(){
  console.log('2000 milliseconds have passed since execution!');
}, 2000);

function fooN(){
  console.log('2000 milliseconds have passed since execution!');
}
setTimeout(fooN, 2000);

function outerFunction1(arg){
  var variableinOuterFunction = arg;
  function bar(){
    console.log(variableinOuterFunction);
  }
  bar();
}
outerFunction1('Hello closure!');

function outerFunction2(arg){
  var variableinOuterFunction = arg;
  return function(){
    console.log(variableinOuterFunction);
  }
}
var innerFunction = outerFunction2('Hello clouse!');
innerFunction();
