var http = require('http');
var fs = require('fs');

function send404(response) {
  response.writeHead(404, { 'Content-Type': 'text/plain' });
  response.write('Error 404: Resource not found.');
  response.end();
}

var server = http.createServer(function(request, response){
  if(request.method == 'GET' && request.url == '/'){
    response.writeHead(200, { 'Content-Type': 'text/html' });
    fs.createReadStream('./index.html').pipe(response);
  }
  else{
    console.log("send404()");
    send404(response);
  }
}).listen(3000);

console.log('server is running.');
