//Takes time to load
var t1 = new Date().getTime();
var foo1 = require('./foo');
console.log(new Date().getTime() - t1);


//Faster because is already loaded or 'Cached'
var t2 = new Date().getTime();
var foo2 = require('./foo');
console.log(new Date().getTime() - t2);
