var str = 'Hello buffer world!';

var buffer = new Buffer(str, 'utf-8');
console.log(buffer);

var roundTrip = buffer.toString('utf-8');
console.log(roundTrip);
