// let sampleText2: string = "lrvmnir bpr sumvbwvr jx bpr lmiwv yjeryrkbi jx qmbm wibpr xjvni mkd ymibrut jx irhx wi bpr riirkvr jxymbinlmtmipw utn qmumbr dj w ipmhh but bj rhnvwdmbr bpryjeryrkbi jx bpr qmbm mvvjudwko bj yt wkbrusurbmbwjklmird jk xjubt trmui jx ibndt wb wi kjb mk rmit bmiq bj rashmwk rmvp yjeryrkb mkd wbiiwokwxwvmkvr mkd ijyr ynib urymwk nkrashmwkrd bj ower mvjyshrbr rashmkmbwjk jkr cjnhd pmer bj lr fnmhwxwrd mkdwkiswurd bj invp mk rabrkb bpmb pr vjnhd urmvp bpr ibmbrjx rkhwopbrkrd ywkd vmsmlhr jx urvjokwgwko ijnkdhriiijnkd mkd ipmsrhrii ipmsr w dj kjb drry ytirhx bpr xwkmhmnbpjuwbt lnb yt rasruwrkvr cwbp qmbm pmi hrxb kj djnlbbpmb bpr xjhhjcwko wi bpr sujsru msshwvmbwjk mkdwkbrusurbmbwjk w jxxru yt bprjuwri wk bpr pjsr bpmb bprriirkvr jx jqwkmcmk qmumbr cwhh urymwk wkbmvb";

// let sampleText: string = "qgeixed ylv bszly teszxdvixp ylvev xaxu dspaxmvdugy ylgr ylv pgbdsve nuvipgavguv lxd tbrudvevd ylvsepugy ygaxnv evhbmylvsep ugy ygevxpgu ilm ylvsep tryyg dg xud dsvsuyg ylv cxbbvmgq dvxyl egdvylv psflrudevdwxuugu ygeszly gq ylva wxuuguyg bvqygq ylvawxuugu suqeguy gqylva cgbbvmvd xud ylrudvevdpygeavd xy isyl plgyxud plvbbtgbdbm ylvm egdv xudivbbsuygylv oxip gqdvxylsuyg ylv agryl gq lvbbegdv ylvpsf lrudevd qbxplvdxbb ylvse pxtevp txevqbxplvd xp ylvm yreuvd su xsepxtesuz ylv zruuvep ylvevwlxezsuz xuxeam ilsbvxbb ylvigebd igudvevdhbruzvdsu ylv txyyvem pagnveszlyylegr ylv bsuv ylvm tegnvwgppxwn xuderppsxuevvbvd qegaylv pxtev pyegnvplxyyvevd xudprudvevdylvu ylvmegdv txwn tryugyugy ylvpsf lrudevd";

let sampleText: string = "qgeixedylvbszlyteszxdvixpylvevxaxudspaxmvdugyylgrylvpgbdsvenuvipgavguvlxdtbrudvevdylvsepugyygaxnvevhbmylvsepugyygevxpguilmylvseptryygdgxuddsvsuygylvcxbbvmgqdvxylegdvylvpsflrudevdwxuuguygeszlygqylvawxuuguygbvqygqylvawxuugusuqeguygqylvacgbbvmvdxudylrudvevdpygeavdxyisylplgyxudplvbbtgbdbmylvmegdvxudivbbsuygylvoxipgqdvxylsuygylvagrylgqlvbbegdvylvpsflrudevdqbxplvdxbbylvsepxtevptxevqbxplvdxpylvmyreuvdsuxsepxtesuzylvzruuvepylvevwlxezsuzxuxeamilsbvxbbylvigebdigudvevdhbruzvdsuylvtxyyvempagnveszlyylegrylvbsuvylvmtegnvwgppxwnxuderppsxuevvbvdqegaylvpxtevpyegnvplxyyvevdxudprudvevdylvuylvmegdvtxwntryugyugyylvpsflrudevd";

//const dictionary: string = "etaoinshrdlcumwfgypbvkjxqz"; //most common ocurrentfrequent of letters
const dictionary: string = "ethoirsanglcymwfbupdvkjxqz";
//const dictionary: string = "ethionclrdasmuwfgypbvkjxqz";
//const dictionary: string = "ethoirpandlcumfwbysbvkjxqz";
//const dictionary: string = "ethionsardlcumfwgypbvkjxqz";
//const dictionary: string = "ethoirsandlcymfwgupbv00xqz";
//const dictionary: string = "ethoirsandlcymfwgupbvkjxqz";

function letterCount(text: string) {
    //generate alphabet
    let alphabet: string[] = "abcdefghijklmnopqrstuvwxyz".split('');
    let total: any[];
    total = alphabet.map((element, index) => {
        let count = (text.match(new RegExp(element, "g")) || []).length;
        return { letter: element, total: count };
    });
    //What to do with letters with zero?
    total.sort((a, b) => {
        if (a.total < b.total) {
            return 1;
        }
        if (a.total > b.total) {
            return -1;
        }
        return 0;
    });
    return total;
}

function frequentLetters() {
    let byFrequency: string[] = dictionary.split('');
    return byFrequency;
}




function findAllPatterns(text:string){
    //let count = (text.match(new RegExp(element, "g")) || []).length;
    let newText: string[] = text.split('');
    let isPattern: string[];


    for(let i = 0; i < newText.length; i++){
        let count = (text.match(new RegExp(newText[i]+newText[i+1], 'g')) || []).length;
        if(count > 1){
            count = (text.match(new RegExp(newText[i]+newText[i+1]+newText[i+2], 'g')) || []).length;
        }
        console.log(newText[i]+newText[i+1], count);
    }


    // newText.forEach((element, index) => {
    //     //console.log(element);
    //     let count = (text.match(new RegExp(element, "g")) || []).length;
    //     console.log(count);
    // });
}

findAllPatterns(sampleText);

function decodeText(text: string) {
    let sortedCount:any = letterCount(text);
    let sortedFrequency:string[] = frequentLetters();
    //console.log(sortedCount);
    //console.log(sortedFrequency);

    let newAlphabet: any[];
    newAlphabet = sortedCount.map((element, index) =>{
        return {letter: element.letter, possibly: sortedFrequency[index] }
    });

    //console.log(newAlphabet);
    //let possibleResult:any[];

    let newText:string[] = text.split('');
    for(let i:number = 0; i < newText.length ; i++){
        newAlphabet.some((element) => {
            if(newText[i] === element.letter){
                //console.log(newText[i], element.letter);
                return newText[i] = newText[i]=(element.possibly);
            }
        });
    }
    //console.log(newText.join(''));

    return null;
}


let outputText = decodeText(sampleText);
