class Graph{
  constructor(nodes, connections){
    this.nodes = {};
    for (var i = 0; i < nodes.length; i++) {
      this.addNode(nodes[i]);
    }
    for (var i = 0; i < connections.length; i++) {
      this.addConnection(connections[i][0], connections[i][1]);
    }
  }

  addNode(node){
    this.nodes[node] = [];
  }

  removeNode(node){
    delete this.nodes[node];
  }

  hasNode(node){
    return this.nodes[node] ? true : false;
  }

  addConnection(fromNode, toNode) {
    this.nodes[toNode].push(fromNode);
    this.nodes[fromNode].push(toNode);
  }

  removeConnection(fromNode, toNode){
    var fromnodearr = this.nodes[fromNode];
    var i = fromnodearr.indexOf(toNode,false);
    fromnodearr.splice(i,1);
    var tonodearr = this.nodes[toNode];
    var j = tonodearr.indexOf(fromNode,false);
    tonodearr.splice(j,1);
  }

  hasConnection(fromNode, toNode){
    var temp = this.nodes[fromNode];
    if(temp.length===0) {
      return false;
    }
    for(var i=0;i<temp.length;i++) {
      if(temp[i] === toNode){
        return true;
      }
    }
    return false;
  }

  forEachNode(fn) {
    for(var n in this.nodes) {
      fn(n);
    }
  }

  findPath(fromNode, toNode, path = []){
    path = path + [fromNode];
    if(fromNode == toNode){
      return path;
    }
    for(var n of this.nodes[fromNode]){
      if(!path.includes(n)){
        var newPath = this.findPath(n, toNode, path);
        if(newPath){
          return newPath;
        }
      }
    }
  }

}

var myNodes = ["A", "B", "C", "D", "E", "F"];
var myConnections = [
  ["A", "B"],
  ["A", "C"],
  ["A", "D"],
  ["A", "E"],
  ["E", "F"],
  ["F", "B"],
]

var myGraph = new Graph(myNodes,myConnections);
console.log(myGraph);

console.log(myGraph.findPath("A", "F"));

// var test = function(item){
//   console.log(myGraph.hasConnection(item, "A"));
// }
// myGraph.forEachNode(test);
