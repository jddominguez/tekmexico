const x = lcg(null, Date.now(), 1664525, 1013904223, Math.pow(2, 32)); //Numerical Recipes

function randomNumberGenerator(){
  return x.next().value;
}

function *lcg(x, seed, a , c , m ){
  if( x + seed == m){
    yield (a * x + c) % m;
    return x / m;
  }
  if(x == null){
    x = (a * seed + c) % m;
  }
  else{
    x = (a * x + c) % m;
  }
  yield x / m;
  yield *lcg(x, seed, a, c, m);
}

let cards = [...Array(20).keys()];

function shuffle(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        let j = Math.floor(randomNumberGenerator() * (i + 1));
        let temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    return arr;
}

console.log(cards);
console.log(shuffle(cards));
