// function getRandomNumber(){
//   let current = null;
//   function* lcg(seed, a, c, m){
//     current = (a * seed + c) % m;
//
//     console.log(current);
//
//     if(current + seed == m){
//       yield current;
//     }
//     else{
//       yield * lcg(current, a, c, m);
//     }
//   }
//   return lcg(1, 2, 3, 5).next().value;
// }
//
// var pos = getRandomNumber();
//
// console.log('->', pos);
//



function lcg(seed, a, c, m){
  current = (a * seed + c) % m;
  console.log(current);
  if(current + seed == m){
    return current;
  }
  return lcg(current, a, c, m);
}

console.log(lcg(1, 2, 3, 10));

// function* randomNumber(){
//   let x = 0;
//   while(true){
//     yield lcg(1, 2, 3, 5);
//   }
// }
//
// var rnum = randomNumber();
//
// console.log(rnum.next().value);
