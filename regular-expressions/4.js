function hashtagsToLinks(text){
  var results = text.match(/#\w+/g);
  for (var i = 0; i < results.length; i++)
    text = text.replace(results[i], '<a href="https://twitter.com/search?q=%23' + results[i].slice(1) + '">' + results[i] + "</a>");
  return text;
}

console.log(hashtagsToLinks("Lorem #ipsum #dolor"));
// console.log(hashtagsToLinks("#Lorem #ipsum dolor sit amet, consectetur adipiscing elit. Integer at leo auctor, molestie quam eget, venenatis augue. Aliquam accumsan facilisis tellus, sit amet tincidunt neque porta a. Morbi dictum, ex ut eleifend feugiat, dui tortor mattis massa, sit amet feugiat ex nisl id lorem. Sed urna ipsum, faucibus ut lobortis sit amet, hendrerit sed orci. Praesent lectus mi, venenatis in magna id, suscipit porta mi. Vivamus luctus, augue ut viverra maximus, odio nisl vestibulum ipsum, eget imperdiet erat lectus ut lectus. Nam iaculis, lectus et mattis vulputate, lorem metus malesuada nisi, et luctus ipsum libero nec purus. Maecenas bibendum leo turpis, gravida tincidunt felis accumsan ac. Nullam sed nisl feugiat, ultrices massa quis, varius elit. Donec imperdiet ac felis suscipit pretium. Praesent ac diam tellus. Curabitur non enim auctor, cursus magna et, convallis mauris. Proin facilisis mattis mauris, id rutrum purus eleifend vel. Integer maximus turpis urna, sed condimentum eros convallis ornare. Ut ut commodo sapien."));
