function regex(input, expression){

  if (expression == 1)
    return input.match(/.abc/);//Any character except ""\n" + "abc"

  if (expression == 2)
    return input.match(/a+b?!!1{4}/);//"a" one or more times + repeat "b" optional + "11" + repeat "1" 4 times

  if (expression == 3)
    return input.match(/.{3}a\.b/);//Repeat any character except "\n" 3 times + "a.b"

  if (expression == 4)
    return input.match(/\w/);//Any word character

  if (expression == 5)
    return input.match(/\s/);//Any whitespace character

  if (expression == 6)
    return input.match(/\d/);//Any digit

  if (expression == 7)
    return input.match(/./);//Any character except "\n"

  if (expression == 8)
    return input.match(/[abc]/);//Any character in [abc]

  if (expression == 9)
    return input.match(/(abc)/);//Group (abc)

  if (expression == 10)
    return input.match(/[a-zA-Z_\$\.]+[A-Za-z_\$0-9\.]*@[a-zA-Z_\$\.]+[a-zA-Z_\$0-9\.]*\.(com|net|org){1}/);
    //Any character in [ a to z A to Z _ $ .] one or more times + any character in [ A to Z a to z _ $ 0 to 9 .] none or more times + "@"
    //Any character in [ a to z A to Z _ $ .] one or more times + any character in [ a to z A to Z _ $ 0 to 9 .] none or more ties + "."
    //Group 1 or match either of the followings (com, net, org) just one time

  if (expression == 11)
    return input.match(/\([0oOn]{1}(_|\s)[0oOn]{1}\)/);
    // "(" + any character in [ 0 o O n] one time + Group 1 or match either of the followings "_", WhiteSpaceCharacter + repeat any character in[ 0 o O n] just one time

  else
    return false;
}

console.log(regex("!abc", 1));
console.log(regex("ab!!1111", 2));
console.log(regex("+++a.b", 3));
console.log(regex("A", 4));
console.log(regex(" +", 5));
console.log(regex("1", 6));
console.log(regex(" ", 7));
console.log(regex("c", 8));
console.log(regex("abc", 9));
console.log(regex("abc123@abc123.org", 10));
console.log(regex("(O_O)(0 0)", 11));
