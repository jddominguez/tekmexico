function regex(input, expression){
  if (expression == 1)
    return input.match(/[a-zA-Z]+\s[0-9]{2},\s[0-9]{4}/);

  if (expression == 2)
      return input.match(/^([0-9]?\w+)$/);

  if (expression == 3)
      return input.match(/^[a-zA-Z]+\.(cpp|jar|txt){1}$/);

  if (expression == 4)
      return input.match(/\b(\w)?(\w)\w?\2\1/);

  if (expression == 5)
    return input.match(/\b([b-yB-Y]+)\b/g);

  if (expression == 6)
    return input.match(/<(?:([\w][\w]*)\b[^>]*>(?:.*?)<\/\1>|[\w][\w]*\b[^>]*\/>)/gm);

  else
    return false;
}

console.log(regex("September 11, 0200", 1));
console.log(regex("February 99, 0001", 1));
console.log(regex("June 04, 3000", 1));

console.log(regex("A52", 2));
console.log(regex("d747", 2));
console.log(regex("27X", 2));
console.log(regex("v2", 2));

console.log(regex("myfile.txt", 3));

console.log(regex("12", 4));
console.log(regex("abcba", 4));
console.log(regex("12321", 4));
console.log(regex("_1a1_", 4));

console.log(regex("Bee zapp Crow Eagle Zorro  mouse Ape  you", 5));

console.log(regex("Is <b>4 < -1/12</b> true? The <b>answer</b> will <em>surprise</em> you", 6));
