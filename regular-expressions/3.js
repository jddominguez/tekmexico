function shift(input){
  var list = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
  var input = input.split("");
  var output= [];
  for (var i = 0; i < input.length; i++) {
    if(input[i].match(/[a-z0-9]/)){
      list.indexOf(input[i]) === 25 ? output.push(list[0]) :
      list.indexOf(input[i]) === 35 ? output.push(list[26]) :
      output.push(list[list.indexOf(input[i])+1]);
    }
    if(input[i].match(/[A-Z]/)){
      list.indexOf(input[i].toLowerCase()) === 25 ? output.push(list[0]) :
      output.push(list[list.indexOf(input[i].toLowerCase())+1].toUpperCase());
    }
  }
  return output;
}

console.log(shift("aBc"));
console.log(shift("xyz"));
console.log(shift("aK89"));
