namespace Arreis {

    export function random(n: number, min: number, max: number): number[] {
        let arr: number[] = [];
        for (let i: number = 0; i < n; i++) {
            arr[i] = Math.floor(Math.random() * (max - min)) + min;
        }
        return arr;
    };

    export function sum(a: number[], b: number[]): number[] {
        let result: number[] = [];
        let t0 = performance.now();
        console.log("Executing function...");
        for (let i: number = 0; i < a.length; i++) {
            result[i] = a[i] + b[i];
        }
        let t1 = performance.now();
        console.log("Time: " + (t1 - t0) + " milliseconds.");
        return result;
    };

}

declare function require(name: string);

let unique = require('uniq');


let arrayA: number[] = Arreis.random(10, 0, 9);


console.log(arrayA);

let arrayB: number[] = Arreis.random(10, 0, 9);
console.log(arrayB);

let arrayC:number[] = Arreis.sum(arrayA, arrayB);
console.log(arrayC);

console.log(unique(arrayA));
