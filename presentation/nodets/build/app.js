var Arreis;
(function (Arreis) {
    function random(n, min, max) {
        let arr = [];
        for (let i = 0; i < n; i++) {
            arr[i] = Math.floor(Math.random() * (max - min)) + min;
        }
        return arr;
    }
    Arreis.random = random;
    ;
    function sum(a, b) {
        let result = [];
        let t0 = performance.now();
        console.log("Executing function...");
        for (let i = 0; i < a.length; i++) {
            result[i] = a[i] + b[i];
        }
        let t1 = performance.now();
        console.log("Time: " + (t1 - t0) + " milliseconds.");
        return result;
    }
    Arreis.sum = sum;
    ;
})(Arreis || (Arreis = {}));
let unique = require('uniq');
let arrayA = Arreis.random(10, 0, 9);
console.log(arrayA);
let arrayB = Arreis.random(10, 0, 9);
console.log(arrayB);
let arrayC = Arreis.sum(arrayA, arrayB);
console.log(arrayC);
console.log(unique(arrayA));
