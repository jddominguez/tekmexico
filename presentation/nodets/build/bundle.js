(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var Arreis;
(function (Arreis) {
    function random(n, min, max) {
        let arr = [];
        for (let i = 0; i < n; i++) {
            arr[i] = Math.floor(Math.random() * (max - min)) + min;
        }
        return arr;
    }
    Arreis.random = random;
    ;
    function sum(a, b) {
        let result = [];
        let t0 = performance.now();
        console.log("Executing function...");
        for (let i = 0; i < a.length; i++) {
            result[i] = a[i] + b[i];
        }
        let t1 = performance.now();
        console.log("Time: " + (t1 - t0) + " milliseconds.");
        return result;
    }
    Arreis.sum = sum;
    ;
})(Arreis || (Arreis = {}));
let unique = require('uniq');
let arrayA = Arreis.random(10, 0, 9);
console.log(arrayA);
let arrayB = Arreis.random(10, 0, 9);
console.log(arrayB);
let arrayC = Arreis.sum(arrayA, arrayB);
console.log(arrayC);
console.log(unique(arrayA));

},{"uniq":2}],2:[function(require,module,exports){
"use strict"

function unique_pred(list, compare) {
  var ptr = 1
    , len = list.length
    , a=list[0], b=list[0]
  for(var i=1; i<len; ++i) {
    b = a
    a = list[i]
    if(compare(a, b)) {
      if(i === ptr) {
        ptr++
        continue
      }
      list[ptr++] = a
    }
  }
  list.length = ptr
  return list
}

function unique_eq(list) {
  var ptr = 1
    , len = list.length
    , a=list[0], b = list[0]
  for(var i=1; i<len; ++i, b=a) {
    b = a
    a = list[i]
    if(a !== b) {
      if(i === ptr) {
        ptr++
        continue
      }
      list[ptr++] = a
    }
  }
  list.length = ptr
  return list
}

function unique(list, compare, sorted) {
  if(list.length === 0) {
    return list
  }
  if(compare) {
    if(!sorted) {
      list.sort(compare)
    }
    return unique_pred(list, compare)
  }
  if(!sorted) {
    list.sort()
  }
  return unique_eq(list)
}

module.exports = unique

},{}]},{},[1]);
