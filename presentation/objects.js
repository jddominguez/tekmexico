function foo(){
  var a = 1;
  function bar(){
    var b = 2;
    console.log(a);
  }
  bar();
  //console.log(b);
  function zap(){
    //console.log(b);
  }
  zap();
}
foo();

var vehicle = {
  type: undefined,
  color: undefined,
  doors: undefined,
  manufacturer: undefined
};

console.log(vehicle["color"] + " _ " + vehicle["doors"]);

vehicle.manufacturer = {
  company: "Toyota",
  country: "Japan"
}

vehicle.type = "Sedan";

console.log(vehicle);


function Auto(brand, model){
  this.brand = brand;
  this.model = model;
}

Auto.prototype.year = 2000;

var taxi = new Auto("Toyota", "Corolla");

console.log(taxi.year);
