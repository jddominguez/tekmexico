class Animal{
  constructor(gender, age){
    this.gender = gender;
    this.age = age;
  }
  info(){
    return console.log(this);
  }
}


class Bird extends Animal{
  constructor(gender, age, flies){
    super(gender, age);
    this.flies = flies;
  };

  fly(){
    if(this.flies){
      console.log("I can fly!");
    }
    else{
      console.log("Sorry!");
    }
  }

}

class Fish extends Animal{
  constructor(gender, age, habitat){
    super(gender, age);
    this.habitat = habitat;
  }
}

var pengiun = new Bird("Male", 5, false);

var eagle = new Bird("Female", 10, true);

var salmon = new Fish("Male", 1, "Freshwater");

pengiun.info();
pengiun.fly();

eagle.info();
eagle.fly();

salmon.info();
