class Mammalia{
  constructor(name, breed, color, age, type){
    this.name = name;
    this.breed = breed;
    this.color = color;
    this.age = age;
    this.type = type;
  }
  info(){
    return console.log( this.type + " - " +this.name + " is a " + this.age + " year old " + this.color + " " + this.breed );
  }
}

class Ferae extends Mammalia{
  constructor(name, breed, color, age, type, subtype){
    super(name, breed, color, age, type);

    if(this.type === "Canis"){
      this.subtype = subtype;
    }
  }
  info(){
    return super.info() + " " + this.subtype;
  }
}

var kobi = new Ferae("Koby", "Pitbull", "Brown", 4, "Canis", "Domestic Dog");
var bombona = new Ferae("Bombona", "French Poodle", "White" , 7, "Canis");

kobi.info();
bombona.info();

var garfield = new Ferae("Garfield", "Tabby", "Orange", 8, "Felis");

garfield.info();
