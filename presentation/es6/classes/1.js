class Animalia{
  constructor(alive, legs){
    this.eyes = alive;
    this.legs = legs;
  }

  createAnimal(){
    return "New creature with " + this.eyes + " eyes and " + this.legs + " legs.";
  }

}

class Canidae extends Animalia{
  constructor(eyes, legs, type){
    super(eyes, legs);
    this.type = type;
  }

  createCanidae(){
    return super.createAnimal + "This Canidae is type " + this.type;
  }

}

var mutantZebra = new Animalia(4, 8);

var dog = new Canidae(2,2, "Dog")

console.log(mutantZebra);

console.log(dog);
