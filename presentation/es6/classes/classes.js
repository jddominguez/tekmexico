class Animalia{
  constructor(alive){
    this.alive = alive;
  }
  create(){
    return console.log(this);
  }
}

class Mammal extends Animalia{
  constructor(alive){
    super(alive);
    this.legs = 4;
  }
}

class fishes{
}


var dog = new Mammal(true);

dog.create();
