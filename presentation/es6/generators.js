function *foo(n){
  yield 1;
  yield 2;
  yield 3;
}

for (var v of foo()) {
  console.log(v);
}
