var o1 = {
  bar() {
    console.log( "o1:foo" );
  }
};

var o2 = {
  foo() {
    super.bar();
    console.log( "o2:foo" );
  }
};

 Object.setPrototypeOf(o1,o2 );

o2.foo();
