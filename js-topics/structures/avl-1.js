Node = function(value, left, right, parent, height){
  this._value = value;
  this._left = left;
  this._right = right;
  this._parent = parent;
  this._height = height;
};

BinaryTree = function(){
  this._root = null;
};

BinaryTree.prototype.insert = function(value, current){
  if(this._root === null)
    this._root = new Node(value, null, null, null);
  var key, current = current || this._root;
  if(current._value > value)
    key = "_left";
  else
    key = "_right";
  if(!current[key])
    current[key] = new Node(value, null, null, current);
  else
    this.insert(value, current[key]);
};

BinaryTree.prototype.find = function(value){
  return this._find(value, this._root);
};

BinaryTree.prototype._find = function(value, current){
  if(!current)
    return null;
  if(current.value === value)
    return current;
  if(current.value > value)
    return this._find(value, current._left);
  if(current.value < value)
    return this._find(value, current._right);
};

//Check if tree is balanced
BinaryTree.prototype.isBalanced = function () {
  return this._isBalanced(this._root);
};
BinaryTree.prototype._isBalanced = function (current) {
  if (!current)
    return true;
  return this._isBalanced(current._left)  && this._isBalanced(current._right) && Math.abs(this._getHeight(current._left) - this._getHeight(current._right)) <= 1;
};
BinaryTree.prototype.getHeight = function () {
  return this._getHeight(this._root);
};
BinaryTree.prototype._getHeight = function (node) {
  if (!node)
    return 0;
  return 1 + Math.max(this._getHeight(node._left), this._getHeight(node._right));
};



var tree = new BinaryTree();

tree.insert(2);
tree.insert(3);
tree.insert(1);

console.log(tree._root._right._right);

console.log(tree.isBalanced());
