var grunt = require('grunt');

grunt.registerTask('world', 'default task description', function(){
  console.log('I am grunt!');
});

grunt.registerTask('hello', 'say hello', function(name){
  if(!name || !name.length){
    grunt.warn('you need to provide a name: ');
  }
  console.log('I am grunt!, my name is ', name);
});

grunt.registerTask('default', ['world', 'hello:Daniel']);
