def baseconvert(n, base):
    """convert positive decimal integer n to equivalent in another base (2-36)"""

    digits = "0123456789abcdefghijklmnopqrstuvwxyz"

    try:
        n = int(n)
        base = int(base)
    except:
        return ""

    if n < 0 or base < 2 or base > 36:
        return ""

    s = ""

    # print n
    # print base

    # print n%base
    # print digits[n%base]+s
    # print n/base

    while 1:
        r = n % base
        s = digits[r] + s


        n = n / base

        print n
        if n == 0:
            break

    return s

baseconvert(0,8)