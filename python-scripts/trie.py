_end = '_end_'
def make_trie(*words):
	root = dict()
	for word in words:
		current_dict = root
		print current_dict
		print ">"
		for letter in word:
			print letter
			print current_dict.setdefault(letter, {})
			current_dict = current_dict.setdefault(letter, {})
		current_dict[_end] = _end
	print root
	return root

make_trie('foo')
{'b': {'a': {'r': {'_end_': '_end_', 'z': {'_end_': '_end_'}}, 
             'z': {'_end_': '_end_'}}}, 
 'f': {'o': {'o': {'_end_': '_end_'}}}}


# def in_trie(trie, word):
# 	current_dict = trie
# 	for letter in word:
# 		if letter in current_dict:
# 			current_dict = current_dict[letter]
# 		else:
# 			return False
# 	else:
# 		if _end in current_dict:
# 			return True
# 		else:
# 			return False

# print in_trie(make_trie('foo', 'bar', 'baz', 'barz'), 'baz')
# print in_trie(make_trie('foo', 'bar', 'baz', 'barz'), 'barz')
# print in_trie(make_trie('foo', 'bar', 'baz', 'barz'), 'fool')