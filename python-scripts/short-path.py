def nPaths(x1, y1, x2, y2):
	if x1 > x2 or y1 > y2:
		return "Not posible!"
	if x1 == x2  and y1 == y2:
		return "Same position!"
	n = x2-x1
	m = y2-y1
	matrix = [[0 for x in range(m)] for x in range(n)]

	i = 0
	j = 0

	def printMatrix(matrix):
		s = [[str(e) for e in row] for row in matrix]
		lens = [max(map(len, col)) for col in zip(*s)]
		fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
		table = [fmt.format(*row) for row in s]
		print '\n'.join(table)
		print '\n'

	for i in range(i,m):
		matrix[i][0] = 1
	printMatrix(matrix)

	for j in range(j, m):
		matrix[0][j] = 1
	printMatrix(matrix)


	for i in range(1, n):
		print "i = %d" % (i)
		for j in range(1, m):
			print "j = %d" % (j)
			print "position(%d, %d) = %d" % (i-1, j, matrix[i-1][j])
			print "position(%d, %d) = %d" % (i, j-1, matrix[i][j-1])
			print "Set(%d, %d) = %d" % (i, j, matrix[i-1][j] + matrix[i][j-1])
			matrix[i][j] = matrix[i-1][j] + matrix[i][j-1]
			printMatrix(matrix)
			j = j + 1
		i = i + 1

	return "Result: %d" % (matrix[n-1][m-1])


print nPaths(0, 0, 3, 3)