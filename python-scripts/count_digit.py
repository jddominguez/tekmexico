def count_digit(max_num, digit):
    str_digit = str(digit)
    return sum(str(num).count(str_digit) for num in xrange(max_num+1))

# print count_digit(1000,2)
# print count_digit(2000,2)
# print count_digit(3000,2)
# print count_digit(4000,2)
# print count_digit(5000,2)
# print count_digit(6000,2)
# print count_digit(7000,2)

#print count_digit(10000000,2)


def knSolver( k, n ):
    # if k == '0':
    #     return knSolver0( n, 0 )
    if not n:
        return 0
    ct = 0
    n = int(n)
    k = int(k)
    l = len(str(n))
    f = int(str(n)[:1])

    print "----"
    print ct
    print n
    print k
    print l
    print f
    print "----"

    #print f

    if l > 1:
        ct += f * 10 ** (l-2) * (l-1)
    if f > k:
        ct += 10 ** (l-1)
    elif f == k:
        ct += n - f * 10 ** (l-1) + 1
    return ct + knSolver( k, str(n)[1:])

print knSolver(2, 12345)

