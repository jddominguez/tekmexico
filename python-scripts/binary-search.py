class Node:

  def __init__(self, data):
    self.left = None
    self.right = None
    self.data = data

  def insert(self, data):
      if self.data:
        if data < self.data:
          if self.left is None:
            self.left = Node(data)
          else:
            self.left.insert(data)
        elif data > self.data:
          if self.right is None:
            self.right = Node(data)
          else:
            self.right.insert(data)
      else:
        self.data = data

  def find(self, data, parent=None):
      if data < self.data:
        if self.left is None:
          return None, None
        print self.left.data
        return self.left.find(data, self)
      elif data > self.data:
        if self.right is None:
          return None, None
        print self.right.data
        return self.right.find(data, self)
      else:
        return self, parent


# def prints(self):
#   if self.left:
#     self.left.prints();
#   print self.data,
#   if self.right:
#     self.right.prints();



root = Node(10)

root.insert(5)
root.insert(9)
root.insert(7)
root.insert(1)
root.insert(6)
root.insert(3)
root.insert(4)
root.insert(2)

root.find(4)
print " "
root.find(2)

# print "\n"
# for data in root.print_generator():
#   print data
# node, parent = root.find(1)
