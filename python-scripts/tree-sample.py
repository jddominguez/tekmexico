tree = [
    [1,'a'],
        [2,'b'],
    [1,'c'],
    [1,'d'],
        [2,'e'],
            [3,'f'],
                [4,'g'],
    [1,'h'],
    [1,'i'],
        [2,'j'],
            [3,'k'],
            [3,'l']
    ]

print tree

stack = [[]]
for level, item in tree:
    while len(stack) > level:
        stack.pop()
    while len(stack) <= level:
        node = (item, [])
        stack[-1].append(node)
        stack.append(node[1])

result = stack[0]

print result


def flatten(l):
    try:
        for i in l: # iterate each element
            for j in flatten(i): #generator
                yield j #return
    except TypeError:
        yield l
        
#print x #print original

#m =  flatten(x)

#n = list(m)
#print n

#print [i for i in flatten(tree)] #print flatten