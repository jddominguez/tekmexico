def changes(amount, set):
    ways = [0] * (amount + 1)
    ways[0] = 1
    for num in set:
        for j in xrange(num, amount + 1):
            ways[j] += ways[j - num]
            print j - num
    return ways[amount]
 
#print changes(6, [1,2,3,4,5])


# 5+1
# 4+1+1
# 3+1+1+1
# 2+1+1+1+1
# 1+1+1+1+1+1

# 4+2
# 3+2+1
# 2+2+1+1

# 3+3

#2+2+2

def accel_asc(n):
    a = [0 for i in range(n + 1)]
    k = 1
    y = n - 1
    while k != 0:
        x = a[k - 1] + 1
        k -= 1
        while 2 * x <= y:
            a[k] = x
            y -= x
            k += 1
        l = k + 1
        while x <= y:
            a[k] = x
            a[l] = y
            yield a[:k + 2]
            x += 1
            y -= 1
        a[k] = x + y
        y = x + y - 1
        yield a[:k + 1]


def partition(number):
	answer = set()
	answer.add((number, ))
	for x in range(1, number):
		for y in partition(number - x):
			answer.add(tuple(sorted((x, ) + y)))
	return answer

#print partition(3)


def partitions(n):
	if n == 0:
		yield []
		return
	for p in partitions(n-1):
		yield [1] + p
		if p and (len(p) < 2 or p[1] > p[0]):
			yield [p[0] + 1] + p[1:]

print max([reduce(lambda x, y: x*y, i) for i in accel_asc(10)])