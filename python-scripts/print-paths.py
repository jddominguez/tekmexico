def printPaths(temp, i, j, m, n, matrix):
	temp = str(temp)+str(matrix[i][j])
	if i == m - 1 and j == n - 1 :
		print temp
	if j + 1 < n :
		printPaths(temp, i, j + 1, m, n, matrix)
	if i + 1 < m :
		printPaths(temp, i + 1, j, m, n, matrix)

matrix = [ [1,2,3], [4,5,6], [7,8,9] ]
temp = ""
printPaths(temp, 0, 0, 3, 3, matrix)