#http://code.activestate.com/recipes/126037-getting-nth-permutation-of-a-sequence/
def NPerms (seq):
    "computes the factorial of the length of "
    return reduce (lambda x, y: x * y, range (1, len (seq) + 1), 1)

def PermN (seq, index):
    "Returns the th permutation of  (in proper order)"
    seqc = list (seq [:])
    result = []
    fact = NPerms (seq)
    index %= fact
    while seqc:
        fact = fact / len (seqc)
        choice, index = index // fact, index % fact
        result += [seqc.pop (choice)]
    return result

for i in range (0, 25):
    print i, PermN ([1, 1, 2, 3], i)

print NPerms ([1,1,2,3])
