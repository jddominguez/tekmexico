#Find the number of combinations possible on any integer number using the alphabet a coding scheme.
# e.g. 1123


def permutations(iterable):
    pool = tuple(iterable)
    n = len(pool)
    r = n
    indices = range(n)
    cycles = range(n, n-r, -1)
    yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                yield tuple(pool[i] for i in indices[:r])
                break
        else:
            return

num = 1123

charset = ['+', 'A', 'B','C']

x = map(int, str(num))

i=1

for x in permutations(x):
	print x
	print i
	i=i+1

	#val = [x[2*i] * 10 + x[2*i+1] for i in range(len(x)/2)]


	#print val.astype(int)
