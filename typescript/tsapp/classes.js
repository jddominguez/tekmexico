var Animal = (function () {
    function Animal(name, age, alive) {
        this.color = 'white';
        this.size = 'small';
        this.name = name;
        this.age = age;
        this.alive = alive;
    }
    Animal.prototype.info = function () {
        return "This animal is a " + this.name + ' of age ' + this.age + ' and looks ' + this.alive;
    };
    return Animal;
}());
var horse = new Animal("horse", 10, true);
console.log(horse.info());
console.log(horse.age);
console.log(horse.color);
var passcode = "secret passcode1";
var Employee = (function () {
    function Employee() {
    }
    Object.defineProperty(Employee.prototype, "fullName", {
        get: function () {
            return this._fullName;
        },
        set: function (newName) {
            if (passcode && passcode == "secret passcode") {
                this._fullName = newName;
            }
            else {
                console.log("Error: Unauthorized update of employee!");
            }
        },
        enumerable: true,
        configurable: true
    });
    return Employee;
}());
var employee = new Employee();
employee.fullName = "Bob Smith";
if (employee.fullName) {
    console.log(employee.fullName);
}
