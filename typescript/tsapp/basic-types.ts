//Boolean
let isDone: boolean = false;

//Number
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
console.log(isDone, decimal, hex, binary, octal);

//String
let color: string = "blue";
color = 'red';
console.log(color);
let fullName: string = 'Daniel Dominguez';
let age: number = 30;
let sentence: string = `Hello, my name is ${ fullName }. I\'ll be ${ age + 1 } years old next January.`;
console.log(sentence);

//Array
let list1: number[] = [1, 2, 3];
let list2: Array<number> = [1, 2, 3];
console.log(list1, list2);

//Tuple
let x: [string, number];
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"]; // Error

console.log(x[0].substr(1)); // OK
//console.log(x[1].substr(1)); // Error, 'number' does not have 'substr'
x[3] = "world"; // OK, 'string' can be assigned to 'string | number'
//console.log(x[5].toString()); // OK, 'string' and 'number' both have 'toString'
//x[6] = true; // Error, 'boolean' isn't 'string | number'

//Enum
enum Color1 {Red = 1, Green = 2, Blue = 3};
let c: Color1 = Color1.Green;
console.log(c);
enum Color2 {Red = 1, Green, Blue};
let colorName: string = Color2[2];
console.log(colorName);

//Any
let notSure: any = 4;
notSure = "maybe a string instead";
notSure = false;
let notSure1: any = 4;
//notSure1.ifItExists(); // okay, ifItExists might exist at runtime
notSure1.toFixed(); // okay, toFixed exists (but the compiler doesn't check)
let prettySure: Object = 4;
//prettySure.toFixed(); // Error: Property 'toFixed' doesn't exist on type 'Object'.
let list: any[] = [1, true, "free"];
list[1] = 100;

//Void
function warnUser(): void {
    console.log('This is my warning.');
}
warnUser();
let unusable: void = undefined;

//Null and undefined
let u: undefined = undefined;
let n: null = null;

//Never
// Function returning never must have unreachable end point
function error(message: string): never {
    throw new Error(message);
}
// Inferred return type is never
function fail() {
    return error("Something failed");
}
// Function returning never must have unreachable end point
function infiniteLoop(): never {
    while (true) {
    }
}

//Type assertions
let someValue : any = "This is a string";
let strLength : number = (<string>someValue).length;

let someValue1 : any = "Another string";
let strLength1 : number = (someValue1 as string).length;
console.log(strLength1);

