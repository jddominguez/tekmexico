var isDone = false;
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
console.log(isDone, decimal, hex, binary, octal);
var color = "blue";
color = 'red';
console.log(color);
var fullName = 'Daniel Dominguez';
var age = 30;
var sentence = "Hello, my name is " + fullName + ". I'll be " + (age + 1) + " years old next January.";
console.log(sentence);
var list1 = [1, 2, 3];
var list2 = [1, 2, 3];
console.log(list1, list2);
var x;
x = ["hello", 10];
console.log(x[0].substr(1));
x[3] = "world";
var Color1;
(function (Color1) {
    Color1[Color1["Red"] = 1] = "Red";
    Color1[Color1["Green"] = 2] = "Green";
    Color1[Color1["Blue"] = 3] = "Blue";
})(Color1 || (Color1 = {}));
;
var c = Color1.Green;
console.log(c);
var Color2;
(function (Color2) {
    Color2[Color2["Red"] = 1] = "Red";
    Color2[Color2["Green"] = 2] = "Green";
    Color2[Color2["Blue"] = 3] = "Blue";
})(Color2 || (Color2 = {}));
;
var colorName = Color2[2];
console.log(colorName);
var notSure = 4;
notSure = "maybe a string instead";
notSure = false;
var notSure1 = 4;
notSure1.toFixed();
var prettySure = 4;
var list = [1, true, "free"];
list[1] = 100;
function warnUser() {
    console.log('This is my warning.');
}
warnUser();
var unusable = undefined;
var u = undefined;
var n = null;
function error(message) {
    throw new Error(message);
}
function fail() {
    return error("Something failed");
}
function infiniteLoop() {
    while (true) {
    }
}
var someValue = "This is a string";
var strLength = someValue.length;
var someValue1 = "Another string";
var strLength1 = someValue1.length;
console.log(strLength1);
