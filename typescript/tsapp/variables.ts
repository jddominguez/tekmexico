function f(x) {
    return x;
}

function g() {
    let x = 100;
    return x;
}

let input = [1, 2];
let [first, second] = input;

console.log(input);

let [, secondA, , fourth] = [1, 2, 3, 4];


let o = {
    a1: "foo",
    b: 12,
    c: "bar"
}

let {a1, b} = o;

({a1, b} = {a1: "baz", b: 101});
