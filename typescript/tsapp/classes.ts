class Animal{
    alive: boolean;
    age: number;
    name: string;
    public color: string = 'white';
    private size: string = 'small';
    constructor(name: string, age: number, alive: boolean){
        this.name = name;
        this.age = age;
        this.alive = alive;
    }
    info(){
        return "This animal is a " + this.name + ' of age ' + this.age + ' and looks ' + this.alive;
    }
}

let horse = new Animal("horse", 10, true);
console.log(horse.info());
console.log(horse.age);
console.log(horse.color);
//console.log(horse.size);


let passcode = "secret passcode1";

class Employee {
    private _fullName: string;

    get fullName(): string {
        return this._fullName;
    }

    set fullName(newName: string) {
        if (passcode && passcode == "secret passcode") {
            this._fullName = newName;
        }
        else {
            console.log("Error: Unauthorized update of employee!");
        }
    }
}

let employee = new Employee();
employee.fullName = "Bob Smith";
if (employee.fullName) {
    console.log(employee.fullName);
}
