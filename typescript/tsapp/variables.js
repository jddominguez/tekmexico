function f(x) {
    return x;
}
function g() {
    var x = 100;
    return x;
}
var input = [1, 2];
var first = input[0], second = input[1];
console.log(input);
var _a = [1, 2, 3, 4], secondA = _a[1], fourth = _a[3];
var o = {
    a1: "foo",
    b: 12,
    c: "bar"
};
var a1 = o.a1, b = o.b;
(_b = { a1: "baz", b: 101 }, a1 = _b.a1, b = _b.b, _b);
var _b;
