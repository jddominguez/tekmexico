class Greeter {
    constructor(public greeting: string) { }
    greet() {
        return "<h1>" + this.greeting + "</h1>";
    }
};

var greeter = new Greeter("Hello, world!");

import num from "./MyModule";

console.log(num); // "123"

document.body.innerHTML = greeter.greet();