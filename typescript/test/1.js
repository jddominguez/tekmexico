"use strict";
var Greeter = (function () {
    function Greeter(greeting) {
        this.greeting = greeting;
    }
    Greeter.prototype.greet = function () {
        return "<h1>" + this.greeting + "</h1>";
    };
    return Greeter;
}());
;
var greeter = new Greeter("Hello, world!");
var MyModule_1 = require("./MyModule");
console.log(MyModule_1["default"]); // "123"
document.body.innerHTML = greeter.greet();
