//Boolean
let isDone = false;
//Number
let decimal = 6;
let hex = 0xf00d;
let binary = 0b1010;
let octal = 0o744;
//String
let color = "blue";
color = 'red';
let fullName = `Bob Bobbington`;
let age = 37;
let sentence = `Hello, my name is ${fullName}. I'll be ${age + 1} years old next month.`;
//Array
let list1 = [1, 2, 3];
let list2 = [1, 2, 3];
//Tuple
// Declare a tuple type
let x;
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"]; // Error
console.log(x[0].substr(1)); // OK
//console.log(x[1].substr(1)); // Error, 'number' does not have 'substr'
//Enum
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
;
let c = Color.Green;
//enum Color {Red = 1, Green, Blue};
//let c: Color = Color.Green;
//enum Color {Red = 1, Green = 2, Blue = 4};
//let c: Color = Color.Green;
//enum Color {Red = 1, Green, Blue};
let colorName = Color[2];
alert(colorName);
//Any
let notSure = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean
//let notSure: any = 4;
notSure.ifItExists(); // okay, ifItExists might exist at runtime
notSure.toFixed(); // okay, toFixed exists (but the compiler doesn't check)
let prettySure = 4;
//prettySure.toFixed(); // Error: Property 'toFixed' doesn't exist on type 'Object'.
let list = [1, true, "free"];
list[1] = 100;
//Void
function warnUser() {
    alert("This is my warning message");
}
let unusable = undefined;
// Not much else we can assign to these variables!
let u = undefined;
let n = null;
//Never
// Function returning never must have unreachable end point
function error(message) {
    throw new Error(message);
}
// Inferred return type is never
function fail() {
    return error("Something failed");
}
// Function returning never must have unreachable end point
function infiniteLoop() {
    while (true) {
    }
}
//Type Assertions
let someValue = "this is a string";
let strLength = someValue.length;
//let someValue: any = "this is a string";
//let strLength: number = (someValue as string).length; 
