{
    let x = 3;
}
{
    let x = [0, 1, null];
}
{
}
{
}
{
    window.onmousedown = function (mouseEvent) {
        //console.log(mouseEvent.buton);  //<- Error
    };
}
{
    window.onmousedown = function (mouseEvent) {
        console.log(mouseEvent.buton); //<- Now, no error is given
    };
}
{
    function createZoo() {
        return;
        //return [new Rhino(), new Elephant(), new Snake()];
    }
}
