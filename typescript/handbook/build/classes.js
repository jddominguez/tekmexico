class Animal {
    constructor(name, age, alive) {
        this.color = 'white';
        this.size = 'small';
        this.name = name;
        this.age = age;
        this.alive = alive;
    }
    info() {
        return "This animal is a " + this.name + ' of age ' + this.age + ' and looks ' + this.alive;
    }
}
let horse = new Animal("horse", 10, true);
console.log(horse.info());
console.log(horse.age);
console.log(horse.color);
//console.log(horse.size);
let passcode = "secret passcode1";
class Employee {
    get fullName() {
        return this._fullName;
    }
    set fullName(newName) {
        if (passcode && passcode == "secret passcode") {
            this._fullName = newName;
        }
        else {
            console.log("Error: Unauthorized update of employee!");
        }
    }
}
let employee = new Employee();
employee.fullName = "Bob Smith";
if (employee.fullName) {
    console.log(employee.fullName);
}
