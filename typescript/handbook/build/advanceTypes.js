//Intersection types
{
    function extend(first, second) {
        let result = {};
        for (let id in first) {
            result[id] = first[id];
        }
        for (let id in second) {
            if (!result.hasOwnProperty(id)) {
                result[id] = second[id];
            }
        }
        return result;
    }
    class Person {
        constructor(name) {
            this.name = name;
        }
    }
    class ConsoleLogger {
        log() {
            // ...
        }
    }
    var jim = extend(new Person("Jim"), new ConsoleLogger());
    //var n = jim.name;
    jim.log();
}
//Union types
{
    /**
 * Takes a string and adds "padding" to the left.
 * If 'padding' is a string, then 'padding' is appended to the left side.
 * If 'padding' is a number, then that number of spaces is added to the left side.
 */
    function padLeft(value, padding) {
        if (typeof padding === "number") {
            return Array(padding + 1).join(" ") + value;
        }
        if (typeof padding === "string") {
            return padding + value;
        }
        throw new Error(`Expected string or number, got '${padding}'.`);
    }
    padLeft("Hello world", 4); // returns "    Hello world"
    let indentedString = padLeft("Hello world", true); // passes at compile time, fails at runtime.
}
//Type aliases
{
    function getName(n) {
        if (typeof n === "string") {
            return n;
        }
        else {
            return n();
        }
    }
}
