{
    class Person {
    }
    let p;
    // OK, because of structural typing
    p = new Person();
}
{
    let x;
    // y's inferred type is { name: string; location: string; }
    let y = { name: "Alice", location: "Seattle" };
    x = y;
    function greet(n) {
        alert("Hello, " + n.name);
    }
    greet(y); // OK
}
{
    let x = (a) => 0;
    let y = (b, s) => 0;
    y = x; // OK
}
{
    let items = [1, 2, 3];
    // Don't force these extra parameters
    items.forEach((item, index, array) => console.log(item));
    // Should be OK!
    items.forEach(item => console.log(item));
}
{
    let x = () => ({ name: "Alice" });
    let y = () => ({ name: "Alice", location: "Seattle" });
    x = y; // OK
}
{
    var EventType;
    (function (EventType) {
        EventType[EventType["Mouse"] = 0] = "Mouse";
        EventType[EventType["Keyboard"] = 1] = "Keyboard";
    })(EventType || (EventType = {}));
    function listenEvent(eventType, handler) {
        /* ... */
    }
    // Unsound, but useful and common
    listenEvent(EventType.Mouse, (e) => console.log(e.x + "," + e.y));
    // Undesirable alternatives in presence of soundness
    listenEvent(EventType.Mouse, (e) => console.log(e.x + "," + e.y));
    listenEvent(EventType.Mouse, ((e) => console.log(e.x + "," + e.y)));
}
{
    function invokeLater(args, callback) {
        /* ... Invoke callback with 'args' ... */
    }
    // Unsound - invokeLater "might" provide any number of arguments
    invokeLater([1, 2], (x, y) => console.log(x + ", " + y));
    // Confusing (x and y are actually required) and undiscoverable
    invokeLater([1, 2], (x, y) => console.log(x + ", " + y));
}
{
    var Status;
    (function (Status) {
        Status[Status["Ready"] = 0] = "Ready";
        Status[Status["Waiting"] = 1] = "Waiting";
    })(Status || (Status = {}));
    ;
    var Color;
    (function (Color) {
        Color[Color["Red"] = 0] = "Red";
        Color[Color["Blue"] = 1] = "Blue";
        Color[Color["Green"] = 2] = "Green";
    })(Color || (Color = {}));
    ;
    let status = Status.Ready;
}
{
    class Animal {
        constructor(name, numFeet) { }
    }
    class Size {
        constructor(numFeet) { }
    }
    let a;
    let s;
    a = s; //OK
    s = a; //OK
}
{
    let x;
    let y;
    x = y; // okay, y matches structure of x
}
{
    let x;
    let y;
}
{
    let identity = function (x) {
        // ...
        return;
    };
    let reverse = function (y) {
        // ...
        return;
    };
    identity = reverse; // Okay because (x: any)=>any matches (y: any)=>any
}
