function identity1(arg) {
    return arg;
}
function identity2(arg) {
    return arg;
}
function identity3(arg) {
    return arg;
}
let output1 = identity3("myString"); // type of output will be 'string'
let output2 = identity3("myString");
//Working with generic type values
function identity4(params) {
    return params;
}
function loggingIdentity1(arg) {
    //console.log(arg.length);  // Error: T doesn't have .length
    return arg;
}
function loggingIdentity2(arg) {
    console.log(arg.length); // Array has a .length, so no more error
    return arg;
}
function loggingIdentity3(arg) {
    console.log(arg.length); // Array has a .length, so no more error
    return arg;
}
//Generic types
function identity5(arg) {
    return arg;
}
let myIdentity1 = identity5;
function identity6(arg) {
    return arg;
}
let myIdentity2 = identity6;
function identity7(arg) {
    return arg;
}
let myIdentity3 = identity7;
function identity8(arg) {
    return arg;
}
let myIdentity4 = identity8;
function identity9(arg) {
    return arg;
}
let myIdentity5 = identity9;
//Generic classes
class GenericNumber {
}
let myGenericNumber = new GenericNumber();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };
let stringNumeric = new GenericNumber();
stringNumeric.zeroValue = "";
stringNumeric.add = function (x, y) { return x + y; };
alert(stringNumeric.add(stringNumeric.zeroValue, "test"));
//Generic constrain
function loggingIdentity4(arg) {
    //console.log(arg.length);  // Error: T doesn't have .length
    return arg;
}
function loggingIdentity5(arg) {
    console.log(arg.length); // Now we know it has a .length property, so no more error
    return arg;
}
//loggingIdentity5(3);  // Error, number doesn't have a .length property
loggingIdentity5({ length: 10, value: 3 });
//Using Class Types in Generics
function create(c) {
    return new c();
}
class BeeKeeper {
}
class ZooKeeper {
}
class AnimalType {
}
class Bee extends Animal {
}
class Lion extends Animal {
}
function findKeeper(a) {
    return a.prototype.keeper;
}
//findKeeper(Lion).nametag;  // typechecks!
