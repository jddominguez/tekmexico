function add(x: number, y: number): number {
    return x + y;
}

let myAdd = function (x: number, y: number): number {
    return x + y;
};

console.log(add(1, 1));

console.log(myAdd(2, 2));

let myAdd2: (baseValue: number, increment: number) => number =
    function (x: number, y: number): number { return x + y; };
