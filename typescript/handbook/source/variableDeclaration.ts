//Var declaration
var a = 10;

function f1() {
    var message = "Hello, world!";

    return message;
}

function f2() {
    var a = 10;
    return function g() {
        var b = a + 1;
        return b;
    }
}
var g = f2();
g(); // returns '11'

function f3() {
    var a = 1;

    a = 2;
    var b = g();
    a = 3;

    return b;

    function g() {
        return a;
    }
}
f3(); // returns '2'

//Scoping rules
function f4(shouldInitialize: boolean) {
    if (shouldInitialize) {
        var x = 10;
    }

    return x;
}
f4(true);  // returns '10'
f4(false); // returns 'undefined'
function sumMatrix1(matrix: number[][]) {
    var sum = 0;
    for (var i = 0; i < matrix.length; i++) {
        var currentRow = matrix[i];
        for (var i = 0; i < currentRow.length; i++) {
            sum += currentRow[i];
        }
    }

    return sum;
}

//Variable capturing quirks
for (var i = 0; i < 10; i++) {
    setTimeout(function() { console.log(i); }, 100 * i);
}
for (var i = 0; i < 10; i++) {
    // capture the current state of 'i'
    // by invoking a function with its current value
    (function(i) {
        setTimeout(function() { console.log(i); }, 100 * i);
    })(i);
}

//Let declarations
let hello = "Hello!";

//Block scoping
function f5(input: boolean) {
    let a = 100;

    if (input) {
        // Still okay to reference 'a'
        let b = a + 1;
        return b;
    }

    // Error: 'b' doesn't exist here
    //return b;

    try {
        throw "oh no!";
    }
    catch (e) {
        console.log("Oh well.");
    }
    // Error: 'e' doesn't exist here
    //console.log(e);
}
a++; // illegal to use 'a' before it's declared;
//let a;
function foo() {
    // okay to capture 'a'
    return a;
}
// illegal call 'foo' before 'a' is declared
// runtimes should throw an error here
foo();
//let a;

//Re-declarations and shadowing
function f6(x) {
    var x;
    var x;

    //let x = 10;
    //let x = 20; // error: can't re-declare 'x' in the same scope

    if (true) {
        var x;
    }
}
function f7(x) {
    //let x = 100; // error: interferes with parameter declaration
}
function g1() {
    let x = 100;
    //var x = 100; // error: can't have both declarations of 'x'
}
function f8(condition, x) {
    if (condition) {
        let x = 100;
        return x;
    }

    return x;
}
f8(false, 0); // returns '0'
f8(true, 0);  // returns '100'
function sumMatrix2(matrix: number[][]) {
    let sum = 0;
    for (let i = 0; i < matrix.length; i++) {
        var currentRow = matrix[i];
        for (let i = 0; i < currentRow.length; i++) {
            sum += currentRow[i];
        }
    }

    return sum;
}

//Block-scoped variable capturing
function theCityThatAlwaysSleeps() {
    let getCity;
    if (true) {
        let city = "Seattle";
        getCity = function() {
            return city;
        }
    }
    return getCity();
}
for (let i = 0; i < 10 ; i++) {
    setTimeout(function() { console.log(i); }, 100 * i);
}

//Const declarations
const numLivesForCat1 = 9;
const numLivesForCat2 = 9;
const kitty1 = {
    name: "Aurora",
    numLives: numLivesForCat1,
}

// Error
// kitty1= {
//     name: "Danielle",
//     numLives: numLivesForCat2
// };

// all "okay"
kitty1.name = "Rory";
kitty1.name = "Kitty";
kitty1.name = "Cat";
kitty1.numLives--;

//Array destructuring
{
    let input: [number, number] = [1, 2];
    let [first, second] = input;
    console.log(first); // outputs 1
    console.log(second); // outputs 2
    first = input[0];
    second = input[1];
    // swap variables
    [first, second] = [second, first];
    function f9([first, second]: [number, number]) {
        console.log(first);
        console.log(second);
    }
    f9(input);
}
{
    let [first, ...rest] = [1, 2, 3, 4];
    console.log(first); // outputs 1
    console.log(rest); // outputs [ 2, 3, 4 ]
    let [first1] = [1, 2, 3, 4];
    console.log(first1); // outputs 1
    let [, second, , fourth] = [1, 2, 3, 4];
}

//Object destructuring
//Property renaming
{
    let o = {
        a: "foo",
        b: 12,
        c: "bar"
    }
    let {a, b} = o;
    ({a, b} = {a: "baz", b: 101});
    let {a: newName1, b: newName2} = o;

    //let newName1 = o.a;
    //let newName2 = o.b;
}

//Default values
{
    function keepWholeObject(wholeObject: { a: string, b?: number }) {
        let {a, b = 1001} = wholeObject;
    }   
}

//Function declarations
{
    type C = { a: string, b?: number }
    function f10({a, b}: C): void {
        // ...
    }
}
{
    function f11({a, b} = { a: "", b: 0 }): void {
        // ...
    }
    f11(); // ok, default to {a: "", b: 0}
}
{
    function f12({a, b = 0} = { a: "" }): void {
        // ...
    }
    f12({ a: "yes" }) // ok, default b = 0
    f12() // ok, default to {a: ""}, which then defaults b = 0
    //f12({}) // error, 'a' is required if you supply an argument
}