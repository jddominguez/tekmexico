function printLabel(labelledObject: { label: string }){
    console.log(labelledObject.label);
}

let myObj = { size: 10, label: "Size 10 object!" };
printLabel(myObj);

//Example 1
interface SquareConfig{
    color?: string;
    width?: number;
}
function createSquare(config: SquareConfig): { color: string; area: number }{
    let newSquare = { color: "white", area: 100 };
    if(config.color){
        newSquare.color = config.color;
    }
    if(config.width){
        newSquare.area = config.width * config.width;
    }
    return newSquare;
}
let mySquare = createSquare({color: 'black'});

//Example 2
interface Point {
    readonly x: number;
    readonly y: number;
}
let p1: Point = { x: 10, y: 20 };
//p1.x = 5; // error!

let a1: number[] = [1, 2, 3, 4];
//let ro: ReadonlyArray<number> = a;
//ro[0] = 12; // error!
//ro.push(5); // error!
//ro.length = 100; // error!
//a = ro; // error!
//a1 = ro as number[];


//Excess property checks
// interface SquareConfig {
//     color?: string;
//     width?: number;
// }

// function createSquare1(config: SquareConfig): { color: string; area: number } {
//     return true;
// }

// let mySquare1 = createSquare1({ colour: "red", width: 100 }); //FAILS! misspelled
