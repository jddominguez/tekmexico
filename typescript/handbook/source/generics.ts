function identity1(arg: number): number {
    return arg;
}
function identity2(arg: any): any {
    return arg;
}
function identity3<T>(arg: T): T {
    return arg;
}
let output1 = identity3<string>("myString");  // type of output will be 'string'
let output2 = identity3("myString");

//Working with generic type values
function identity4<T>(params: T): T {
    return params;
}
function loggingIdentity1<T>(arg: T): T {
    //console.log(arg.length);  // Error: T doesn't have .length
    return arg;
}
function loggingIdentity2<T>(arg: T[]): T[] {
    console.log(arg.length);  // Array has a .length, so no more error
    return arg;
}
function loggingIdentity3<T>(arg: Array<T>): Array<T> {
    console.log(arg.length);  // Array has a .length, so no more error
    return arg;
}

//Generic types
function identity5<T>(arg: T): T {
    return arg;
}
let myIdentity1: <T>(arg: T) => T = identity5;

function identity6<T>(arg: T): T {
    return arg;
}
let myIdentity2: <U>(arg: U) => U = identity6;

function identity7<T>(arg: T): T {
    return arg;
}
let myIdentity3: {<T>(arg: T): T} = identity7;

interface GenericIdentityFn1 {
    <T>(arg: T): T;
}
function identity8<T>(arg: T): T {
    return arg;
}
let myIdentity4: GenericIdentityFn1 = identity8;

interface GenericIdentityFn2<T> {
    (arg: T): T;
}
function identity9<T>(arg: T): T {
    return arg;
}
let myIdentity5: GenericIdentityFn2<number> = identity9;

//Generic classes
class GenericNumber<T> {
    zeroValue: T;
    add: (x: T, y: T) => T;
}
let myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function(x, y) { return x + y; };

let stringNumeric = new GenericNumber<string>();
stringNumeric.zeroValue = "";
stringNumeric.add = function(x, y) { return x + y; };

alert(stringNumeric.add(stringNumeric.zeroValue, "test"));

//Generic constrain
function loggingIdentity4<T>(arg: T): T {
    //console.log(arg.length);  // Error: T doesn't have .length
    return arg;
}
interface Lengthwise {
    length: number;
}
function loggingIdentity5<T extends Lengthwise>(arg: T): T {
    console.log(arg.length);  // Now we know it has a .length property, so no more error
    return arg;
}
//loggingIdentity5(3);  // Error, number doesn't have a .length property
loggingIdentity5({length: 10, value: 3});

//Using Class Types in Generics
function create<T>(c: {new(): T; }): T {
    return new c();
}
class BeeKeeper {
    hasMask: boolean;
}

class ZooKeeper {
    nametag: string;
}

class AnimalType {
    numLegs: number;
}

class Bee extends Animal {
    keeper: BeeKeeper;
}

class Lion extends Animal {
    keeper: ZooKeeper;
}

function findKeeper<A extends Animal, K> (a: {new(): A;
    prototype: {keeper: K}}): K {

    return a.prototype.keeper;
}
//findKeeper(Lion).nametag;  // typechecks!
