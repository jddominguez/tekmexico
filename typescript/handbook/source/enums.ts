enum Direction {
    Up = 1,
    Down,
    Left,
    Right
}

enum FileAccess {
    // constant members
    None,
    Read    = 1 << 1,
    Write   = 1 << 2,
    ReadWrite  = Read | Write,
    // computed member
    G = "123".length
}

enum Enum1 {
    A
}

let a1 = Enum1.A;
let nameOfA1 = Enum1[Enum.A]; // "A"

var Enum2;

(function (Enum) {
    Enum2[Enum2["A"] = 0] = "A";
})(Enum2 || (Enum2 = {}));

var a2 = Enum2.A;
var nameOfA2 = Enum2[Enum2.A]; // "A"

const enum Enum {
    A = 1,
    B = A * 2
}

const enum Directions {
    Up,
    Down,
    Left,
    Right
}

let directions1 = [Directions.Up, Directions.Down, Directions.Left, Directions.Right];

var directions2 = [0 /* Up */, 1 /* Down */, 2 /* Left */, 3 /* Right */];


declare enum Enum3 {
    A = 1,
    B,
    C = 2
}