var obj = {
  val: 0,
  context: this,
  a: function(){
    console.log(true);
    this.b(2);
  },
  b: function(a){
    this.val = a;
    console.log(this.val);
  }
};

function b(){
  console.log(true);
}

console.log(obj.val);
obj.a();
