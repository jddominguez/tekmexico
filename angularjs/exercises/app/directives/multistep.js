app.directive('multistepComponent', function($templateRequest, $compile){
  return{
    scope:{
      subscription: '=',
      index: '@',
      form: "=data",
      update: '&'
    },
    controller: function($scope){
      
    },
    link: function(scope, element, attrs){

      element.append(angular.element('<h1>').text(scope.form.title));

      var tabs = angular.element('<ul>').addClass('nav nav-tabs nav-justified').attr('role', 'tablist');

      var panels = (angular.element('<div>').addClass('tab-content'));

      scope.form.steps.forEach(function (steps, index) {

        var currentElement = steps.title.toLowerCase().split(' ').join('-');

        tabs.append(angular.element('<li>').attr('role', 'presentation').addClass(index === 0 ? 'active' : '').append(angular.element('<a>').text(steps.title).attr({ 'target':'_self', 'href':'#'+currentElement, 'aria-controls':currentElement, 'data-toggle':'tab' })));

        var panel = angular.element('<div>').attr({ 'role':'tabpanel', 'id':currentElement }).addClass('tab-pane' + (index === 0 ? ' active' : ''));

        panel.append(angular.element('<hr>'));

        steps.fields.forEach(function(inputs, index){
          // panel.append(angular.element('<label>').text(inputs.label));
          var inputName = 'input' + inputs.label.toLowerCase().split(' ').join('');
          if(inputs.type){
            panel.append(angular.element('<input>').attr({ 'name':inputName, 'type':inputs.type, 'ng-model':'model'+inputName, 'placeholder': inputs.label }).addClass('form-control').prop('required', inputs.required ? true : false));
          }
          panel.append(angular.element('<p>').attr('ng-bind', 'model'+inputName));
          panel.append(angular.element('<div>').addClass('alert alert-danger').attr('ng-show', 'myForm.'+ inputName  +'.$error.required').text('Required!'));
        });

        panels.append(panel);

      });

      var nav = angular.element('<nav>').attr('aria-label', '...')
      .append(angular.element('<ul>').addClass('pager')
      .append(angular.element('<li>').addClass('previous disabled').append(angular.element('<a>').attr('target', '_self').attr('href', '').text('Back')))
      .append(angular.element('<li>').addClass('next disabled').append(angular.element('<a>').attr('target', '_self').attr('href', '').text('Next')))
    );

    element.append($compile(angular.element(tabs))(scope));
    element.append($compile(angular.element('<form>').attr('name', 'myForm').append(angular.element(panels)))(scope));
    //element.append($compile(angular.element(nav))(scope));

    console.log(scope.myForm);

    console.log(scope.myForm.$dirty);

  },
  restrict: 'EACM'
}
});
