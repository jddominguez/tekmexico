app.directive("spreadsheetComponent", function(){
  return {
    scope: {
      subscription: '=',
      index: '@',
      tableData: "=tabledata",
      update: '&'
    },
    controller: function($scope){

      //console.log(currentData);

      $scope.formatData = function(currentData){

        $scope.parsedTableData = {
          labels:[],
          datasets:[]
        };

        if(currentData[0][0] == ""){
          $scope.parsedTableData.labels = currentData[0];
          $scope.parsedTableData.labels.shift();

          for (var i = 1; i < currentData.length; i++) {
            var dataset = {
              label: "",
              data: []
            };
            for (var j = 0; j < currentData[i].length; j++) {
              if(j === 0){
                dataset.label = currentData[i][j];
              }
              else{
                dataset.data.push(currentData[i][j]);
              }
            }
            $scope.parsedTableData.datasets.push(dataset);
          }
        }
        return $scope.parsedTableData;
      };
    },
    link: function(scope, element, attrs){

      //setup spreadsheet element
      scope.table = angular.element("<div>");
      element.append(scope.table);
      scope.hot = new Handsontable(scope.table[0], {
        data: scope.tableData,
        rowHeaders: true,
        colHeaders: true,
        width: 800,
        height: 400,
      });

      scope.hot.updateSettings({
        contextMenu:{
          callback: function(key, options){
            if(key === 'send'){
              var selection = this.getSelected();
              if(selection){
                var currentData = this.getData(selection[0],selection[1],selection[2],selection[3]);
                scope.$emit('spreadsheetScope', scope.formatData(currentData)); // send data to mainCtrl
              }
              else{
                console.log("No data selected!");
              }
            }
          },
          items:{
            'send': {name : "Send To Chart"}
          }
        }
      });





    },
    restrict: "EACM"
  }
});
