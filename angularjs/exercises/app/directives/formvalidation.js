app.directive("formvalidationComponent", function($compile, $templateRequest){
  return{
    scope: {
      subscription: '=',
      index: '@',
      config: "=config",
      update: '&',
    },
    scope: function($scope){

    },
    link: function(scope, element, attrs){

      $templateRequest("templates/formvalidation.html").then(function(html){
        element.append($compile(angular.element(html))(scope));
      });

      scope.validatePassword = function(){
        if(scope.passwordInputA === scope.passwordInputB && (scope.passwordInputA != "" || scope.passwordInputB != "")){
          $('input[type="password"]').removeClass('alert-danger').addClass('alert-success');
        }
        else {
          $('input[type="password"]').addClass('alert-danger').removeClass('alert-success');
        }
      }

      scope.date = new Date(2010, 1, 28);
      scope.number = 1;

      // scope.inputNumber = 0;

      scope.checkType = function(type, value){
        if(type === 'number'){
          if(!isNaN(value)){
            $('input[name="inputNumber"]').removeClass('alert-danger');
          }
          else{
            $('input[name="inputNumber"]').addClass('alert-danger');
          }
        }
      }

      scope.inputAlphabetic = "Hello";

    },
    restrict: 'EACM',
    template: '<h1>Form Validation</h1>'
  }
});
