app.directive('galleryComponent', function(){
  return{
    scope:{
      subscription: '=',
      index: '@',
      source: "=source",
      update: '&'
    },
    controller: function($scope, $http, $element){
      console.log("directive controller!" + $scope.$id);

      $element.append(angular.element('<div>').attr('id', 'gallery-' + $scope.$id).attr('style', 'height:400px; overflow:scroll; overflow-x:hidden; background-color:#FFF;"'));
      $('#gallery-'+$scope.$id).append(angular.element('<div class="spinner">').hide());
      $('#gallery-'+$scope.$id+'>.spinner').fadeIn(250);

      $scope.getImageArray = function(){
        $scope.promise = $http.get($scope.source.url);
        $scope.promise.then(
          function(response){
            console.log(response, $scope.$id);
            $('#gallery-'+$scope.$id+' > .spinner').fadeOut(250);
            $scope.fullImageArray = response;
            for( ; $scope.source.currentIndex < $scope.source.offsetImages; $scope.source.currentIndex++){
              $scope.req = {
                method: 'GET',
                url: 'https://unsplash.it/'+$scope.source.imageSize+'?image=' + $scope.fullImageArray.data[$scope.source.currentIndex].id,
                responseType: "blob",
                data: {
                  id: $scope.source.currentIndex
                }
              };

              $scope.image = angular.element('<img>').attr('id', $scope.source.currentIndex + '-' + $scope.$id).hide();
              $('#gallery-' + $scope.$id).append($scope.image);
              $scope.imagePromise = $http($scope.req);
              $scope.imagePromise.then(function(response){
                //console.log(response);
                $scope.urlCreator = window.URL || window.webkitURL;
                $scope.imageUrl = $scope.urlCreator.createObjectURL(response.data);
                $('#' + response.config.data.id + '-' + $scope.$id).attr('src', $scope.imageUrl).fadeIn(1000);
              },function(error){
                console.log(false);
              });
            }
          },
          function(){
            $('#gallery-'+$scope.$id+' > .spinner').fadeOut(250);
            $('#gallery-'+$scope.$id).append(angular.element('<h3>').text('Failed to load images!').addClass('text-center'));
            console.log("Fail!");
          }
        );
      };

      $scope.getImageArray();

      $scope.updateGallery = function(direction){

        if(direction === 'top' && $scope.source.currentIndex - $scope.source.offsetImages >= $scope.source.loadingImages){

          console.log("top");

          var startIndex = $scope.source.currentIndex - $scope.source.offsetImages;

          for (var i = startIndex - 1; i >= startIndex - $scope.source.loadingImages ; i--) {
            console.log(i);
            $scope.req = {
              method: 'GET',
              url: 'https://unsplash.it/'+$scope.source.imageSize+'?image=' + $scope.fullImageArray.data[i].id,
              responseType: "blob",
              data: {
                id: i
              }
            };

            $scope.image = angular.element('<img>').attr('id', i + '-' + $scope.$id).hide();
            $('#gallery-' + $scope.$id).prepend($scope.image);
            $scope.imagePromise = $http($scope.req);
            $scope.imagePromise.then(function(response){
              console.log(response);
              $('#' + (response.config.data.id - $scope.source.offsetImages) + '-' + $scope.$id).remove();
              $scope.urlCreator = window.URL || window.webkitURL;
              $scope.imageUrl = $scope.urlCreator.createObjectURL(response.data);
              $('#' + response.config.data.id + '-' + $scope.$id).attr('src', $scope.imageUrl).fadeIn(1000);
            },function(error){
              console.log(false);
            });
          }
        }

        if(direction === 'bottom'){
          console.log("bottom");
          for ( var i = $scope.source.currentIndex; $scope.source.currentIndex < $scope.source.loadingImages + i; $scope.source.currentIndex++) {
            $scope.req = {
              method: 'GET',
              url: 'https://unsplash.it/'+$scope.source.imageSize+'?image=' + $scope.fullImageArray.data[$scope.source.currentIndex].id,
              responseType: "blob",
              data: {
                id: $scope.source.currentIndex
              }
            };
            $scope.image = angular.element('<img>').attr('id', $scope.source.currentIndex + '-' + $scope.$id).hide();
            $('#gallery-' + $scope.$id).append($scope.image);
            $scope.imagePromise = $http($scope.req);
            $scope.imagePromise.then(function(response){
              $('#' + (response.config.data.id - $scope.source.offsetImages) + '-' + $scope.$id).remove();
              $scope.urlCreator = window.URL || window.webkitURL;
              $scope.imageUrl = $scope.urlCreator.createObjectURL(response.data);
              $('#' + response.config.data.id + '-' + $scope.$id).attr('src', $scope.imageUrl).fadeIn(1000);
            },function(error){
              console.log(false);
            });
          }
        }

      }
    },

    link: function(scope, element, attrs){
      console.log("directive link!");

      //detect top or bottom scrolling
      $(document).ready(function () {
        $('#gallery-' + scope.$id).bind('scroll', chk_scroll);
      });
      function chk_scroll(e) {
        var elem = $(e.currentTarget);
        //console.log(elem[0].scrollHeight - elem.scrollTop(), elem.outerHeight());
        if (elem[0].scrollHeight - elem.scrollTop() <= elem.outerHeight()){
          scope.updateGallery("bottom");
        }
        if (elem.scrollTop() === 0) {
          scope.updateGallery("top");
        }
      }
    },
    restrict: 'EACM',
    template: ''
  }
});
