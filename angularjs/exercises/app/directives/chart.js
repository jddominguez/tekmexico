app.directive("chartComponent", function(){
  return {
    scope: {
      subscription: '=',
      index: '@',
      chart: "=chartdata",
      update: '&'
    },
    controller: function($scope){
      $scope.updateChart = function(currentType){
        $scope.chart.currentType = currentType;
        if($scope.myChart){
          $scope.myChart.destroy();
        }
        $scope.chart.data = $scope.chart.data;
        $scope.myChart = new Chart($scope.canvas, {
          type: $scope.chart.currentType,
          data: $scope.chart.data,
          options: $scope.chart.options
        });
      };
    },
    link: function(scope, element, attrs){
      if(scope.chart){
        scope.canvas = angular.element("<canvas>");
        element.append(scope.canvas);
        scope.myChart = new Chart(scope.canvas, {
          type: scope.chart.currentType,
          data: scope.chart.data,
          options: scope.chart.options
        });
      }
      else{
        scope.valid = true;
        // element.append(angular.element("p").text("Invalid chart data!").addClass("text-center text-danger"));
      }
    },
    restrict: "EACM",
    // templateUrl: "directives/chart-component.html",
    template: '<select ng-model="scope.chart.currentType"'+
    'ng-options="option as option for option in chart.types"'+
    'ng-change="updateChart(scope.chart.currentType)"'+
    'class="form-control" ng-hide="valid">'+
    '<option value="" disabled="">Chart Type</option>'+
    '</select>',
  }
});
