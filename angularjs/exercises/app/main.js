var app = angular.module("exampleApp", ["ngRoute", "ngSanitize"]);

app.config(function($routeProvider, $locationProvider){

  // $locationProvider.html5Mode(true).hashPrefix('!');

  $routeProvider.when("/chart",{
    templateUrl: "views/chart.html",
     title : 'My Charts',
  });

  $routeProvider.when("/spreadsheet",{
    templateUrl: "views/spreadsheet.html",
    title: "My Spreadsheet"
  });

  $routeProvider.when("/gallery",{
    templateUrl: "views/gallery.html",
    title: "My Gallery"
  });

  $routeProvider.when("/form-validation",{
    templateUrl: "views/formvalidation.html",
    title: "A Form Validation"
  });

  $routeProvider.when("/multi-step-form",{
    templateUrl: "views/multistepform.html",
    title: "A Multi-step Form"
  });

  $routeProvider.when("/text-spoiler",{
    templateUrl: "views/textspoiler.html",
    title: "My Text Spoiler"
  });

  $routeProvider.when("/ui-router",{
    templateUrl: "views/uirouter.html",
    title: "My UI Router"
  });

  $routeProvider.when("/notes",{
    templateUrl: "views/notes.html",
    title: "My Notes"
  });



  $routeProvider.otherwise({
    templateUrl: "views/home.html",
    title: "AngularJS App"
  });

});

//function that performs no operations, expects a callback? fixed the rendering issue of a ngview inside nginclude
app.run(['$route', angular.noop]);

//Apply routeprovider title property
app.run(['$rootScope', '$route', function($rootScope, $route) {
  $rootScope.$on('$routeChangeSuccess', function() {
    document.title = $route.current.title;
    $rootScope.title = $route.current.title;
  });
}]);


app.controller("mainCtrl", function($scope){

  // $scope.tableData1 = [
  //   ["", "Ford", "Volvo", "Toyota", "Honda"],
  //   ["2016", 10, 11, 12, 13],
  //   ["2017", 20, 11, 14, 13],
  //   ["2018", 30, 15, 12, 13]
  // ];

  $scope.tableData1 = Handsontable.helper.createSpreadsheetData(10,15);



  $scope.$on('spreadsheetScope', function (event, data) {

    $scope.tableData = data;

    //console.log($scope.tableData);

    $scope.chart3 = {
      types: ['bar', 'pie', 'line'],
      currentType: 'bar',
      data:{
        labels: $scope.tableData.labels,
        datasets: $scope.tableData.datasets
      },
      options: {
        animation:{
          duration: 0
        },
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }]
        }
      }
    };


  });

  $scope.name = "Hello";
  $scope.app = {
    name: "AngularJS App",
    title: "My App",
    version: 0.1
  };

  //charts data
  $scope.chart = {
    types: ["bar","pie", "line"],
    currentType: "line",
    data:{
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "My First dataset",
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1,
          data: [65, 59, 80, 81, 56, 55, 40],
        }
      ]
    },
    options: {
      animation:{
        duration: 0
      },
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }
  }

  //charts data
  $scope.chart2 = {
      types: ["bar","pie", "line"],
      currentType: "bar",
      data:{
        labels: ["January", "February", "March"],
        datasets: [
          {
            label: "My First dataset",
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            data: [65, 59, 80],
          }
        ]
      },
      options: {
        animation:{
          duration: 0
        },
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }]
        }
      }
  }


  //gallery data
  $scope.gallerySource = {
    url: "https://unsplash.it/list",
    imageSize: 200,
    numerOfImages: 0,
    offsetImages: 20,
    currentIndex: 0,
    loadingImages: 5,
  };
  $scope.gallerySource2 = {
    url: "https://unsplash.it/list",
    imageSize: 100,
    numerOfImages: 0,
    offsetImages: 100,
    currentIndex: 0,
    loadingImages: 20,
  };





  //multistep configuration

  $scope.form1 = {
    title: "My Custom Form",
    steps: [
      {
        title: "Personal Info",
        fields: [
          { label: "Name", type: "text", required: true },
          { label: "Last Name", type: "text", required: true },
          { label: "Age", type: "number", required: true }
        ]
      },
      {
        title: "Address",
        fields: [
          { label: "Street", type: "text", required: false },
          { label: "City", type: "text", required: false },
        ]
      },
      {
        title: "Contact",
        fields: [
          { label: "Phone Number", type: "tel", required: true },
          { label: "Email", type: "email", required: true },
        ]
      }
    ]
  };

  $scope.someText = '<p>In an African desert millions of years ago, a tribe of man-apes faces starvation and competition for a water hole by a rival tribe. They awaken to find a featureless black monolith has appeared before them. Guided in some fashion by the Black Monolith, one man-ape realizes how to use a bone as a tool and weapon; the tribe learns to hunt for food, and kills the leader of their rivals, reclaiming the water hole.</p>' +
  '<p>Millions of years later, a Pan Am space plane carries Dr. Heywood Floyd to a space station orbiting Earth for a layover on his trip to Clavius Base, a United States outpost on the moon. After a videophone call with his daughter, Floyds Soviet scientist friend and her colleague ask about rumors of a mysterious epidemic at Clavius. Floyd declines to answer. At Clavius, Floyd heads a meeting of base personnel, apologizing for the epidemic cover story but stressing secrecy. His mission is to investigate a recently found artifact buried four million years ago. Floyd and others ride in a Moonbus to the artifact, which is a monolith identical to the one encountered by the man-apes. Sunlight strikes the monolith and a loud high-pitched radio signal is heard.</p>' +
  '<p>Eighteen months later, the United States spacecraft Discovery One is bound for Jupiter. On board are mission pilots and scientists Dr. David Bowman and Dr. Frank Poole along with three other scientists in cryogenic hibernation. Most of Discoverys operations are controlled by the ships computer, HAL 9000, referred to by the crew as "Hal". Bowman and Poole watch Hal and themselves being interviewed on a BBC show about the mission, in which the computer states that he is "foolproof and incapable of error." When asked by the host if Hal has genuine emotions, Bowman replies that he appears to, but that the truth is unknown. Later, when Bowman questions Hal on the purpose of the mission, Hal responds by reporting the imminent failure of an antenna control device. The astronauts retrieve the component making use of an EVA pod but find nothing wrong with it. Hal suggests reinstalling the part and letting it fail so the problem can be found. Mission Control advises the astronauts that results from their twin HAL 9000 indicate that Hal is in error. Hal insists that the problem, like previous issues ascribed to HAL series units, is due to human error. Concerned about Hals behavior, Bowman and Poole enter an EVA pod to talk without Hal overhearing, and agree to disconnect Hal if he is proven wrong. Hal secretly follows their conversation by lip reading.</p>';

});
