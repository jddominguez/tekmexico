app.directive("authComponent", function($templateRequest, $compile, $window, $rootScope){
  return{
    // scope:{
    //   subscription: '=',
    //   index: '@',
    //   params: "=params",
    //   update: '&'
    // },

    controller: function($scope){
      //console.log("authComponent controller!");
      $scope.login = {
        email: "",
        password: ""
      }

      $scope.register = {
        email: "",
        password: ""
      }

      $scope.getUserStatus().then(
        function(response){
          $window.location.href = '#/account';
        },
        function(error){
        }
      );

    },
    link: function(scope, element, attrs){
      $('.loading').hide();


        $templateRequest("directives/auth.html").then(function(html){
          element.append($compile(angular.element(html))(scope));
        });


      //register promise
      scope.registerFn = function(){
        $('.loading').fadeIn(100);
        var registerPromise = firebase.auth().createUserWithEmailAndPassword(scope.register.email, scope.register.password);
        registerPromise.then(
          function(user){
            $('.loading').fadeOut(100);
          },
          function(error){
            $('.loading').fadeOut(100);
            scope.displayResponse(false, error.message);
          }
        );
      }

      scope.resetPasswordFn = function(){
        $('.loading').fadeIn(100);
        var resetPromise = firebase.auth().sendPasswordResetEmail(scope.login.email);
        resetPromise.then(
          function(response){
            $('.loading').fadeOut(100);
            scope.displayResponse(true, 'Check your email!');
          },
          function(error){
            $('.loading').fadeOut(100);
            scope.displayResponse(false, error.message);
          }
        );
      }

      //login promise
      scope.loginFn = function(){
        $('.loading').fadeIn(100);
        var loginPromise = firebase.auth().signInWithEmailAndPassword(scope.login.email, scope.login.password);
        loginPromise.then(
          function(user){
            $rootScope.currentUser = user;
            $('.loading').fadeOut(100);
            $window.location.href = '#/account';
          },
          function(error){
            $('.loading').fadeOut(100);
            scope.displayResponse(false, error.message);

          }
        );
      }

      // scope.logoutFn = function(){
      //   $('.loading').fadeIn(100);
      //   var signoutPromise = firebase.auth().signOut();
      //   signoutPromise.then(
      //     function(response){
      //       //add login form but keep loading div.
      //       $('.loading').fadeOut(100);
      //       $templateRequest("directives/auth.html").then(function(html){
      //         element.append($compile(angular.element(html))(scope));
      //       });
      //     },
      //     function(error){
      //       $('.loading').fadeOut(100);
      //     }
      //   );
      // }

      scope.displayResponse = function(type, msg){
        if(type === true){
          $('#alert').empty().append(angular.element('<div class="alert alert-success">'));
          $('.alert').text(msg);
        }
        if(type === false){
          $('#alert').empty().append(angular.element('<div class="alert alert-danger">'));
          $('.alert').text(msg);
        }
      }
    },
    restrict: "EACM",
    template: '',
  }
});
