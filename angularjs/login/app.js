var app = angular.module("mainApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider){

  $routeProvider.when("/account",{
    // resolve:{
    //   access: function(Access){
    //     console.log("Requesting resolve...");
    //     return Access.isAuthenticated();
    //   }
    // },
    templateUrl: "views/account.html",
    title: 'My Account'
  });

  $routeProvider.otherwise({
    templateUrl: "views/auth/login.html",
    title: 'Login',
  });

});

//function that performs no operations, expects a callback? fixed the rendering issue of a ngview inside nginclude
//Apply routeprovider title property
app.run(function($rootScope, $route, $location) {
  angular.noop;
  $rootScope.$on('$routeChangeSuccess', function() {
    document.title = $route.current.title;
    $rootScope.title = $route.current.title;
  });
});


// app.factory("Access", function($q, $location){
//   return{
//     isAuthenticated: function(){
//       $('.loading').fadeIn(500);
//       var defer = $q.defer();
//       firebase.auth().onAuthStateChanged(function(user){
//         if(user){
//           return defer.resolve(user);
//         }
//         else{
//           console.log("No access!");
//           $('.loading').fadeOut(500);
//           return defer.reject($location.path('/'));
//         }
//       });
//       //return defer.promise;
//     }
//   }
// });

app.controller("mainCtrl", function($rootScope, $scope, $location, $route) {

  //console.log($route);


  $scope.$on('routeChangeStart', function(event, current, previous, rejection){
    console.log("routeChangeStart");
  });

  $scope.$on('routeChangeSuccess', function(event, current, previous, rejection){
    console.log("routeChangeSuccess");
  });


  //$scope.logged = {};

  $scope.app = {
    title: "FireBase",
    name: "FireBase"
  };

  $scope.$on('$locationChangeStart', function(event, newUrl, oldurl){

    console.log($location.url());

    $scope.getUserStatus().then(
      function(response){
      },
      function(error){
        if(error && newUrl != 'http://192.168.10.200:3001/login/')
        $location.path("/");
      }
    );

  });

  // $scope.$on('$routeChangeStart', function(angularEvent, newUrl, currentUrl) {
  //   if(newUrl.requireAuth){
  //     $scope.getUserStatus().then(
  //       function(response){
  //         $('.loading').fadeOut(500);
  //         console.log(response);
  //         $scope.logged = response;
  //         console.log("user is logged in...");
  //       },
  //       function(error){
  //         console.log("redirecting to login...");
  //         $('.loading').fadeOut(500);
  //         $location.path("/");
  //       }
  //     );
  //   }
  //
  // });

  $scope.getUserStatus = function(){
    return new Promise(function(resolve, reject){
      firebase.auth().onAuthStateChanged(function(user){
        if(user){
          resolve(user);
        }
        else{
          reject(true);
        }
      });
    });
  };

  $scope.init = function(){
    $scope.getUserStatus().then(
      function(response){
        $scope.currentUser = response;
        $scope.$apply();
        $('.loading').fadeOut(500);
        console.log(response);
        $location.path("/account");
      },
      function(error){
        $('.loading').fadeOut(500);
        console.log("user is not auth!");
      }
    );
  }

  $scope.login = {
    email: "",
    password: ""
  }

  $scope.register = {
    email: "",
    password: ""
  }

  $scope.loginFn = function(){
    $('.loading').fadeIn(500);
    var loginPromise = firebase.auth().signInWithEmailAndPassword($scope.login.email, $scope.login.password);
    loginPromise.then(
      function(user){
        $scope.logged = user;
         //$scope.$apply();
        $('.loading').fadeOut(500);
        console.log($scope.logged);
        //return $location.path('/account');
        $location.path("/account");
      },
      function(error){
        $('.loading').fadeOut(500);
        $scope.displayResponse(false, error.message);
      }
    );
  }

  $scope.logoutFn = function(){
    $('.loading').fadeIn(500);
    var signoutPromise = firebase.auth().signOut();
    signoutPromise.then(
      function(response){
        $scope.logged = {};
        $('.loading').fadeOut(500);
        $location.path("/");
      },
      function(error){
        $('.loading').fadeOut(500);
        console.log(error);
        $location.path("/");
      }
    );
  };

  $scope.$on('$viewContentLoaded', function(event) {
    //console.log('load!');
    //$('.loading').fadeOut(500);
  });

});

//
// app.controller("accountCtrl", function(){
//
//   var storageRef = firebase.storage().ref("image.jpg");
//
//   console.log(storageRef);
//   console.log(true);
//
//   $('#fileUpload').on('change', function(event){
//     console.log(event);
//     var firstFile = this.files[0]; // get the first file uploaded
//     var uploadTask = storageRef.put(firstFile);
//     var progressBar = $('#progressbar');
//     uploadTask.on('state_changed', function progress(snapshot) {
//
//       var currentValue = (snapshot.b/snapshot.h)*500;
//       progressBar.text(currentValue + "%");
//       progressBar.attr('aria-valuenow', currentValue);
//       progressBar.css('width', currentValue + '%');
//
//       console.log(snapshot); // progress of upload
//     });
//   });
//
//
// });
