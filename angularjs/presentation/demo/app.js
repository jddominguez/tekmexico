var app = angular.module('exampleApp', []);

app.controller('defaultCtrl', function($scope){

  $scope.app = {
    title: "My App"
  };

  $scope.loginFn = function(){
    var loginPromise = firebase.auth().signInWithEmailAndPassword($scope.email, $scope.password);
    loginPromise.then(
      function(user){
        console.log(user);
        $('form').remove();
        $('#container').append(angular.element('<div>').addClass('alert alert-success').text(user.uid));
        $('#uploadContainer').removeClass('hidden');
      },
      function(error){
        console.log(error);
        $('#alert').empty().append(angular.element('<div>').addClass('alert alert-danger').text(error.message));
      }
    );


  }

});


app.controller('uploadCtrl', function($scope){

  $('input:file').on('change', function(){

    var storageRef = firebase.storage().ref($scope.fileName);
    var uploadTask = storageRef.put(this.files[0]);
    var progressBar = $('#progressbar');

    uploadTask.on('state_changed', function progress(response) {
      var currentValue = Math.floor((response.b/response.h)*100);
      progressBar.text(currentValue + "%").attr('aria-valuenow', currentValue).css('width', currentValue + '%');
      console.log(response);
    });

  });



});
