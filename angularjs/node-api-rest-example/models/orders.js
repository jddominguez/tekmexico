exports = module.exports = function(app, mongoose) {

	var orderSchema = new mongoose.Schema({
		name: 		{ type: String },
		streetAddress: 		{ type: String },
		city:  	{ type: String },
		state:  	{ type: String },
		zipCode:  	{ type: String },
		country:  	{ type: String },
		wrapItems: { type: Boolean }
	});

	mongoose.model('Order', productSchema);
};
