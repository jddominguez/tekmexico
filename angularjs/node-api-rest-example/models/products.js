exports = module.exports = function(app, mongoose) {

	var productSchema = new mongoose.Schema({
		name: 		{ type: String },
		price: 		{ type: Number },
		description:  	{ type: String },
		category: 		{
			type: String,
			enum: ['Water Sports', 'Soccer', 'Chess']
		}
	});

	mongoose.model('Product', productSchema);
};
