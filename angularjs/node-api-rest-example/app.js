var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    mongoose        = require('mongoose');

// Connection to DB
mongoose.connect('mongodb://localhost/products', function(err, res) {
  if(err) throw err;
  console.log('Connected to Database!');
});

app.use(function(req, res, next){ // Allow CORS
  if(req.method === "OPTIONS"){
    var headers = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "POST, GET, PUT, OPTIONS, DELETE",
      "Access-Control-Allow-Credentials": false,
      "Access-Control-Max-Age": '864000', // 24 hours
      "Access-Control-Allow-Headers": "X-Requested-Width, X-HTTP-Method-Override, Content-Type, Accept, Authorization, myheader"
    };
    res.writeHead(204, headers);
    return res.end();
  }else{
    res.set('Access-Control-Allow-Origin', '*');
    return next();
  }
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Import Models and controllersAPP.JS
var models     = require('./models/products', './models/orders')(app, mongoose);
var ProductCtrl = require('./controllers/products');

// Example Route
var router = express.Router();

// router.get('/', function(req, res) {
//   res.send("Hello world!");
// });


app.get('/', function(req, res){
  res.render("index.html");
});

app.use(router);

// API routes
var Products = express.Router();

Products.route('/products')
  .get(ProductCtrl.findAllProducts)
  .post(ProductCtrl.addProduct);

Products.route('/products/:id')
  .get(ProductCtrl.findById)
  .put(ProductCtrl.updateProduct)
  .delete(ProductCtrl.deleteProduct);

app.use('/api', Products);

// Start server
app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});
