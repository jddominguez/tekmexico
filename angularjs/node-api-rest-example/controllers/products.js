//File: controllers/Products.js
var mongoose = require('mongoose');
var Product  = mongoose.model('Product');

//GET - Return all Products in the DB
exports.findAllProducts = function(req, res) {
	Product.find(function(err, products) {
		if(err)
			res.send(500, err.message);
		console.log('GET /products')
		res.status(200).jsonp(products);
	});
};

//GET - Return a Product with specified ID
exports.findById = function(req, res) {
	Product.findById(req.params.id, function(err, product) {
		if(err) return res.send(500, err.message);
		console.log('GET /products/' + req.params.id);
		res.status(200).jsonp(product);
	});
};

//POST - Insert a new Product in the DB
exports.addProduct = function(req, res) {
	console.log('POST');
	console.log(req.body);
	var product = new Product({
		name:    req.body.name,
		price: 	  req.body.price,
		description:  req.body.description,
		category:    req.body.category,
	});
	product.save(function(err, product) {
		if(err) return res.send(500, err.message);
		res.status(200).jsonp(product);
	});
};

//PUT - Update a register already exists
exports.updateProduct = function(req, res) {
	Product.findById(req.params.id, function(err, product) {
		console.log(product);
		product.name = req.body.name;
		product.price = req.body.price;
		product.description = req.body.description;
		product.category = req.body.category;
		console.log(product);
		product.save(function(err) {
			console.log(product);
			if(err)
				return res.send(500, err.message);
			res.status(200).jsonp(product);
		});
	});
};

//DELETE - Delete a Product with specified ID
exports.deleteProduct = function(req, res) {
	Product.findById(req.params.id, function(err, product) {
		console.log(err);
		console.log(product);
		product.remove(function(err) {
			if(err){
				return res.send(500, err.message);
			}
			else {
				res.status(200).end(); //Deprecated, use below instead
				//res.sendStatus(200);
			}
		})
	});
};
