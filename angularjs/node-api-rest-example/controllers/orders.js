//File: controllers/Orders.js
var mongoose = require('mongoose');
var Order  = mongoose.model('Order');

//GET - Return all Orders in the DB
exports.findAllOrders = function(req, res) {
	Order.find(function(err, orders) {
    if(err) res.send(500, err.message);

    console.log('GET /orders')
		res.status(200).jsonp(orders);
	});
};

//GET - Return a Order with specified ID
exports.findById = function(req, res) {
	Order.findById(req.params.id, function(err, order) {
    if(err) return res.send(500, err.message);

    console.log('GET /orders/' + req.params.id);
		res.status(200).jsonp(order);
	});
};

//POST - Insert a new Order in the DB
exports.addOrder = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var order = new Order({
		name:    req.body.name,
		streetAddress: 	  req.body.streetAddress,
		city:  req.body.city,
		state:    req.body.state,
		zipCode:    req.body.zipCode,
		country:    req.body.country,
		wrapItems:    req.body.wrapItems
	});

	order.save(function(err, order) {
		if(err) return res.send(500, err.message);
    res.status(200).jsonp(order);
	});
};

//PUT - Update a register already exists
exports.updateOrder = function(req, res) {
	Order.findById(req.params.id, function(err, order) {
		Order.name   = req.body.petId;
		Order.price    = req.body.price;
		Order.description = req.body.description;
		Order.category   = req.body.category;

		Order.save(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200).jsonp(order);
		});
	});
};

//DELETE - Delete a Order with specified ID
exports.deleteOrder = function(req, res) {
	Order.findById(req.params.id, function(err, order) {
		order.remove(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200);
		})
	});
};
