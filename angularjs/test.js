var n = ["Veronica", "Alex", "Michael", "Harry", "Dave", "Michael", "Victor", "Harry", "Alex", "Mary", "Mary", "Veronica"]

var votes = {};
var ordered = {};

for (var i = 0; i < n.length; i++) {
  votes[n[i]] = votes[n[i]] ? votes[n[i]] + 1 : 1;
}

Object.keys(votes).sort().forEach( function(key) {
  ordered[key] = votes[key];
});

var winner = Object.keys(ordered).reduce(
  function(a, b){
    return ordered[a] > ordered[b] ? a : b;
  }
);

console.log(winner);
