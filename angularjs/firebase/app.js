var app = angular.module("mainApp", ["firebase"]);

app.controller("mainCtrl", function($scope, $firebaseObject, $firebaseAuth) {

  $scope.app = {
    title: 'FireBase'
  }

  $scope.authObj = $firebaseAuth();

  $scope.init = function(){
    console.log("Application started...");
  };

  $scope.authObj.$signInWithEmailAndPassword("eontool@gmail.com", "123456789").then(function(firebaseUser) {

    console.log("Signed in as:", firebaseUser.uid);

    // var ref = firebase.database().ref().child("data");
    // var syncObject = $firebaseObject(ref);
    // syncObject.$bindTo($scope, "data");

    $scope.message = {
      title: "Note 1",
      text: "Some text!"
    };

    var messages = firebase.database().ref().child("notes");
    var syncMessages = $firebaseObject(messages);
    syncMessages.$bindTo($scope, "message");

  }).catch(function(error) {
    console.error("Authentication failed:", error);
  });

});
