describe("First Test", function(){

  //Arrange (set up scenario)
  var counter;

  beforeEach(() => {
    counter = 0;
  });

  it('increment value', function () {
    //Act (attempt the operation)
    counter++;
    //Assert (verify the result)
    expect(counter).toEqual(1);
  });

  it('decrements value', function () {
    //Act (attempt the operation)
    counter--;
    //Assert (verify the result)
    expect(counter).toEqual(-1);
  });

});
