var app = angular.module('mainApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

  var viewPath = 'views/';

  var signinState = {
    name: 'signin',
    url: '/signin',
    fileName: 'auth/signin.html',
    resolve: {
      loadTemplate: ["LoadTemplate", function(LoadTemplate){ return LoadTemplate.template(viewPath + signinState.fileName); }]
    },
    templateUrl: function(){return viewPath + signinState.fileName; },
  };

  var registerState = {
    name: 'register',
    url: '/register',
    fileName: 'auth/register.html',
    resolve: {
      loadTemplate: ["LoadTemplate", function(LoadTemplate){ return LoadTemplate.template(viewPath + registerState.fileName); }]
    },
    templateUrl: function(){return viewPath + registerState.fileName; },
  };

  var accountState = {
    name: 'account',
    url: '/account',
    fileName: 'account/account.html',
    resolve: {
      access: ["Access", function (Access) { return Access.auth(); }],
      loadTemplate: ["LoadTemplate", function(LoadTemplate){ return LoadTemplate.template(viewPath + accountState.fileName); }]
    },
    templateUrl: function(){return viewPath + accountState.fileName; },
    //template: '<h1>My Account</h1>'
  };

  $urlRouterProvider.otherwise('/signin');

  $stateProvider.state(signinState);
  $stateProvider.state(registerState);
  $stateProvider.state(accountState);

});

app.factory("LoadTemplate", function($templateCache){
  return{
    template: function(path){
      console.log(path);
      return new Promise(
        function(resolve, reject){
          var template = $templateCache.get(path);
          if (template){
            resolve(template);
            $('.loading').fadeOut(500);
            $('.container').fadeIn(500);
          }
          else {
            reject("Template loading failed!");
          }
        }
      );
    }
  }
});

app.factory("Access", ['$rootScope', '$location', function (rootScope, location) {
  return{
    auth: function(){
      return new Promise(function(resolve, reject){
        firebase.auth().onAuthStateChanged(function(user){
          if(user){
            location.path('/account');
            resolve(user);
          }
          else{
            location.path('/login');
            reject("no access!");
          }
        });
        console.log("end!");
      });
    },
    // logged: function(){
    //   console.log("/login");
    //   return new Promise(function(resolve, reject){
    //     firebase.auth().onAuthStateChanged(function(user){
    //       if(user){
    //         location.path('/account');
    //         rootScope.$apply();
    //         console.log("user is logged!");
    //         //resolve();
    //       }
    //       else{
    //         location.path('/login');
    //         rootScope.$apply();
    //         //reject("no access!");
    //       }
    //     });
    //   });
    // }
  }
}]);


app.controller('mainCtrl', ['$rootScope', '$scope', '$location', function(rootScope, scope, location){

  scope.getUserStatus = function(){
    return new Promise(function(resolve, reject){
      firebase.auth().onAuthStateChanged(function(user){
        if(user){
          resolve(user);
        }
        else{
          reject(true);
        }
      });
    });
  };

  scope.init = function(){
    scope.getUserStatus().then(
      function(response){
        console.log("user is authorized!");
        location.path('/account');
        rootScope.$apply();
      },
      function(error){
        console.log("user is not auth!");
      }
    );
  }

  scope.registerFn = function(){
    var registerPromise = firebase.auth().createUserWithEmailAndPassword(this.email, this.passwordA);
    registerPromise.then(
      function(user){
        console.log(user);
        location.path('/account');
        rootScope.$apply();
      },
      function(error){
        console.log(error);
      }
    );
  }

  scope.logoutFn = function(){
    $('.loading').fadeIn(100);
    var signoutPromise = firebase.auth().signOut();
    signoutPromise.then(
      function(response){
        console.log(response);
        //add login form but keep loading div.
        $('.loading').fadeOut(100);
        location.path('/login');
        rootScope.$apply();
      },
      function(error){
        console.log(error);
        $('.loading').fadeOut(100);
        location.path('/login');
        rootScope.$apply();
      }
    );
  }

  scope.loginProvider = function(email, password){
    console.log(email, password);
    console.log("login user...");
    return new Promise(function(resolve, reject){
      firebase.auth().signInWithEmailAndPassword(email, password).then(
        function(){
          resolve("access!");
        },
        function(){
          reject("error!");
        }
      )
    });
  }


  scope.loginFn = function(){
    scope.loginProvider(this.signin.email, this.signin.password).then(
      function(response){
        console.log(response);
        location.path('/account');
        rootScope.$apply();
      },
      function(error){
        console.log(error);
        $('form').append(angular.element('<p>').text(error));
      }
    )

    // var loginPromise = firebase.auth().signInWithEmailAndPassword("eontool@gmail.com", "123456789");
    // loginPromise.then(
    //   function(user){
    //     console.log(user);
    //   },
    //   function(error){
    //     console.log(error);
    //   }
    // );



  }


  scope.loginGithub = function(){
    var provider = new firebase.auth.GithubAuthProvider();

    firebase.auth().signInWithRedirect(provider);

    firebase.auth().getRedirectResult().then(function(result) {
      if (result.credential) {
        console.log(result);
        // This gives you a GitHub Access Token. You can use it to access the GitHub API.
        var token = result.credential.accessToken;
        // ...
      }
      // The signed-in user info.
      var user = result.user;
    }).catch(function(error) {
      console.log(error);
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });

  };

  scope.app = {
    title: 'Mi Libreta'
  }

}]);
