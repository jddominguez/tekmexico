var express = require('express');
var app = express();

app.use('/', express.static(__dirname + '/'));

// app.use(function(req, res, next){ // Allow CORS
//   if(req.method === "OPTIONS"){
//     var headers = {
//       "Access-Control-Allow-Origin": "*",
//       "Access-Control-Allow-Methods": "POST, GET, PUT, OPTIONS, DELETE",
//       "Access-Control-Allow-Credentials": false,
//       "Access-Control-Max-Age": '864000', // 24 hours
//       "Access-Control-Allow-Headers": "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization, myheader"
//     };
//     res.writeHead(204, headers);
//     return res.end();
//   }else{
//     res.set('Access-Control-Allow-Origin', '*');
//     return next();
//   }
// });

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3001, function () {
  console.log('Listening on port 3001!');
});
