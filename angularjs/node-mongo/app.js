var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    mongoose        = require('mongoose');


//app.use('/', express.static(__dirname + '/'));

// Connection to DB
mongoose.connect('mongodb://localhost/', function(err, res) {
  if(err) throw err;
  console.log('Connected to Database!');
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());


// API routes
var Products = express.Router();

Products.route('/')

  .get(
    function(req, res) {
  		res.status(200).jsonp("[{}]");
  	});





// Start server
app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});
