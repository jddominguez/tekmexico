var app = angular.module("exampleApp", ["increment", "ngResource"]);

app.constant("baseUrl", "http://192.168.10.200:3000/api/products/");

app.controller("defaultCtrl", function($scope, $http, $resource, baseUrl){

  $scope.displayMode = "list";
  $scope.currentProducts = null;

  $scope.productsResource = $resource(
    baseUrl + ":id",
    { id: "@_id" },
    { create: { method: "POST" }, save: { method: "PUT" }}
  );

  $scope.listProducts = function(){

    $scope.products = $scope.productsResource.query();

    // $http.get(baseUrl).success(function(data){
    //   $scope.products = data;
    // });

    // $scope.products = [
    //   { id:0, name:"Dummy1", category:"Test", price:1.25 },
    //   { id:1, name:"Dummy2", category:"Test", price:2.25 },
    //   { id:2, name:"Dummy3", category:"Test", price:3.25 },
    //   { id:3, name:"Dummy4", category:"Test", price:4.25 }
    // ];
  }

  $scope.deleteProduct = function(product){

    product.$delete().then(function () {
      $scope.products.splice($scope.products.indexOf(product), 1);
    });
    $scope.displayMode = "list";
    // $http({
    //   method: "DELETE",
    //   url: baseUrl + product._id
    // }).success(function(){
    //   $scope.products.splice($scope.products.indexOf(product),1);
    // }).error(function(error){
    //   console.log(error);
    // });
  }

  $scope.createProduct = function(product){

    new $scope.productsResource(product).$create().then(function(newProduct){
      $scope.products.push(newProduct);
      $scope.displayMode = "list";
    });

    // $http.post(baseUrl, product).success(function(newProduct){
    //   console.log("Creating product...");
    //   $scope.products.push(newProduct);
    //   $scope.displayMode = "list";
    // });
  }

  $scope.updateProduct = function(product){

    product.$save();

    $scope.displayMode = "list"

    // $http({
    //   url: baseUrl + product._id,
    //   method: "PUT",
    //   data: product
    // }).success(function(modifiedProduct){
    //   console.log("Updating products...");
    //   for (var i = 0; i < $scope.products.length; i++) {
    //     if($scope.products[i]._id == modifiedProduct._id){
    //       $scope.products[i] = modifiedProduct;
    //       break;
    //     }
    //   }
    //   $scope.displayMode = "list";
    // });
  }

  $scope.editOrCreateProduct = function(product){
    // $scope.currentProduct = product ? angular.copy(product) : {};
    $scope.currentProduct = product ? product : {};
    $scope.displayMode = "edit";
  }

  $scope.saveEdit = function(product){
    if (angular.isDefined(product._id)) {
      $scope.updateProduct(product)
    }
    else {
      $scope.createProduct(product);
    }
  }

  $scope.cancelEdit = function(){
    // $scope.currentProduct = {};
    // $scope.displayMode = "list";
    if ($scope.currentProduct && $scope.currentProduct.$get) {
      $scope.currentProduct.$get();
    }
    $scope.currentProduct = {};
    $scope.displayMode = "list";
  }

  $scope.listProducts();

});
