{
  class Foo {
    constructor(a,b) {
      this.x = a;
      this.y = b;
    }
    gimmeXY() {
      return this.x * this.y;
    }
  }
}

{
  function Foo(a,b) {
    this.x = a;
    this.y = b;
  }
  Foo.prototype.gimmeXY = function() {
    return this.x * this.y;
  }
}


{
  var f = new Foo( 5, 15 );
  f.x;
  f.y;
  f.gimmeXY();
}


{
  class Bar extends Foo {
    constructor(a,b,c) {
      super( a, b );
      this.z = c;
    }
    gimmeXYZ() {
      return super.gimmeXY() * this.z;
    }
  }
  var b = new Bar( 5, 15, 25 );
  b.x;
  b.y;
  b.z;
  b.gimmeXYZ();
}

{
  class ParentA {
    constructor() { this.id = "a"; }
    foo() { console.log( "ParentA:", this.id ); }
  }
  class ParentB {
    constructor() { this.id = "b"; }
    foo() { console.log( "ParentB:", this.id ); }
  }
  class ChildA extends ParentA {
    foo() {
      super.foo();
      console.log( "ChildA:", this.id );
    }
  }
  class ChildB extends ParentB {
    foo() {
      super.foo();
      console.log( "ChildB:", this.id );
    }
  }
  var a = new ChildA();
  a.foo();
  var b = new ChildB();
  b.foo();
}

//subclass constructor
//pre es6
{
  function Foo() {
    this.a = 1;
  }
  function Bar() {
    this.b = 2;
    Foo.call( this );
  }
  // `Bar` "extends" `Foo`
  Bar.prototype = Object.create( Foo.prototype );
}

{
  class Foo {
    constructor() { this.a = 1; }
  }
  class Bar extends Foo {
    constructor() {
      this.b = 2;
      super();
    }
  }
}

//extending natives
{
  class MyCoolArray extends Array {
    first() { return this[0]; }
    last() { return this[this.length - 1]; }
  }
  var a = new MyCoolArray( 1, 2, 3 );
  a.length;
  a; // 3
  // [1,2,3]
  a.first();
  a.last();
}


//new target
{
  class Foo {
    constructor() {
      console.log( "Foo: ", new.target.name );
    }
  }
  class Bar extends Foo {
    constructor() {
      super();
      console.log( "Bar: ", new.target.name );
    }
    baz() {
      console.log( "baz: ", new.target );
    }
  }
  var a = new Foo();
  // Foo: Foo
  var b = new Bar();
  // Foo: Bar <-- respects the `new` call-site
  // Bar: Bar
  b.baz();
  // baz: undefined
}

//static
{
  class Foo {
    static cool() { console.log( "cool" ); }
    wow() { console.log( "wow" ); }
  }
  class Bar extends Foo {
    static awesome() {
      super.cool();
      console.log( "awesome" );
    }
    neat() {
      super.wow();
      console.log( "neat" );
    }
  }
  Foo.cool();
  Bar.cool();
  Bar.awesome();
  var b = new Bar();
  b.neat();
  b.awesome;
  b.cool;
}

//symbol.species constructor getter
{
  class MyCoolArray extends Array {
    static get [Symbol.species]() { return Array; }
  }
  var a = new MyCoolArray( 1, 2, 3 ),
  b = a.map( function(v){ return v * 2; } );
  b instanceof MyCoolArray;
  b instanceof Array;
}

{
  class Foo {// defer `species` to derived constructor
    static get [Symbol.species]() { return this; }
    spawn() {
      return new this.constructor[Symbol.species]();
    }
  }
  class Bar extends Foo {// force `species` to be parent constructor
    static get [Symbol.species]() { return Foo; }
  }
  var a = new Foo();
  var b = a.spawn();
  b instanceof Foo; // true
  var x = new Bar();
  var y = x.spawn();
  y instanceof Bar;
  y instanceof Foo;
}
