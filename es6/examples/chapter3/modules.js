//old way
{
  function Hello(name){
    function greeting(){
      console.log("Hello " + name + "!");
    }
    //Public API
    return {
      greeting: greeting
    };
  }
  var me = Hello("Daniel");
  me.greeting();
}


{
  var me = (function Hello(name){
    function greeting() {
      console.log( "Hello " + name + "!" );
    }
    // public API
    return {
      greeting: greeting
    };
  })( "Jose" );
  me.greeting();
}

//es6 new way
{
  export function foo() {

  }
  export var awesome = 42;
  var bar = [1,2,3];
  export { bar };
}

{
  function foo() { }
  export { foo as bar };
}

{
  var awesome = 42;
  export { awesome };
  awesome = 100;
}

{
  function foo(..) {
  }
  export default foo;
}

{
  function foo(..) {
  }
  export { foo as default };
}

{
  export default function foo(..) {
  }
}

{
  function foo(..) {
  }
  export { foo as default };
}

{
  export default {
    foo() { .. },
    bar() { .. },
  };
}

//discouraged pattern
{
  export default function foo() { .. }
  foo.bar = function() { .. };
  foo.baz = function() { .. };
}

//better
{
  export default function foo() { .. }
  export function bar() { .. }
  export function baz() { .. }
}

{
  function foo() { .. }
  function bar() { .. }
  function baz() { .. }
  export { foo as default, bar, baz, .. };
}

{
  var foo = 42;
  export { foo as default };
  export var bar = "hello world";
  foo = 10;
  bar = "cool";
}

{
  export { foo, bar } from "baz";
  export { foo as FOO, bar as BAR } from "baz";
  export * from "baz";
}

//importing API members

{
  import { foo, bar, baz } from "foo";
}

{
  import { foo } from "foo";
  foo();
}

{
  import { foo as theFooFunc } from "foo";
  theFooFunc();
}

{
  import foo from "foo";
  // or:
  import { default as foo } from "foo";
}

{
  export default function foo() { .. }
  export function bar() { .. }
  export function baz() { .. }
}

{
  import FOOFN, { bar, baz as BAZ } from "foo";
  FOOFN();
  bar();
  BAZ();
}

{
  export function bar() { .. }
  export var x = 42;
  export function baz() { .. }
}

{
  import * as foo from "foo";
  foo.bar();
  foo.x;
  foo.baz();
}

{
  export default function foo() { .. }
  export function bar() { .. }
  export function baz() { .. }
}

{
  import foofn, * as hello from "world";
  foofn();
  hello.default();
  hello.bar();
  hello.baz();
}

{
  import foofn, * as hello from "world";
  foofn = 42;// (runtime) TypeError!
  hello.default = 42;// (runtime) TypeError!
  hello.bar = 42;
  hello.baz = 42;// (runtime) TypeError!
}

{
  foo();
  import { foo } from "foo";
}

//circular module dependency
{
  import bar from "B";
  export default function foo(x) {
    if (x > 10) return bar( x - 1 );
    return x * 2;
  }
}

{
  import foo from "A";
  export default function bar(y) {
    if (y > 5) return foo( y / 2 );
    return y * 3;
  }
}


{
  import foo from "foo";
  import bar from "bar";
  foo( 25 );
  bar( 25 );
}


//module loading
{
  // normal script loaded in browser via `<script>`,
  // `import` is illegal here
  Reflect.Loader.import( "foo" ) // returns a promise for `"foo"`
  .then( function(foo){
    foo.bar();
  } );
}

//customized loading
{
  Reflect.Loader.import( "foo", { address: "/path/to/foo.js" } )
  .then( function(foo){
    // ..
  } )
}
