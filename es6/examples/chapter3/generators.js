{
  function* foo(){

  }
}

{
  var a = {
    *foo(){}
  };
}

{
  function *foo(x,y) {
  }
  foo( 5, 10 );
}

{
  function *foo() {
    var x = 10;
    var y = 20;
    yield;
    var z = x + y;
  }
}

{
  function *foo() {
    while (true) {
      yield Math.random();
    }
  }
}

{
  function *foo() {
    var x = yield 10;
    console.log( x );
  }
}

{
  function *foo() {
    var arr = [ yield 1, yield 2, yield 3 ];
    console.log( arr, yield 4 );
  }
}

{
  var a, b;
  a = 3;
  //b = 2 + a = 3;
  b = 2 + (a = 3);
  //yield 3;
  //a = 2 + yield 3;
  //a = 2 + (yield 3);
}

{
  function *foo() {
    yield *[1,2,3];
  }
}

{
  function *foo() {
    yield 1;
    yield 2;
    yield 3;
    return 4;
  }
  function *bar() {
    var x = yield *foo();
    console.log( "x:", x );
  }
  for (var v of bar()) {
    console.log( v );
  }
}

{
  function *foo(x) {
    if (x < 3) {
      x = yield *foo( x + 1 );
    }
    return x * 2;
  }
  foo( 1 );
}

//iterator control
{
  function *foo(x) {
    if (x < 3) {
      x = yield *foo( x + 1 );
    }
    return x * 2;
  }
  var it = foo( 1 );
  it.next();
}

{
  function *foo() {
    yield 1;
    yield 2;
    yield 3;
  }
}

{
  function *foo() {
    yield 1;
    yield 2;
    yield 3;
  }
  var it = foo();
  it.next();
  it.next();
  it.next();
  it.next();
}

{
  function *foo() {
    var x = yield 1;
    var y = yield 2;
    var z = yield 3;
    console.log( x, y, z );
  }
}

{
  var it = foo();
  it.next();
  it.next( "foo" );
  it.next( "bar" );
  it.next( "baz" );
}

{
  function *foo() {
    try {
      yield 1;
      yield 2;
      yield 3;
    }
    finally {
      console.log( "cleanup!" );
    }
  }
  for (var v of foo()) {
    console.log( v );
  }
  var it = foo();
  it.next();
  it.return( 42 );
}

{
  function *foo() {
    yield 1;
    yield 2;
    yield 3;
  }
  var it1 = foo();
  it1.next(); // { value: 1, done: false }
  it1.next(); // { value: 2, done: false }

  var it2 = foo();
  it2.next(); // { value: 1, done: false }
  it1.next(); // { value: 3, done: false }
  it2.next(); // { value: 2, done: false }
  it2.next(); // { value: 3, done: false }

  it2.next();
  it1.next();
}

{
  function *foo() {
    yield 1;
    yield 2;
    yield 3;
  }
  var it = foo();
  it.next();// { value: 1, done: false }
  try {
    it.throw( "Oops!" );
  }
  catch (err) {
    console.log( err ); // Exception: Oops!
  }
  it.next();// { value: undefined, done: true }
}

//error handling
{

  yield 2;
  throw "Hello!";
}
var it = foo();
it.next();
try {
  it.throw( "Hi!" );
  it.next();
  console.log( "never gets here" );
}
catch (err) {
  console.log( err ); // Hello!
}
yield 2;
throw "Hello!";
}
var it = foo();
it.next();
try {
  it.throw( "Hi!" );
  it.next();
  console.log( "never gets here" );
}
catch (err) {
  console.log( err ); // Hello!
}

}


{
  function *foo() {
    try {
      yield 1;
    }
    catch (err) {
      console.log( err );
    }
    //yield 2;
    throw "foo: e2";
  }
  function *bar() {
    try {
      yield *foo();
      console.log( "never gets here" );
    }
    catch (err) {
      console.log( err );
    }
  }
  var it = bar();
  try {
    it.next();
  }
  it.throw( "e1" ); // e1
  it.next(); // foo: e2
}
catch (err) {
  console.log( "never gets here" );
}
it.next();

}

//transpiling a generator
{
  function *foo() {
    var x = yield 42;
    console.log( x );
  }
}

{
  function foo() {
    return {
      next: function(v) {
      }
    };
  }
}

{
  function foo() {
    function nextState(v) {
      switch (state) {
        case 0:
        state++;
        // the `yield` expression
        return 42;
        case 1:
        state++;
        // `yield` expression fulfilled
        x = v;
        console.log( x );
        // the implicit `return`
        return undefined;
        // no need to handle state `2`
      }
    }
    var state = 0, x;
    return {
      next: function(v) {
        var ret = nextState( v );
        return { value: ret, done: (state == 2) };
      }
      // we'll skip `return(..)` and `throw(..)`
    };
  }

  var it = foo();
  it.next(); // { value: 42, done: false }
  it.next( 10 );
}
