{
  var arr = [1,2,3];
  var it = arr[Symbol.iterator]();
  it.next();
  it.next();
  it.next();
  it.next();
}

// next() iteration
{
  var greeting = "hello";
  var it = greeting[Symbol.iterator]();
  console.log(it.next());
  console.log(it.next());
  console.log(it.next());
  console.log(it.next());
  console.log(it.next());
  console.log(it.next());
}

{
  var m = new Map();
  m.set( "foo", 42 );
  m.set( { cool: true }, "hello world" );
  var it1 = m[Symbol.iterator]();
  var it2 = m.entries();
  it1.next(); // { value: [ "foo", 42 ], done: false }
  it2.next(); // { value: [ "foo", 42 ], done: false }
}

//iterator loop
// {
//   var it = {
//     // make the `it` iterator an iterable
//     [Symbol.iterator]() { return this; },
//     next() { }
//   };
//
//   it[Symbol.iterator]() === it;
//
//   for (var v of it) {
//     console.log( v );
//   }
//
//   for (var v, res; (res = it.next()) && !res.done; ) {
//     v = res.value;
//     console.log( v );
//   }
// }


//custom iterators
{
  var Fib = {
    [Symbol.iterator]() {
      var n1 = 1, n2 = 1;
      return {
        // make the iterator an iterable
        [Symbol.iterator]() { return this; },
        next() {
          var current = n2;
          n2 = n1;
          n1 = n1 + current;
          return { value: current, done: false };
        },
        return(v) {
          console.log("Fibonacci sequence abandoned.");
          return { value: v, done: true };
        }
      };
    }
  };
  for (var v of Fib) {
    console.log( v );
    if (v > 50) break;
  }

}


{
  var tasks = {
    [Symbol.iterator]() {
      var steps = this.actions.slice();
      return {
        // make the iterator an iterable
        [Symbol.iterator]() { return this; },
        next(...args) {
          if (steps.length > 0) {
            let res = steps.shift()( ...args );
            return { value: res, done: false };
          }
          else {
            return { done: true }
          }
        },
        return(v) {
          steps.length = 0;
          return { value: v, done: true };
        }
      };
    },
    actions: []
  };
}


{
  tasks.actions.push(
    function step1(x){
      console.log( "step 1:", x );
      return x * 2;
    },
    function step2(x,y){
      console.log( "step 2:", x, y );
      return x + (y * 2);
    },
    function step3(x,y,z){
      console.log( "step 3:", x, y, z );
      return (x * y) + z;
    }
  );
  var it = tasks[Symbol.iterator]();
  it.next( 10 ); // step 1: 10 { value: 20, done: false }
  it.next( 20, 50 ); // step 2: 20 50  { value: 120, done: false }
  it.next( 20, 50, 120 ); // step 3: 20 50 120  { value: 1120, done: false }
  it.next(); // { done: true }
}


{
  if (!Number.prototype[Symbol.iterator]) {
    Object.defineProperty(
      Number.prototype,
      Symbol.iterator,
      {
        writable: true,
        configurable: true,
        enumerable: false,
        value: function iterator(){
          var i, inc, done = false, top = +this;
          // iterate positively or negatively?
          inc = 1 * (top < 0 ? -1 : 1);
          return {
            // make the iterator itself an iterable!
            [Symbol.iterator](){ return this; },
            next() {
              if (!done) {
                // initial iteration always 0
                if (i == null) {
                  i = 0;
                }
                // iterating positively
                else if (top >= 0) {
                  i = Math.min(top,i + inc);
                }
                // iterating negatively
                else {
                  i = Math.max(top,i + inc);
                }
                // done after this iteration?
                if (i == top) done = true;
                return { value: i, done: false };
              }
              else {
                return { done: true };
              }
            }
          };
        }
      }
    );
  }
}


{
  for (var i of 3) {
    console.log( i );
  }
  // 0 1 2 3
  [...-3];
}

//iterator consumption

{
  var a = [1,2,3,4,5];
  function foo(x,y,z,w,p) {
    console.log( x + y + z + w + p );
  }
  foo( ...a );
}

{
  var b = [ 0, ...a, 6 ];
  b;
}

{
  var it = a[Symbol.iterator]();
  var [x,y] = it; //take just the first two elements from `it`
  var [z, ...w] = it; // take the third, then the rest all at once is `it` fully exhausted? Yep.
  it.next(); // { value: undefined, done: true }
  x;
  y;
  z;
  w;
}
