{
  if (!Number.isNaN) {
    Number.isNaN = function(x) {
      return x !== x;
    };
  }
}

{
  try {
    a = () => {};
    ARROW_FUNCS_ENABLED = true;
  }
  catch (err) {
    ARROW_FUNCS_ENABLED = false;
  }
}

{
  try {
    new Function( "( () => {} )" );
    ARROW_FUNCS_ENABLED = true;
  }
  catch (err) {
    ARROW_FUNCS_ENABLED = false;
  }
}


//featureTests.io
