{
  function daz() {
    // ..
  }
  var obj = {
    foo: function() {
      // ..
    },
    bar: function baz() {
      // ..
    },
    bam: daz,
    zim() {
      // ..
    }
  };
}


{
  function foo(cb) {
    // what is the name of `cb()` here?
  }
  foo( function(){
    // I'm anonymous!
  } );
}

{
  function foo(i) {
    if (i < 10) return foo( i * 2 );
    return i;
  }
}

//inferences
{
  var abc = function() {
    // ..
  };
  abc.name; // "abc"
}

{
  (function(){ }); // name:
  (function*(){ }); // name:
  window.foo = function(){  }; // name:

  class Awesome {
    constructor() {  }
    funny() {  }
  }
  var c = class Awesome { };
  var o = {
    foo() {  },
    *bar() {  },
    baz: () => { },
    bam: function(){ },
    get qux() { },
    set fuz() { },
    ["b" + "iz"]:
      function(){ },
    [Symbol( "buz" )]:
      function(){  }
  };

  var x = o.foo.bind( o );
  (function(){ }).bind( o );
  export default function() {  }
  var y = new Function();
  var GeneratorFunction =
  function*(){}.__proto__.constructor;
  var z = new GeneratorFunction();
}

//meta properties
{
  class Parent {
    constructor() {
      if (new.target === Parent) {
        console.log( "Parent instantiated" );
      }
      else {
        console.log( "A child instantiated" );
      }
    }
  }
  class Child extends Parent {}
  var a = new Parent();
  var b = new Child();
}
