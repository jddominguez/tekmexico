//symbol.iterator
{
  var arr = [4,5,6,7,8,9];
  for (var v of arr) {
    console.log( v );
  }
  // 4 5 6 7 8 9
  // define iterator that only produces values
  // from odd indexes
  arr[Symbol.iterator] = function*() {
    var idx = 1;
    do {
      yield this[idx];
    } while ((idx += 2) < this.length);
  };
  for (var v of arr) {
    console.log( v );
  }
  // 5 7 9
}

//symbol.toStringTag symbol.hasInstance
{
  function Foo() {}
  var a = new Foo();
  a.toString();  // [object Object]
  a instanceof Foo;  // true
}

{
  function Foo(greeting) {
    this.greeting = greeting;
  }
  Foo.prototype[Symbol.toStringTag] = "Foo";
  Object.defineProperty( Foo, Symbol.hasInstance, {
    value: function(inst) {
      return inst.greeting == "hello";
    }
  } );
  var a = new Foo( "hello" ),
  b = new Foo( "world" );
  b[Symbol.toStringTag] = "cool";
  a.toString(); // [object Foo]
  String( b );  // [object cool]
  a instanceof Foo; // true
  b instanceof Foo; // false
}

//symbols.species
{
  class Cool {
    // defer `@@species` to derived constructor
    static get [Symbol.species]() { return this; }
    again() {
      return new this.constructor[Symbol.species]();
    }
  }
  class Fun extends Cool {}
  class Awesome extends Cool {
    // force `@@species` to be parent constructor
    static get [Symbol.species]() { return Cool; }
  }
  var a = new Fun(),
  b = new Awesome(),
  c = a.again(),
  d = b.again();

  c instanceof Fun; // true
  d instanceof Awesome;  // false
  d instanceof Cool;  // true
}

//symbol.toPrimitive
{
  var arr = [1,2,3,4,5];
  arr + 10;
  // 1,2,3,4,510
  arr[Symbol.toPrimitive] = function(hint) {
    if (hint == "default" || hint == "number") {
      // sum all numbers
      return this.reduce( function(acc,curr){
        return acc + curr;
      }, 0 );
    }
  };
  arr + 10;// 25
}

//regular expression symbols


//symbol.isConcatSpreadable
{
  var a = [1,2,3],
  b = [4,5,6];
  b[Symbol.isConcatSpreadable] = false;
  [].concat( a, b );// [1,2,3,[4,5,6]]
}

//symbols.unscopables
{
  var o = { a:1, b:2, c:3 },
  a = 10, b = 20, c = 30;
  o[Symbol.unscopables] = {
    a: false,
    b: true,
    c: false
  };
  with (o) {
    console.log( a, b, c );// 1 20 3
  }

}
