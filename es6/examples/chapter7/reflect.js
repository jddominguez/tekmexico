{
  var o = {};
  o[Symbol("c")] = "yay";
  o[2] = true;
  o[1] = true;
  o.b = "awesome";
  o.a = "cool";
  Reflect.ownKeys( o );
  // [1,2,"b","a",Symbol(c)]
  Object.getOwnPropertyNames( o );
  // [1,2,"b","a"]
  Object.getOwnPropertySymbols( o ); // [Symbol(c)]
}

{
  var o = { a: 1, b: 2 };
  var p = Object.create( o );
  var p.c = 3;
  var p.d = 4;
  for (var prop of Reflect.enumerate( p )) {
  console.log( prop );
  }
  // c d a b
  for (var prop in p) {
  console.log( prop );
  }
  // c d a b
  JSON.stringify( p );
  // {"c":3,"d":4}
  Object.keys( p );
  // ["c","d"]
}
