//
{
  var obj = { a: 1 },
  handlers = {
    get(target,key,context) {
      // note: target === obj,
      // context === pobj
      console.log( "accessing: ", key );
      return Reflect.get(
        target, key, context
      );
    }
  },
  pobj = new Proxy( obj, handlers );
  obj.a;
  // 1
  pobj.a;
  // accessing: a
  // 1
}

{
  var handlers = {
    getOwnPropertyDescriptor(target,prop) {
      console.log(
        "getOwnPropertyDescriptor"
      );
      return Object.getOwnPropertyDescriptor(
        target, prop
      );
    },
    defineProperty(target,prop,desc){
      console.log( "defineProperty" );
      return Object.defineProperty(
        target, prop, desc
      );
    }
  },
  proxy = new Proxy( {}, handlers );
  proxy.a = 2;
  // getOwnPropertyDescriptor
  // defineProperty
}

//proxy limitations
{
  var obj = { a:1, b:2 },
  handlers = {  },
  pobj = new Proxy( obj, handlers );
  typeof obj;
  String( obj );
  obj + "";
  obj == pobj;
  obj === pobj
}

//revocable proxies
{
  var obj = { a: 1 },
  handlers = {
    get(target,key,context) {
      // note: target === obj,
      // context === pobj
      console.log( "accessing: ", key );
      return target[key];
    }
  },
  { proxy: pobj, revoke: prevoke } =
  Proxy.revocable( obj, handlers );
  pobj.a;
  // accessing: a
  // 1

  // later:
  prevoke();
  pobj.a;
  // TypeError
}

//proxy first - proxy last
{
  var messages = [],
  handlers = {
    get(target,key) {
      // string value?
      if (typeof target[key] == "string") {
        // filter out punctuation
        return target[key]
        .replace( /[^\w]/g, "" );
      }
      // pass everything else through
      return target[key];
    },
    set(target,key,val) {
      // only set unique strings, lowercased
      if (typeof val == "string") {
        val = val.toLowerCase();
        if (target.indexOf( val ) == -1) {
          target.push(
            val.toLowerCase()
          );
        }
      }
      return true;
    }
  },
  messages_proxy =
  new Proxy( messages, handlers );
  // elsewhere:
  messages_proxy.push(
    "heLLo...", 42, "wOrlD!!", "WoRld!!"
  );
  messages_proxy.forEach( function(val){
    console.log(val);
  } );// hello world
  messages.forEach( function(val){
    console.log(val);
  } );// hello... world!!
}

{
  var handlers = {
    get(target,key,context) {
      return function() {
        context.speak(key + "!");
      };
    }
  },
  catchall = new Proxy( {}, handlers ),
  greeter = {
    speak(who = "someone") {
      console.log( "hello", who );
    }
  };
  // set up `greeter` to fall back to `catchall`
  Object.setPrototypeOf( greeter, catchall );
  greeter.speak();
  greeter.speak( "world" ); // hello someone
  // hello world
  greeter.everyone(); // hello everyone!
}

//No such property method
{
  var obj = {
    a: 1,
    foo() {
      console.log( "a:", this.a );
    }
  },
  handlers = {
    get(target,key,context) {
      if (Reflect.has( target, key )) {
        return Reflect.get(
          target, key, context
        );
      }
      else {
        throw "No such property/method!";
      }
    },
    set(target,key,val,context) {
      if (Reflect.has( target, key )) {
        return Reflect.set(
          target, key, val, context
        );
      }
      else {
        throw "No such property/method!";
      }
    }
  },
  pobj = new Proxy( obj, handlers );
  pobj.a = 3;
  pobj.foo(); // a: 3
  pobj.b = 4; // Error: No such property/method!
  pobj.bar(); // Error: No such property/method!
}

{
  var handlers = {
    get() {
      throw "No such property/method!";
    },
    set() {
      throw "No such property/method!";
    }
  },
  pobj = new Proxy( {}, handlers ),
  obj = {
    a: 1,
    foo() {
      console.log( "a:", this.a );
    }
  };
  // set up `obj` to fall back to `pobj`
  Object.setPrototypeOf( obj, pobj );
  obj.a = 3;
  obj.foo(); // a: 3
  obj.b = 4; // Error: No such property/method!
  obj.bar(); // Error: No such property/method!
}

//proxy hacking prototype chain
{
  var handlers = {
    get(target,key,context) {
      if (Reflect.has( target, key )) {
        return Reflect.get(
          target, key, context
        );
      }
      // fake circular `[[Prototype]]`
      else {
        return Reflect.get(
          target[
            Symbol.for( "[[Prototype]]" )
          ],
          key,
          context
        );
      }
    }
  },
  obj1 = new Proxy(
    {
      name: "obj-1",
      foo() {
        console.log( "foo:", this.name );
      }
    },
    handlers
  ),
  obj2 = Object.assign(
    Object.create( obj1 ),
    {
      name: "obj-2",
      bar() {
        console.log( "bar:", this.name );
        this.foo();
      }
    }
  );
  // fake circular `[[Prototype]]` link
  obj1[ Symbol.for( "[[Prototype]]" ) ] = obj2;
  obj1.bar();
  // bar: obj-1 <-- through proxy faking [[Prototype]]
  // foo: obj-1 <-- `this` context still preserved
  obj2.foo();
  // foo: obj-2 <-- through [[Prototype]]
}

{
  var obj1 = {
    name: "obj-1",
    foo() {
      console.log( "obj1.foo:", this.name );
    },
  },
  obj2 = {
    name: "obj-2",
    foo() {
      console.log( "obj2.foo:", this.name );
    },
    bar() {
      console.log( "obj2.bar:", this.name );
    }
  },
  handlers = {
    get(target,key,context) {
      if (Reflect.has( target, key )) {
        return Reflect.get(
          target, key, context
        );
      }
      // fake multiple `[[Prototype]]`
      else {
        for (var P of target[
          Symbol.for( "[[Prototype]]" )
        ]) {
          if (Reflect.has( P, key )) {
            return Reflect.get(
              P, key, context
            );
          }
        }
      }
    }
  },
  obj3 = new Proxy(
    {
      name: "obj-3",
      baz() {
        this.foo();
        this.bar();
      }
    },
    handlers
  );
  // fake multiple `[[Prototype]]` links
  obj3[ Symbol.for( "[[Prototype]]" ) ] = [
    obj1, obj2
  ];
  obj3.baz();
  // obj1.foo: obj-3
  // obj2.bar: obj-3
}
