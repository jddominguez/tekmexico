{
  "use strict";
  function foo(x) {
    return x * 2;
  }
  function bar(x) {
    // not a tail call
    return 1 + foo( x );
  }
  bar( 10 );// 21
}

{
  "use strict";
  function foo(x) {
    return x * 2;
  }
  function bar(x) {
    x = x + 1;
    if (x > 10) {
      return foo( x );
    }
    else {
      return bar( x + 1 );
    }
  }
  bar( 5 );// 24
  bar( 15 );// 32
}

//tail call rewrite
{
  "use strict";
  function foo(x) {
    if (x <= 1) return 1;
    return (x / 2) + foo( x - 1 );
  }
  //foo( 123456 ); // RangeError
}

{
  "use strict";
  var foo = (function(){
    function _foo(acc,x) {
      if (x <= 1) return acc;
      return _foo( (x / 2) + acc, x - 1 );
    }
    return function(x) {
      return _foo( 1, x );
    };
  })();
  //  foo( 123456 ); // 3810376848.5
}

//non-tco optimization
{
  "use strict";
  function trampoline( res ) {
    while (typeof res == "function") {
      res = res();
    }
    return res;
  }
  var foo = (function(){
    function _foo(acc,x) {
      if (x <= 1) return acc;
      return function partial(){
        return _foo( (x / 2) + acc, x - 1 );
      };
    }
    return function(x) {
      return trampoline( _foo( 1, x ) );
    };
  })();
  foo( 123456 ); // 3810376848.5
}

{
  "use strict";
  function foo(x) {
    var acc = 1;
    while (x > 1) {
      acc = (x / 2) + acc;
      x = x - 1;
    }
    return acc;
  }
  foo( 123456 ); // 3810376848.5
}

//meta?
{
  "use strict";
  try {
    (function foo(x){
      if (x < 5E5) return foo( x + 1 );
    })( 1 );
    TCO_ENABLED = true;
  }
  catch (err) {
    TCO_ENABLED = false;
  }
}

//self adjusting code
{
  "use strict";
  function foo(x) {
    function _foo() {
      if (x > 1) {
        acc = acc + (x / 2);
        x = x - 1;
        return _foo();
      }
    }
    var acc = 1;
    while (x > 1) {
      try {
        _foo();
      }
      catch (err) { }
    }
    return acc;
  }
  foo( 123456 ); // 3810376848.5
}
