//unicode functions
//string
{
  String.fromCodePoint( 0x1d49e );

}

{
  var s1 = "e\u0301";
  s1.length;
  var s2 = s1.normalize();
  s2.length;
  s2 === "\xE9";
}

//string.raw
{
  var str = "bc";
  String.raw`\ta${str}d\xE9`;
}

//repeat
{
  "foo" * 3;
}

{
  console.log("foo".repeat( 3 ));
}

//string inspections functions
{
  var palindrome = "step on no pets";
  palindrome.startsWith( "step on" ); // true
  palindrome.startsWith( "on", 5 );// true
  palindrome.endsWith( "no pets" );// true
  palindrome.endsWith( "no", 10 ); // true
  palindrome.includes( "on" );// false
  palindrome.includes( "on", 6 ); // true
}
