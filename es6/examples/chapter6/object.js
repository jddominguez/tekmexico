//object.is
{
  var x = NaN, y = 0, z = -0;
  x === x;// false
  y === z;// true
  Object.is( x, x );// true
  Object.is( y, z );// false
}

//object.getOwnPropertySymbols
{
  var o = {
    foo: 42,
    [ Symbol( "bar" ) ]: "hello world",
    baz: true
  };
  Object.getOwnPropertySymbols( o );// [ Symbol(bar) ]
}

//object.setPropertyOf
{
  var o1 = {
    foo() { console.log( "foo" ); }
  };
  var o2 = {
    // .. o2's definition ..
  };
  Object.setPrototypeOf( o2, o1 ); // delegates to `o1.foo()`
  o2.foo(); // foo
}

{
  var o1 = {
    foo() { console.log( "foo" ); }
  };
  var o2 = Object.setPrototypeOf( {
    // .. o2's definition ..
  }, o1 );

  // delegates to `o1.foo()`
  o2.foo();// foo
}

//object.assign
{
  var target = {},
  o1 = { a: 1 }, o2 = { b: 2 },
  o3 = { c: 3 }, o4 = { d: 4 };
  // set up read-only property
  Object.defineProperty( o3, "e", {
    value: 5,
    enumerable: true,
    writable: false,
    configurable: false
  } );
  // set up non-enumerable property
  Object.defineProperty( o3, "f", {
    value: 6,
    enumerable: false
  } );
  o3[ Symbol( "g" ) ] = 7;
  // set up non-enumerable symbol
  Object.defineProperty( o3, Symbol( "h" ), {
    value: 8,
    enumerable: false
  } );

  Object.setPrototypeOf( o3, o4 );

  Object.assign( target, o1, o2, o3 );
  target.a;  // 1
  target.b;  // 2
  target.c;  // 3
  Object.getOwnPropertyDescriptor( target, "e" );
  // { value: 5, writable: true, enumerable: true,
  //configurable: true }
  Object.getOwnPropertySymbols( target );
  // [Symbol("g")]
}

{
  var o1 = {
    foo() { console.log( "foo" ); }
  };
  var o2 = Object.assign(
    Object.create( o1 ),
    {
      // .. o2's definition ..
    }
  );
  // delegates to `o1.foo()`
  o2.foo();// foo
}
