//number.isNaN static function
{
  var a = NaN, b = "NaN", c = 42;
  isNaN( a );
  isNaN( b );
  isNaN( c );
  Number.isNaN( a );
  Number.isNaN( b );
  Number.isNaN( c );

  console.log(Number.isNaN( b ));
}

//number.isFinite
{
  var a = NaN, b = Infinity, c = 42;
  Number.isFinite( a );
  Number.isFinite( b );
  Number.isFinite( c );
  var a = "42";
  isFinite( a );
  Number.isFinite( a );
  console.log(Number.isFinite( a ));
}

//Integer-related
{
  //x === Math.floor( x );
  Number.isInteger( 4 );
  Number.isInteger( 4.2 );
  Number.isInteger( NaN );
  Number.isInteger( Infinity );
}

{
  function isFloat(x) {
    return Number.isFinite( x ) && !Number.isInteger( x );
  }
  isFloat( 4.2 );
  isFloat( 4 );
  isFloat( NaN );
  isFloat( Infinity );
}
{
  var x = Math.pow( 2, 53 ),
  y = Math.pow( -2, 53 );
  Number.isSafeInteger( x - 1 );
  Number.isSafeInteger( y + 1 );
  Number.isSafeInteger( x );
  Number.isSafeInteger( y );
}
