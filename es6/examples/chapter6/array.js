//array.of
{
  var a = Array( 3 );
  a.length;
  a[0];
  var b = Array.of( 3 );
  b.length;
  b[0];
  var c = Array.of( 1, 2, 3 );
  c.length;
  c;
}

{
  class MyCoolArray extends Array {
    sum() {
      return this.reduce( function reducer(acc,curr){
        return acc + curr;
      }, 0 );
    }
  }
  var x = new MyCoolArray( 3 );
  x.length;
  x.sum();
  // var y = [3];
  // y.length;
  // y.sum();
  var z = MyCoolArray.of( 3 );
  z.length;
  z.sum();
}

{
  Array.of(1);         // [1]
  Array.of(1, 2, 3);   // [1, 2, 3]
  Array.of(undefined); // [undefined]
  console.log(Array.of(1));
  console.log();
  console.log();
}

//array.from
{
  // array-like object
  var arrLike = {
    length: 3,
    0: "foo",
    1: "bar"
  };
  var arr = Array.prototype.slice.call( arrLike );
}

{
  var arrLike = {
    length: 4,
    2: "foo"
  };
  Array.from( arrLike );
}

{
  var emptySlotsArr = [];
  emptySlotsArr.length = 4;
  emptySlotsArr[2] = "foo";
  Array.from( emptySlotsArr );
}

//avoiding empty slots
{
  var a = Array( 4 );
  var b = Array.apply( null, { length: 4 } );
  var c = Array.from( { length: 4 } );
}

//mapping
{
  var arrLike = {
    length: 4,
    2: "foo"
  };
  Array.from( arrLike, function mapper(val,idx){
    if (typeof val == "string") {
      return val.toUpperCase();
    }
    else {
      return idx;
    }
  } );
}

//Creating arrays and subtypes
{
  class MyCoolArray extends Array {
  }
  MyCoolArray.from( [1, 2] ) instanceof MyCoolArray;
  Array.from(MyCoolArray.from( [1, 2] )) instanceof MyCoolArray;

  var x = new MyCoolArray( 1, 2, 3 );
  x.slice( 1 ) instanceof MyCoolArray;
}

{
  class MyCoolArray extends Array {
    // force `species` to be parent constructor
    static get [Symbol.species]() { return Array; }
  }
  var x = new MyCoolArray( 1, 2, 3 );
  x.slice( 1 ) instanceof MyCoolArray;
  x.slice( 1 ) instanceof Array;
}

{
  class MyCoolArray extends Array {
    // force `species` to be parent constructor
    static get [Symbol.species]() { return Array; }
  }
  var x = new MyCoolArray( 1, 2, 3 );
  MyCoolArray.from( x ) instanceof MyCoolArray;  // true
  MyCoolArray.of( [2, 3] ) instanceof MyCoolArray;  // true
}

//copy within proptotype method
{
  [1,2,3,4,5].copyWithin( 3, 0 ); // [1,2,3,1,2]
  [1,2,3,4,5].copyWithin( 3, 0, 1 ); // [1,2,3,1,5]
  [1,2,3,4,5].copyWithin( 0, -2 ); // [4,5,3,4,5]
  [1,2,3,4,5].copyWithin( 0, -2, -1 ); // [4,2,3,4,5]
  [1,2,3,4,5].copyWithin( 2, 1 );
  console.log([1,2,3,4,5].copyWithin( 2, 1 ));
}

//fill prototype method
{
  var a = Array( 4 ).fill( undefined );
  a; // [undefined,undefined,undefined,undefined]
}

{
  var a = [ null, null, null, null ].fill( 42, 1, 3 );
  a;// [null,42,42,null]
}

//find proptotype method
{
  var a = [1,2,3,4,5];
  (a.indexOf( 3 ) != -1);// true
  (a.indexOf( 7 ) != -1); // false
  (a.indexOf( "2" ) != -1); // false
}

{
  var a = [1,2,3,4,5];
  a.some( function matcher(v){
    return v == "2";
  } ); // true
  a.some( function matcher(v){
    return v == 7;
  } ); // false
}

{
  var a = [1,2,3,4,5];
  a.find( function matcher(v){
    return v == "2";
  } );  // 2
  a.find( function matcher(v){
    return v == 7;
  });  // undefined
}

{

var points = [
  { x:10, y:20},
  { x:20, y:30},
  { x:30, y:40},
  { x:40, y:50},
  { x:50, y:60}
];

points.findIndex( function matcher(point) {
  return (
    point.x % 3 == 0 &&
    point.y % 4 == 0
  );
} );
// 2

points.findIndex( function matcher(point) {
  return (
    point.x % 6 == 0 &&
    point.y % 7 == 0
  );
} );
// -1

}

//entries, values and keys
{
  // var a = [1,2,3];
  // [...a.values()];
  // [...a.keys()];
  // [...a.entries()];
  // [...a[Symbol.iterator]()];
}
