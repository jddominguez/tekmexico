{
  var dec = 42,
      oct = 0x52,
      hex = 0x2a;
}

{
  Number("42");
  Number("052");
  Number("0x2a");
}

{
  var a = 42;
  a.toString();
  a.toString(8);
  a.toString(16);
  a.toString(2);
}
