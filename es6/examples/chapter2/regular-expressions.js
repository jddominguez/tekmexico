{
  /𝄞/.test( "𝄞-clef" );
}


{
  /^.-clef/ .test( "𝄞-clef" );
  /^.-clef/u.test( "𝄞-clef" );
}

//sticky flag
{
  var re1 = /foo/,
  str = "++foo++";
  re1.lastIndex;
  re1.test( str );
  re1.lastIndex;
  re1.lastIndex = 4;
  re1.test( str );
  re1.lastIndex;
}

{
  var re2 = /foo/y,
  str = "++foo++";
  re2.lastIndex;
  re2.test( str );
  re2.lastIndex;
  re2.lastIndex = 2;
  re2.test( str );
  re2.lastIndex; // true
  re2.test( str );
  re2.lastIndex; // false
}

//sticky positioning
{
  var re = /f../y,
  str = "foo far fad";
  str.match( re ); // ["foo"]
  re.lastIndex = 10;
  str.match( re ); // ["far"]
  re.lastIndex = 20;
  str.match( re ); // ["fad"]
}

//sticky vs global
{
  var re = /o+./g,
  // <-- look, `g`!
  str = "foot book more";
  re.exec( str );
  re.lastIndex; // ["oot"]
  re.exec( str );
  re.lastIndex; // ["ook"]
  re.exec( str );
  re.lastIndex; // ["or"]
  re.exec( str );
  re.lastIndex; // null--no more matches
}

//anchored global
{
  var re = /^foo/y,
  str = "foo";
  re.test( str );
  re.test( str );
}


{
  re.lastIndex; // 0--reset after failure
  re.lastIndex = 1;
  re.test( str );
  re.lastIndex;
}


//regular expression flags
{
  var re = /foo/ig;
  re.toString();
  var flags = re.toString().match( /\/([gim]*)$/ )[1];
  flags;
}

{
  var re = /foo/ig;
  re.flags;
}

{
  var re1 = /foo*/y;
  re1.source;
  re1.flags;
  var re2 = new RegExp( re1 );
  re2.source;
  re2.flags;
  var re3 = new RegExp( re1, "ig" );
  re3.source;
  re3.flags;
}

{
  var dec = 42,
      oct = 0o52,
      hex = 0x2a,
      bin = 0b101010;
}

{
  var a = 42;
  a.toString();
  a.toString( 8 );
  a.toString( 16 );
  a.toString( 2 );
}
