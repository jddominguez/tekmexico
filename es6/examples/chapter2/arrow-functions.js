{
  function foo(x, y){
    return x + y;
  }
}

{
  var foo = (x, y) => x + y;
}


{
  var f1 = () => 12;
  var f2 = x => x * 2;
  var f3 = (x, y) => {
    var z = x * 2 + y;
    y++;
    x*=3;
    return (x + y + z) / 2;
  };
}


{
  var a = [1,2,3,4,5];

  a = a.map( v => v * 2);

  console.log(a);

}

{
  var dollabillsyall = (strings, ...values) =>
  strings.reduce( (s,v,idx) => {
    if (idx > 0) {
      if (typeof values[idx-1] == "number") {
        s += `$${values[idx-1].toFixed( 2 )}`;
      }
      else {
        s += values[idx-1];
      }
    }
    return s + v;
  }, "" );
}


{
  var controller = {
    makeRequest: function(){
      var self = this;
      btn.addEventListener( "click", function(){
        // ..
        self.makeRequest();
      }, false );
    }
  };
}

{
  var controller = {
    makeRequest: function(){
      btn.addEventListener( "click", () => {
        // ..
        this.makeRequest();
      }, false );
    }
  };
}

//fails!
// {
//   var controller = {
//     makeRequest: () => {
//       // ..
//       this.helper();
//     },
//     helper: () => {
//       // ..
//     }
//   };
//   controller.makeRequest();
// }


//for loops!
{
  var a = ["a","b","c","d","e"];
  for (var idx in a) {
    console.log( idx );
  }
  for (var val of a) {
    console.log( val );
  }
}

{
  for (var c of "hello") {
    console.log( c );
  }
}

{
  var o = {};
  for (o.a of [1,2,3]) {
    console.log( o.a );
  }
  for ({x: o.a} of [ {x: 1}, {x: 2}, {x: 3} ]) {
    console.log( o.a );
  }
}
