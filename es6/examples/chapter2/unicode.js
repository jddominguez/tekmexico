{
  var snowman = "\u2603";
  console.log( snowman );
}

{
  var gclef = "\uD834\uDD1E";
  console.log( gclef );
}

{
  var gclef = "\u{1D11E}";
console.log( gclef );
}

//unicode aware string operations
{
  var snowman = "☃";
  snowman.length;
  var gclef = "𝄞";
  gclef.length;
}

{
  var gclef = "𝄞";
  [...gclef].length;
  Array.from( gclef ).length;
}

{
  var s1 = "\xE9",
  s2 = "e\u0301";
  [...s1].length;
  [...s2].length;
}


{
  var s1 = "\xE9",
  s2 = "e\u0301";
  s1.normalize().length;
  s2.normalize().length;
  s1 === s2;
  s1 === s2.normalize();
}

{
  var s1 = "o\u0302\u0300",
  s2 = s1.normalize(),
  s3 = "ồ";
  s1.length;
  s2.length;
  s3.length;
  s2 === s3;
}

{
  var s1 = "e\u0301\u0330";
  console.log( s1 );
  s1.normalize().length;
}



//character positioning
{
  var s1 = "abc\u0301d",
  s2 = "ab\u0107d",
  s3 = "ab\u{1d49e}d";
  console.log( s1 );
  console.log( s2 );
  console.log( s3 );
  s1.charAt(2);
  s2.charAt(2);
  s3.charAt(3);
  s3.charAt(3);
}

{
  var s1 = "abc\u0301d",
  s2 = "ab\u0107d",
  s3 = "ab\u{1d49e}d";
  [...s1.normalize()][2];
  [...s2.normalize()][2];
  [...s3.normalize()][2];
}

{
  var s1 = "abc\u0301d",
  s2 = "ab\u0107d",
  s3 = "ab\u{1d49e}d";
  s1.normalize().codePointAt( 2 ).toString( 16 );
  s2.normalize().codePointAt( 2 ).toString( 16 );
  s3.normalize().codePointAt( 2 ).toString( 16 );
}

{
  String.fromCodePoint( 0x107 );
  String.fromCodePoint( 0x1d49e );
}

{
  var s1 = "abc\u0301d",
  s2 = "ab\u0107d",
  s3 = "ab\u{1d49e}d";
  String.fromCodePoint( s1.normalize().codePointAt( 2 ) );
  String.fromCodePoint( s2.normalize().codePointAt( 2 ) );
  String.fromCodePoint( s3.normalize().codePointAt( 2 ) );
}

//unicode identifiers name
{
  var \u03A9 = 42;
  var \u{2B400} = 42;
}
