
//default parameters
{
  function foo(x, y){
    x = x || 11;
    y = y || 31;
    console.log(x + y);
  }

  foo();
  foo(5,6);
  foo(null, 6);
  foo(0, 42);
}

{
  function foo(x,y) {
    x = (x !== undefined) ? x : 11;
    y = (y !== undefined) ? y : 31;
    console.log( x + y );
  }
  foo( 0, 42 );
  foo( undefined, 6 );
}

{
  function foo(x,y) {
    x = (0 in arguments) ? x : 11;
    y = (1 in arguments) ? y : 31;
    console.log( x + y );
  }
  foo( 5 );
  foo( 5, undefined );
  // 36
  // NaN
}


{
  function foo(x = 11, y = 31) {
    console.log( x + y );
  }
  foo();
  foo( 5, 6 );
  foo( 0, 42 ); // 42
  // 11
  // 42
  foo( 5 );
  foo( 5, undefined );
  foo( 5, null ); // 36
  // 36 <-- `undefined` is missing
  // 5 <-- null coerces to `0`
  foo( undefined, 6 );
  foo( null, 6 ); // 17 <-- `undefined` is missing
}


//default value expressions
{

function bar(val){
  console.log("bar called!");
  return y + val;
}

function foo(x = y + 3, z = bar( x )){
  console.log(x, z);
}

var y = 5;
foo();

foo(10);

y = 6;

foo(undefined, 10)

}


{
  var w = 1, z = 2;
  function foo( x = w + 1, y = x + 1, z = z + 1 ) {
    console.log( x, y, z );
  }
  //foo(); //reference error!
}


{
  function foo( x =
    (function(v){ return v + 11; })( 31 )
  ) {
    console.log( x );
  }
  foo();
}
