{
  var name = "Kyle";
  var greeting = "Hello " + name + "!";
  console.log( greeting );
  console.log( typeof greeting );
}

{
  var name = "Kyle";
  var greeting = `Hello ${ name }!`;
  console.log( greeting );
  console.log( typeof greeting );
}

//interpolated expressions
{
  var text = 'Now is the time for all good men to come to the aid of their country!';
  console.log( text );
}

{
  function upper(s) {
    return s.toUpperCase();
  }
  var who = 'reader';
  var text =`A very ${upper( "warm" )} welcome to all of you ${upper( `${who}s` )}!`;
  console.log( text );
}

//expression scope
{
  function foo(str) {
    var name = "foo";
    console.log( str );
  }
  function bar() {
    var name = "bar";
    foo( `Hello from ${name}!` );
  }
  var name = "global";
  bar();
}

//tagged template literals
{
  function foo(strings, ...values) {
    console.log( strings );
    console.log( values );
  }
  var desc = "awesome";
  foo`Everything is ${desc}!`;
}


{
  function tag(strings, ...values) {
    return strings.reduce( function(s,v,idx){
      return s + (idx > 0 ? values[idx-1] : "") + v;
    }, "" );
  }
  var desc = "awesome";
  var text = tag`Everything is ${desc}!`;
  console.log( text );
}


{
  function dollabillsyall(strings, ...values){
    return strings.reduce( function(s, v, idx){
      if(idx>0){
        if(typeof values[idx-1] == 'number'){
          s += `$${values[idx-1].toFixed( 2 )}`;
        }
        else {
          s += values[idx-1];
        }
      }
      return s + v;
    }, "");
  }
  var amt1 = 11.99,
  amt2 = amt1 * 1.08,
  name = "Kyle";
  var text = dollabillsyall
  `Thanks for your purchase, ${name}! Your
  product cost was ${amt1}, which with tax
  comes out to ${amt2}.`
  console.log( text );
}

//raw strings
{
  function showraw(strings, ...values) {
    console.log( strings );
    console.log( strings.raw );
  }
  showraw`Hello\nWorld`;
}

{
  console.log( `Hello\nWorld` );
  // Hello
  // World
  console.log( String.raw`Hello\nWorld` );
  // Hello\nWorld
  String.raw`Hello\nWorld`.length;
  // 12
}
