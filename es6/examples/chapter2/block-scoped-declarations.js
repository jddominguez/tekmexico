//start iife
{
  var a = 2;
  (function iife(){
    var a = 3;
    console.log(a);
  })();
  console.log(a);
}

// let declaration
{
  var a = 2;
  {
    let a = 3;
    console.log(a);
  }
  console.log(a);
}

//implicit block scoping
{
  let a = 2;
  if (a > 1) {
    let b = a * 3;
    console.log( b );
    for (let i = a; i <= b; i++) {
      let j = i + 10;
      console.log( j );
    }
    let c = a + b;
    console.log( c );
  }
}

//let declarations access
{
  console.log(a);
  //console.log(b); //reference error!
  var a;
  let b;
}

{
  if(typeof a === 'undefined'){
    console.log("cool!");
  }
  // if(typeof b === 'undefined'){ //reference error!
  //   console.log(b);
  // }
  let b;
}


//let+for
{
  var funcs = [];
  for(let i = 0; i < 5; i++){
    funcs.push(function(){
      console.log(i);
    });
  }

  funcs[3]();
}

//const declaartions
{
  const a = 2;
  console.log(a);
  //a = 3; //type error!
}

{
  const  a = [1,2,3];
  a.push(4);
  console.log(a);
  //a = 42; //type error!
}

{
  foo();
  function foo(){
  }
}
foo();

{
  if(true){
    function foo(){
      console.log("1");
    }
  }
  else{
    function foo(){
      console.log("2");
    }
  }
  foo();
}

//spread-rest
{
  function foo(x,y,z){
    console.log(x,y,z);
  }

  foo(...[1,2,3]);
}

{
  function foo(x, y, ...z) {
    console.log( x, y, z );
  }
  foo( 1, 2, 3, 4, 5 );
}


{
  //new es6 way
  function foo(...args) {
    args.shift();
    console.log( ...args );
  }
  // pre es6 way
  function bar() {
    var args = Array.prototype.slice.call( arguments );
    args.push( 4, 5 );
    args = args.filter( function(v){
      return v % 2 == 0;
    } );
    foo.apply( null, args );
  }
  bar( 0, 1, 2, 3 );
}
