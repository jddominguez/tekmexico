{
  function foo() {
    return [1,2,3];
  }
  var tmp = foo(),
  a = tmp[0], b = tmp[1], c = tmp[2];
  console.log( a, b, c );
}


{
  function bar() {
    return {
      x: 4,
      y: 5,
      z: 6
    };
  }
  var tmp = bar(),
  x = tmp.x, y = tmp.y, z = tmp.z;
  console.log( x, y, z );
}

{
  var [ a, b, c ] = foo();
  var { x: x, y: y, z: z } = bar();
  console.log( a, b, c );
  console.log( x, y, z );
}


//object property assignment pattern
{
  var { x, y, x} = bar();
  console.log(x, y, z);
}

{
  var { x: bam, y: baz, z: bap } = bar();
  console.log( bam, baz, bap );
  console.log( x, y, z );
}

{
  var X = 10, Y = 20;
  var o = { a: X, b: Y };
  console.log( o.a, o.b );
}

{
  var aa = 10, bb = 20;
  var o = { x: aa, y: bb };
  var { x: AA, y: BB } = o;
  console.log( AA, BB );
}

{
  var a, b, c, x, y, z;
  [a,b,c] = foo();
  ( { x, y, z } = bar() );
  console.log( a, b, c );
  console.log( x, y, z );
}

{
  var o = {};
  [o.a, o.b, o.c] = foo();
  ( { x: o.x, y: o.y, z: o.z } = bar() );
  console.log( o.a, o.b, o.c );
  console.log( o.x, o.y, o.z );
}

{
  var which = "x",
  o = {};
  ( { [which]: o[which] } = bar() );
  console.log( o.x );
}


{
  var o1 = { a: 1, b: 2, c: 3 },
  o2 = {};
  ( { a: o2.x, b: o2.y, c: o2.z } = o1 );
  console.log( o2.x, o2.y, o2.z );
}

{
  var o1 = { a: 1, b: 2, c: 3 },
  a2 = [];
  ( { a: a2[0], b: a2[1], c: a2[2] } = o1 );
  console.log( a2 );
}

{
  var a1 = [ 1, 2, 3 ],
  o2 = {};
  [ o2.a, o2.b, o2.c ] = a1;
  console.log( o2.a, o2.b, o2.c );
}

{
  var a1 = [ 1, 2, 3 ],
  a2 = [];
  [ a2[2], a2[0], a2[1] ] = a1;
  console.log( a2 );
}


{
  var x = 10, y = 20;
  [ y, x ] = [ x, y ];
  console.log( x, y );
}


//repeated asigments
{
  var { a: X, a: Y } = { a: 1 };
  X;
  Y;
}

{
  var { a: { x: X, x: Y }, a } = { a: { x: 1 } };
  X;
  Y;
  a;
  ( { a: X, a: Y, a: [ Z ] } = { a: [ 1 ] } );
  X.push( 2 );
  Y[0] = 10;
  X;
  Y;
  Z;
}

{
  // // harder to read:
  // var { a: { b: [ c, d ], e: { f } }, g } = obj;
  // // better:
  // var {
  //   a: {
  //   b: [ c, d ],
  //   e: { f }
  // },
  // g
  // } = obj;
}


//destructing assignment expressions
{
  var o = { a:1, b:2, c:3 },
  a, b, c, p;
  p = { a, b, c } = o;
  console.log( a, b, c );
  p === o;
}

{
  var o = [1,2,3],
  a, b, c, p;
  p = { a, b, c } = o;
  console.log( a, b, c );
  p === o;
}

{
  var o = { a:1, b:2, c:3 },
  p = [4,5,6], a, b, c, x, y, z;
  ( {a} = {b,c} = o );
  [x,y] = [z] = p;
  console.log( a, b, c );
  console.log( x, y, z );
}



{
  var [,b] = foo();
  var { x, z } = bar();
  console.log( b, x, z );
}

{
  var [,,c,d] = foo();
  var { w, z } = bar();
  console.log( c, z );
  console.log( d, w );
}


{
  var a = [2,3,4];
  var b = [ 1, ...a, 5 ];
  console.log( b );
}

{
  var a = [2,3,4];
  var [ b, ...c ] = a;
  console.log( b, c );
}

//default value assigment
{
  var [ a = 3, b = 6, c = 9, d = 12 ] = foo();
  var { x = 5, y = 10, z = 15, w = 20 } = bar();
  console.log( a, b, c, d );
  console.log( x, y, z, w );
}

{
  var { x, y, z, w: WW = 20 } = bar();
  console.log( x, y, z, WW );
}

{
  var x = 200, y = 300, z = 100;
  var o1 = { x: { y: 42 }, z: { y: z } };
  ( { y: x = { y: y } } = o1 );
  ( { z: y = { y: z } } = o1 );
  ( { x: z = { y: x } } = o1 );
  console.log( x.y, y.y, z.y );
}


//nested destructuring

{
  var a1 = [ 1, [2, 3, 4], 5 ];
  var o1 = { x: { y: { z: 6 } } };
  var [ a, [ b, c, d ], e ] = a1;
  var { x: { y: { z: w } } } = o1;
  console.log( a, b, c, d, e );
  console.log( w );
}


//desctructuring parameters
{
  function foo(x) {
    console.log( x );
  }
  foo( 42 );
}

{
  function foo( [ x, y ] ) {
    console.log( x, y );
  }
  foo( [ 1, 2 ] );
  foo( [ 1 ] );
  foo( [] );
}


{
  function foo( { x, y } ) {
    console.log( x, y );
  }
  foo( { y: 1, x: 2 } );
  foo( { y: 42 } );
  foo( {} );
}

{
  function f1([ x=2, y=3, z ]) { }
  function f2([ x, y, ...z], w) { }
  function f3([ x, y, ...z], ...w) { }
  function f4({ x: X, y }) { }
  function f5({ x: X = 10, y = 20 }) { }
  function f6({ x = 10 } = {}, { y } = { y: 10 }) { }
}


{
  function f3([ x, y, ...z], ...w) {
    console.log( x, y, z, w );
  }
  f3( [] );
  f3( [1,2,3,4], 5, 6 );
}


{
  function f6({ x = 10 } = {}, { y } = { y: 10 }) {
    console.log( x, y );
  }
  f6();
}

{
  f6( {}, {} );
}



{
  function f6({ x = 10 } = {}, { y } = { y: 10 }) {
    console.log( x, y );
  }
  f6();
  f6( undefined, undefined );
  f6( {}, undefined );
  f6( {}, {} );
  f6( undefined, {} );
  f6( { x: 2 }, { y: 3 } );
}
