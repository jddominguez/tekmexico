{
  var sym = Symbol( "some optional description" );
  typeof sym;

  sym.toString();
  sym instanceof Symbol; // false
  var symObj = Object( sym );
  symObj instanceof Symbol; // true
  symObj.valueOf() === sym; // true
}

{
  const EVT_LOGIN = Symbol( "event.login" );
}

{
  const INSTANCE = Symbol( "instance" );
  function HappyFace() {
    if (HappyFace[INSTANCE]) return HappyFace[INSTANCE];
    function smile() {  }
    return HappyFace[INSTANCE] = {
      smile: smile
    };
  }
  var me = HappyFace(),
  you = HappyFace();
  me === you; //true
}

//symbol registry
{
  const EVT_LOGIN = Symbol.for( "event.login" );
  console.log( EVT_LOGIN );  // Symbol(event.login)
}


{
  function HappyFace() {
    const INSTANCE = Symbol.for( "instance" );
    if (HappyFace[INSTANCE]) return HappyFace[INSTANCE];
      return HappyFace[INSTANCE] = {};
  }
}

{
  function extractValues(str) {
    var key = Symbol.for( "extractValues.parse" ),
    re = extractValues[key] ||
    /[^=&]+?=([^&]+?)(?=&|$)/g,
    values = [], match;
    while (match = re.exec( str )) {
      values.push( match[1] );
    }
    return values;
  }
}


{
  extractValues[Symbol.for( "extractValues.parse" )] = /..some pattern../g;
  extractValues( "..some string.." );
}

{
  var s = Symbol.for( "something cool" );
  var desc = Symbol.keyFor( s );
  console.log( desc ); // "something cool"
  // get the symbol from the registry again
  var s2 = Symbol.for( desc );
  s2 === s;
}

//symbols as object properties
{
  var o = {
    foo: 42,
    [ Symbol( "bar" ) ]: "hello world",
    baz: true
  };
  Object.getOwnPropertyNames( o ); // [ "foo","baz" ]
  Object.getOwnPropertySymbols( o ); // [ Symbol(bar) ]
}


//built in symbols
{
  var a = [1,2,3];
  a[Symbol.iterator];
}
