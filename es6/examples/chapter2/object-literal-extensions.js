//concise properties
{
  var x = 2, y = 3,
  o = {
    x: x,
    y: y
  };
}

{
  var x = 2, y = 3,
  o = {
    x,
    y
  };
}

{
  var o = {
    x: function(){
    },
    y: function(){
    }
  }
}

{
  var o = {
    *foo() { }
  };
}


//concisely unnamed
function runSomething(o) {
  var x = Math.random(),
  y = Math.random();
  return o.something( x, y );
}
runSomething( {
  something: function something(x,y) {
    if (x > y) {
      return something( y, x );
    }
    return y - x;
  }
} );


//es5 getter/setter
{
  var o = {
    __id: 10,
    get id() { return this.__id++; },
    set id(v) { this.__id = v; }
  }
  o.id;
  o.id;
  o.id = 20;
  o.id;
  o.__id;
  o.__id;
}

//computed property names
{
  var prefix = "user_";
  var o = {
    baz: function(){ }
  };
  o[ prefix + "foo" ] = function(){ };
  o[ prefix + "bar" ] = function(){ };
}

{
  var o1 = {
    foo() {
      console.log( "o1:foo" );
    }
  };
  var o2 = {
    foo() {
      super.foo();
      console.log( "o2:foo" );
    }
  };
  Object.setPrototypeOf( o2, o1 );
  o2.foo();
}
