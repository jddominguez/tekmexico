{
  var p = new Promise( function(resolve,reject){
    // ..
  } );
}


{
  function ajax(url,cb) {
    // make request, eventually call `cb(..)`
  }
  // ..
  ajax( "http://some.url.1", function handler(err,contents){
    if (err) {
      // handle ajax error
    }
    else {
      // handle `contents` success
    }
  } );
}

{
  function ajax(url) {
    return new Promise( function pr(resolve,reject){
      // make request, eventually call
      // either `resolve(..)` or `reject(..)`
    } );
  }
  // ..
  ajax( "http://some.url.1" )
  .then(
    function fulfilled(contents){
      // handle `contents` success
    },
    function rejected(reason){
      // handle ajax error reason
    }
  );
}

{
  ajax( "http://some.url.1" )
  .then(
    function fulfilled(contents){
      return contents.toUpperCase();
    },
    function rejected(reason){
      return "DEFAULT VALUE";
    }
  )
  .then( function fulfilled(data){
    // handle data from original promise's
    // handlers
  } );
}

{
  ajax( "http://some.url.1" )
  .then(
    function fulfilled(contents){
      return ajax(
        "http://some.url.2?v=" + contents
      );
    },
    function rejected(reason){
      return ajax(
        "http://backup.url.3?err=" + reason
      );
    }
  )
  .then( function fulfilled(contents){
    // `contents` comes from the subsequent
    // `ajax(..)` call, whichever it was
  } );
}

//thenables
{
  var th = {
    then: function thener( fulfilled ) {
      // call `fulfilled(..)` once every 100ms forever
      setInterval( fulfilled, 100 );
    }
  };
}

//promise API
{
  var p1 = Promise.resolve( 42 );
  var p2 = new Promise( function pr(resolve){
    resolve( 42 );
  } );
}

{
  var theP = ajax( );
  var p1 = Promise.resolve( theP );
  var p2 = new Promise( function pr(resolve){
    resolve( theP );
  } );
}

{
  var p1 = Promise.reject( "Oops" );
  var p2 = new Promise( function pr(resolve,reject){
    reject( "Oops" );
  } );
}

{
  var p1 = Promise.resolve( 42 );
  var p2 = new Promise( function pr(resolve){
    setTimeout( function(){
      resolve( 43 );
    }, 100 );
  } );
  var v3 = 44;
  var p4 = new Promise( function pr(resolve,reject){
    setTimeout( function(){
      reject( "Oops" );
    }, 10 );
  } );
}

{
  Promise.all( [p1,p2,v3] )
  .then( function fulfilled(vals){
    console.log( vals );
  } );
  Promise.all( [p1,p2,v3,p4] )
  .then(
    function fulfilled(vals){
      // never gets here
    },
    function rejected(reason){
      console.log( reason );
    }
  );
}

{
  // NOTE: re-setup all test values to
  // avoid timing issues misleading you!
  Promise.race( [p2,p1,v3] )
  .then( function fulfilled(val){
    console.log( val );
  } );
  Promise.race( [p2,p4] )
  .then(
    function fulfilled(val){
      // never gets here
    },
    function rejected(reason){
      console.log( reason );
    }
  );
}

//generators + promises
{
  // step1()
  // .then(
  //   step2,
  //   step2Failed
  // )
  // .then(
  //   function(msg) {
  //     return Promise.all( [
  //       step3a( msg ),
  //       step3b( msg ),
  //       step3c( msg )
  //     ] )
  //   }
  // )
  // .then(step4);
}


{
  function *main() {
    var ret = yield step1();
    try {
      ret = yield step2( ret );
    }
    catch (err) {
      ret = yield step2Failed( err );
    }
    ret = yield Promise.all( [
      step3a(ret ),
      step3b(ret ),
      step3c( ret )
    ]);
    yield step4( ret );
  }
}

{
  function run(gen) {
    var args = [].slice.call( arguments, 1), it;
    it = gen.apply( this, args );
    return Promise.resolve()
    .then( function handleNext(value){
      var next = it.next( value );
      return (function handleResult(next){
        if (next.done) {
          return next.value;
        }
        else {
          return Promise.resolve( next.value )
          .then(
            handleNext,
            function handleErr(err) {
              return Promise.resolve(
                it.throw( err )
              )
              .then( handleResult );
            }
          );
        }
      })( next );
    } );
  }
}

{
  run( main )
  .then(
    function fulfilled(){
      // `*main()` completed successfully
    },
    function rejected(reason){
      // Oops, something went wrong
    }
  );
}
