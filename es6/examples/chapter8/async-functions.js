{
  run( function *main() {
    var ret = yield step1();
    try {
      ret = yield step2( ret );
    }
    catch (err) {
      ret = yield step2Failed( err );
    }
    ret = yield Promise.all([
      step3a( ret ),
      step3b( ret ),
      step3c( ret )
    ]);
    yield step4( ret );
  } )
  .then(
    function fulfilled(){
      // `*main()` completed successfully
    },
    function rejected(reason){
      // Oops, something went wrong
    }
  );
}

{
  async function main() {
    var ret = await step1();
    try {
      ret = await step2( ret );
    }
    catch (err) {
      ret = await step2Failed( err );
    }
    ret = await Promise.all( [
      step3a( ret ),
      step3b( ret ),
      step3c( ret )
    ] );
    await step4( ret );
  }
  main()
  .then(
    function fulfilled(){
      // `main()` completed successfully
    },
    function rejected(reason){
      // Oops, something went wrong
    }
  );
}

//caveats
{
  async function request(url) {
    var resp = await (
      new Promise( function(resolve,reject){
        var xhr = new XMLHttpRequest();
        xhr.open( "GET", url );
        xhr.onreadystatechange = function(){
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              resolve( xhr );
            }
            else {
              reject( xhr.statusText );
            }
          }
        };
        xhr.send();
      } )
    );
    return resp.responseText;
  }
  var pr = request( "http://some.url.1" );
  pr.then(
    function fulfilled(responseText){
      // ajax success
    },
    function rejected(reason){
      // Oops, something went wrong
    }
  );
}
