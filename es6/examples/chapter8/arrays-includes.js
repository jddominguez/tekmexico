{
  var vals = [ "foo", "bar", 42, "baz" ];
  if (vals.indexOf( 42 ) >= 0) {
    // found it!
  }
}

{
  var vals = [ "foo", "bar", 42, "baz" ];
  if (~vals.indexOf( 42 )) {
    // found it!
  }
}

{
  var vals = [ "foo", "bar", 42, "baz" ];
  if (vals.includes( 42 )) {
    // found it!
  }
}

{
  var vals = [ "foo", "bar", 42, "baz" ];
  if (vals.includes( 42 )) {
    // found it!
  }
}
