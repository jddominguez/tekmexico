{
  var m = {};
  var x = { id: 1 },
  y = { id: 2 };
  m[x] = "foo";
  m[y] = "bar";
  m[x];
  m[y];
}

//fake map
{
  var keys = [], vals = [];
  var x = { id: 1 },
  y = { id: 2 };
  keys.push( x );
  vals.push( "foo" );
  keys.push( y );
  vals.push( "bar" );
  keys[0] === x;
  vals[0];
  keys[1] === y;
  vals[1];
}

//es6 maps
{
  var m = new Map();
  var x = { id: 1 },
  y = { id: 2 };
  m.set( x, "foo" );
  m.set( y, "bar" );
  m.get( x );
  m.get( y );
}

{
  m.set( x, "foo" );
  m.set( y, "bar" );
  m.delete( y );
  m.set( x, "foo" );
  m.set( y, "bar" );
  m.size; // 2
  m.clear();
  m.size;
}


{
  var m2 = new Map( m.entries() );
  var m2 = new Map( m );
  var x = { id: 1 },
  y = { id: 2 };
  var m = new Map( [
    [ x, "foo" ],
    [ y, "bar" ]
  ] );
  m.get( x );
  m.get( y );
}


//map values
{
  var m = new Map();
  var x = { id: 1 },
  y = { id: 2 };
  m.set( x, "foo" );
  m.set( y, "bar" );
  var vals = [ ...m.values() ];
  vals;
  Array.from( m.values() );
}

{
  var m = new Map();
  var x = { id: 1 },
  y = { id: 2 };
  m.set( x, "foo" );
  m.set( y, "bar" );
  var vals = [ ...m.entries() ];
  vals[0][0] === x;
  vals[0][1];
  vals[1][0] === y;
  vals[1][1];
}

{
  var m = new Map();
  var x = { id: 1 },
  y = { id: 2 };
  m.set( x, "foo" );
  m.set( y, "bar" );
  var keys = [ ...m.keys() ];
  keys[0] === x;
  keys[1] === y;
  var m = new Map();
  var x = { id: 1 },
  y = { id: 2 };
  m.set( x, "foo" );
  m.has( x );
  m.has( y );
}

//weakmaps
{
  var m = new WeakMap();
  var x = { id: 1 },
  y = { id: 2 };
  m.set( x, "foo" );
  m.has( x );//true
  m.has( y );//false
}

{
 var m = new WeakMap();

 var x = { id:1 },
     y = { id:2 },
     z = { id:3 },
     w = { id:4 };

     m.set(x, y);
     x = null;
     y = null;

     m.set(z,w);
     w = null;
}
