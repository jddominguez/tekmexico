{
  var buf = new ArrayBuffer( 32 );
  buf.byteLength;
}

{
  var arr = new Uint16Array( buf );
  arr.length;
}


//endianness
{
  var littleEndian = (function() {
    var buffer = new ArrayBuffer( 2 );
    new DataView( buffer ).setInt16( 0, 256, true );
    return new Int16Array( buffer )[0] === 256;
  })();
}

//multiple views
{
  var buf = new ArrayBuffer( 2 );
  var view8 = new Uint8Array( buf );
  var view16 = new Uint16Array( buf );
  view16[0] = 3085;
  view8[0];
  view8[1];
  view8[0].toString( 16 );
  view8[1].toString( 16 );
  var tmp = view8[0];
  view8[0] = view8[1];
  view8[1] = tmp;
  view16[0];

  // var first = new Uint16Array( buf, 0, 2 )[0],
  //    second = new Uint8Array( buf, 2, 1 )[0],
  //     third = new Uint8Array( buf, 3, 1 )[0],
  //    fourth = new Float32Array( buf, 4, 4 )[0];
}

//typed array constructors
{
  var a = new Int32Array( 3 );
  a[0] = 10;
  a[1] = 20;
  a[2] = 30;
  a.map( function(v){
    console.log( v );
  } );
  a.join( "-" );
}

{
  var a = new Uint8Array( 3 );
  a[0] = 10;
  a[1] = 20;
  a[2] = 30;
  var b = a.map( function(v){
    return v * v;
  } );
  b;
}

{
  var a = new Uint8Array( 3 );
  a[0] = 10;
  a[1] = 20;
  a[2] = 30;
  var b = Uint16Array.from( a, function(v){
    return v * v;
  } );
  b;
}

{
  var a = [ 10, 1, 2, ];
  a.sort();
  var b = new Uint8Array( [ 10, 1, 2 ] );
  b.sort();
}
