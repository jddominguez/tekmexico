{
  var s = new Set();
  var x = { id: 1 },
  y = { id: 2 };
  s.add( x );
  s.add( y );
  s.add( x );
  s.size; // 2
  s.delete( y );
  s.size; // 1
  s.clear();
  s.size; // 0
}
