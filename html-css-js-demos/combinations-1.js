var count=0;
   function permute(pre,cur){
       var len=cur.length;
       for(var i=0;i<len;i++){
           var p=clone(pre);
           var c=clone(cur);
           p.push(cur[i]);
           remove(c,cur[i]);
           if(len>1){
               permute(p,c);
           }else{
               print(p);
               count++;
           }
       }
   }
   function print(arr){
       var len=arr.length;
       for(var i=0;i<len;i++){
           console.log(arr[i]);
       }
   }
   function remove(arr,item){
       if(contains(arr,item)){
           var len=arr.length;
           for(var i = len-1; i >= 0; i--){ // STEP 1
               if(arr[i] == item){             // STEP 2
                   arr.splice(i,1);              // STEP 3
               }
           }
       }
   }
   function contains(arr,value){
       for(var i=0;i<arr.length;i++){
           if(arr[i]==value){
               return true;
           }
       }
       return false;
   }
   function clone(arr){
       var a=new Array();
       var len=arr.length;
       for(var i=0;i<len;i++){
           a.push(arr[i]);
       }
       return a;
   }


print(123);
