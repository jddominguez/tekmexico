function solver(k, n){
  if(k == 0 || n == 0)
    return 0;

  var ctr = 0;
  //var l = n.toString().length;
  var l = (Math.log(Math.abs(n)+1) * 0.43429448190325176 | 0) + 1;
  var f = retInt(n);
  if( l > 1){
    ctr += f * Math.pow(10, l-2) * (l-1);
  }
  if( f > k){
    ctr += Math.pow(10, l-1);
  }
  else if(f == k){
    ctr += n - f * Math.pow(10, l-1) + 1;
  }
  var m = n.toString().slice(1);

  return ctr + solver(k, m);
}

function retInt(x){
  if(x > 10)
    return retInt(x/10);
  else
    return Math.floor(x);
}

console.log(solver(2,1234));
