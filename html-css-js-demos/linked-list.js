function linkedList(){

  this._length = 0;
  this.head = null;

  function Node(data){
    this.data = data;
    this.next = null;
  }

  this.add = function(val){
    var node = new Node(val), currentNode = this.head;
    if(currentNode === null){
      this.head = node;
      this._length++;
      return node;
    }
    while(currentNode.next)
      currentNode = currentNode.next;
    currentNode.next = node;
    this._length++;
    return node;
  };

  this.getNode = function(pos){
    var currentNode = this.head, length = this._length, count = 1;
    if(length === 0 || pos < 1 || pos > length)
      return false;
    while(count < pos){
      currentNode = currentNode.next;
      count++;
    }
    return currentNode;
  };

  this.isPalindrome = function(){
    for(var i = 1, j = this._length; i < j; i++, j--){
      //console.log(i);
      if(this.getNode(i).data != this.getNode(j).data)
        return console.log(false);
    }
    return console.log(true);
  }

}

// linkedList.prototype.add = function(val){
//   var node = new Node(val);
//   var currentNode = this.head;
//   if(currentNode === null){
//     this.head = node;
//     this._length++;
//     return node;
//   }
//   while(currentNode.next)
//     currentNode = currentNode.next;
//   currentNode.next = node;
//   this._length++;
//   return node;
// };

// linkedList.prototype.searchNode = function(pos){
//   var currentNode = this.head;
//   var length = this._length;
//   var count = 1;
//   if(length === 0 || pos < 1 || pos > length)
//     return false;
//   while(count < pos){
//     currentNode = currentNode.next;
//     count++;
//   }
//   return currentNode;
// };

// linkedList.prototype.isPalindrome = function(){
//   for(var i = 1, j = this._length; i < j; i++, j--){
//     if(this.searchNode(i).data != this.searchNode(j).data)
//       return console.log(false);
//   }
//   return console.log(true);
// }

var myList = new linkedList();

myList.add(1);
myList.add(0);
myList.add(0);
myList.add(0);
myList.add(1);

// for (var i = 0; i < 100000; i++) {
//   myList.add(1);
// }

myList.isPalindrome();
