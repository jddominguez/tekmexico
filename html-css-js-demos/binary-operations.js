var Node = function(data){

  this.left = null;
  this.right = null;
  this.data = data;

    this.insert = function(data){
    if(this.data){
      if(data < this.data){
          if(this.left == null)
            this.left = new Node(data);
          else
            this.left.insert(data);
      }
      if(data > this.data){
          if(this.right == null)
            this.right = new Node(data);
          else
            this.right.insert(data);
      }
    }
    else{
      this.data = data;
    }
  };


  this.find = function(data){
    if(data < this.data){
      if(this.left == null)
        return null;
      return this.left.find(data);
    }
  };

}


var root = new Node(8);

root.insert(5);
root.insert(1);
root.insert(0);
root.insert(6);


console.log(root.left);
