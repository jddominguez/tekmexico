function linkedList(){

  this._length = 0;
  this.head = null;

  function Node(data){
    this.data = data;
    this.next = null;
  }

  this.add = function(val){
    var node = new Node(val), currentNode = this.head;
    if(currentNode === null){
      this.head = node;
      this._length++;
      return node;
    }
    while(currentNode.next)
      currentNode = currentNode.next;
    currentNode.next = node;
    this._length++;
    return node;
  };

  this.reverse = function(){
    var prev = null;
    var current = this.head;
    var next = null;
    while(current != null){
      next = current.next;
      current.next = prev;
      prev = current;
      current = next;
    }
    node = prev;
    return node;
  };

  this.getNode = function(pos){
    var currentNode = this.head, length = this._length, count = 1;
    if(length === 0 || pos < 1 || pos > length)
      return false;
    while(count < pos){
      currentNode = currentNode.next;
      count++;
    }
    return currentNode;
  };

  this.printList = function(){
    for (var i = 1; i <= this._length; i++) {
      console.log(this.getNode(i).data);
      //console.log(this.getNode(i).data);
    }
  };


}

var myList = new linkedList();

myList.add(1);
myList.add(2);
myList.add(3);
myList.add(4);
myList.add(5);

//console.log(myList.head);

//myList.printList();

//console.log(myList.reverse());

myList.reverse();

//myList.printList();


//myList.printList();


// myList.printList();
//
// console.log("------");
//
// myList.reverseList();
