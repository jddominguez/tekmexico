var adder = function (x) {
    return function (y) {
        return x + y;
    };
};
add5 = adder(5);

console.log(add5(1));
