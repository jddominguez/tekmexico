//      _______      __
//     / ____(_)____/ /_
//    / /_  / / ___/ __ \
//   / __/ / (__  ) / / /
//  /_/   /_/____/_/ /_/
//

//0 = no fish, 1 = small fish, 2 = big fish
var fr = [2,1,1,0,0,0]; //fish going right
var fl = [0,0,0,1,2,1]; //fish going left

function fish(fr, fl){
  var x = 0;
  var y = 0;
  for (var i = 0; i < fr.length; i++)
    if(fr[i] !== 0)
      x++;
  for (var j = 0; j < fl.length; j++)
    if(fl[j] !== 0)
      x++;
  for (var n = 0; n < fr.length; n++){
    for (var m = 0; m < fl.length; m++){
      if (fr[n] > 2 && fl[m] == 1){
        y++;
		    fl[m] = 0;
      }
      if (fr[n] == 1 && fl[m] == 2){
		      y++;
		      fr[n] = 0;
      }
    }
  }
  return x-y;
}
console.log("Lucky fish = " + fish(fr,fl));
