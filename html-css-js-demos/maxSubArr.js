//Kadane's algorithm https://en.wikipedia.org/wiki/Maximum_subarray_problem
function maxSubArr(list){
  console.log(list);
  var maxEnd = 0, maxVal = 0, pos = [];
  for (var i = 0, j = 0; i < list.length; i++) {
    maxEnd = Math.max.apply(Math, [0, maxEnd + list[i]]);
    console.log(i + "=(" + (maxVal) + ", " + (maxEnd) + ") sum=" + (maxEnd+list[i]));
    // if(true){
    //   pos[j] = i+1;
    //   j++;
    // }

    maxVal = Math.max.apply(Math, [maxVal, maxEnd]);



  }
  //console.log("Enter day: " + pos[0] + ", exit day: " + (pos[pos.length-1]+0));
  return "Max: " + maxVal;
}

function genSeq(size, min, max){
  for (var i = 0, list = []; i < size; i++)
    list[i] =  Math.floor(Math.random() * (max - min + 1)) + min;
  return list;
}

//console.log(maxSubArr(genSeq(10, -100, 100)));
console.log((maxSubArr([-87, 12, -99, -35, -49, 79, -12, 23, -65, 100 ])));
//console.log(maxSubArr([-1, -2, 3, 4, -5, 6]));

//console.log(maxSubArr([5, -1, -1, -1, 5]));
