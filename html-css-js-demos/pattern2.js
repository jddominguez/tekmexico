var isIsomorphic = function(s, t) {
  var sA = s.split("");
  var tA = t.split("");
  var hashMap = [], hashMap2 = [];
  for( var i = 0, j = sA.length; i < j; i++ ){
    var sAPop = sA.pop();
    var tAPop = tA.pop();
    if( hashMap[sAPop] === undefined ){
        hashMap[sAPop] = tAPop;
    }else{
        if( hashMap[sAPop] !== tAPop ) return false;
    }
    if( hashMap2[tAPop] === undefined){
        hashMap2[tAPop] = sAPop;
    }else{
       if( hashMap2[tAPop] !== sAPop ) return false;
    }
  }
  return true;
};

console.log(isIsomorphic("ABBA", "ABBC"));
