"use strict";
class LinkedList {
  constructor(head) {
    this.head = null;
  }

  isPalindrome() {
    var stack = [this.head.value];
    var current = this.head;
    var middle = this.head;
    var length = 0;
    while (current) {
      current = current.next;
      if (current) {
        current = current.next;
        middle = middle.next;
        stack.unshift(middle.value);
        length++;
      }
      length++;
    }
    if (length <= 2 && middle.value == this.head.value) {
      return true;
    } else if (length == 2) {
      return false;
    } else {
      if (length % 2 === 0 && middle.value != stack.shift()) {
        return false;
      }
      while (middle) {
        if (middle.value != stack.shift()) {
          return false;
        }
        middle = middle.next;
      }
      return true;
    }
  }

  addNode(node) {
    if (!this.head) this.head = node;
    else {
      var current = this.head;
      var next = current.next;
      while (next) {
        current = next;
        next = current.next;
      }
      current.next = node;
    }
  }

  traverse() {
    var current = this.head;
    while (current) {
      current = current.next;
    }
  }
}

class Node {
  constructor(value) {
    this.next = null;
    this.value = value;
  }
}

var list = new LinkedList();

for (var i = 0; i < 100000; i++) {
  list.addNode(new Node(1));
}

console.log(list.isPalindrome());
