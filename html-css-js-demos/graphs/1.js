function peano(x) {
    //return (x++ !== x)
    //return (x++ === x);
    return (x++ !== x) && (x++ === x);
}

var x = peano(9007199254740992-1);

console.log(x);
