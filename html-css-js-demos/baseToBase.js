function baseConvert(n, b){
  if(n < 0 || b < 2 || b > 20)
    return false;
  if(n === 0)
    return 0;
  var list = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J'];
  var num = [];
  while(n !== 0){
    console.log(n%b);
    num = list[Math.floor(n % b)] + num;
    console.log(num);
    n = Math.floor(n / b);
    console.log(n);
  }
  return num;
}

for (var i = 14; i <= 14; i++) {
  console.log("base("+i+")="+baseConvert(2016, i));
}
