function LinkedList(){
  var Node = function(letter){
    this.letter= letter;
    this.next=null;
  };
  var head= null;
  var tail=null;
  this.add =function(letter){
    if (!head){
      head=new Node(letter);
      return head;
    }
    else{
      var node = new Node(letter);
      var current=head;
      while(current.next) {
        current= current.next;
      }
      current.next=node;
      tail=node;
    }
  };
  this.printList = function(){
    var string = "";
    var current =head;
    while(current){
      string+=current.letter;
      current=current.next;
    }
    console.log(string);
    return string;
  }
  function isPalindrome(current,match){
    var first=current;
    var last=match;
    if(!first.next) return true;
    if(first.letter!=last.letter) return false;
    else if(first.next==last || first.next.next==last) return true;
    else{
      while(first.next!=last){
        first=first.next;
      }
      return isPalindrome(current.next,first);
    }
  }
  this.check=function(){
    console.log( isPalindrome(head,tail) );
  }
}
var myList=new LinkedList();

for (var i = 0; i < 10; i++) {
  myList.add(1);
}

myList.check();
myList.printList();
