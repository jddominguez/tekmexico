/* LINKED LIST CODE */
function LinkedList(root) {
    this.head = new Node();
    this.pointer = this.head;
    this.addNode(root);
}

function Node(data) {
    this.data = data;
    this.next = null;
}

LinkedList.prototype.hasNext = function () {
    return this.pointer.next ? true : false;
};

LinkedList.prototype.traverse = function (callback) {
    this.pointer = this.head;
    while (this.hasNext()) {
        this.pointer = this.pointer.next;
        callback(this.pointer);
    }
};

LinkedList.prototype.addNode = function (node) {
    this.pointer = this.head;
    while (this.hasNext()) {
        this.pointer = this.pointer.next;
    }
    this.pointer.next = node;
};

/* PALINDROME CODE */
LinkedList.prototype.isPalindrome = function () {
    this.pointer = this.head;
    var word = [];
    var isit = true;
    function check(pointer) {
        if (pointer.next) {
            word.push(pointer.next.data);
            //console.log(word);
            check(pointer.next);
        }
        if (pointer.data != word.shift()) {
            isit = false;
        }
    }

    check(this.pointer);
    return isit;
};

/* TEST CASE */
var llist = new LinkedList();

for (var i = 0; i < 10; i++) {
  llist.addNode(new Node(1));
}

console.log(llist.isPalindrome());
