//     ______
//    / ____/___ ___________
//   / /   / __ `/ ___/ ___/
//  / /___/ /_/ / /  (__  )
//  \____/\__,_/_/  /____/
//

//0 = no car, 1 = car
var cr = [1,0,1,0,0,0]; //cars going right
var cl = [0,0,0,1,0,1]; //cars going left
function cars(cr, cl){
  var x = 0;
  for (var i = 0; i < cr.length; i++)
    for (var j = 0; j < cl.length; j++)
      if ( cr[i] == cl[j] && cr[i] == 1 && i < j)
        x++;
  return x;
}
console.log("Car collisions = " + cars(cr,cl));
