function Car(brand, year){
  this.brand  = brand;
  this.year = year;
}

Car.prototype.color = null;

var taxi = new Car("Nissan", 2001);
var police = new Car("Ford", 2010);



taxi.doors = 4;

police.doors = 4;


console.log(taxi);
console.log(police);
