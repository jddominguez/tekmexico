function *getCombinations(n, m){
  if(m == undefined){
    m = [];
    console.log(m);
  }
  mSum = m.reduce(function(pv, cv) { return pv + cv; }, 0);
  if(mSum == n){
    console.log(m);
    yield m;
  }
  else{
    var lastN = m[-1] ? m : 1;
    for (var i in _.range(lastN, n-1)) {
      if(mSum + i <= a){
        eSum = m + [i];
        for (var c in getCombinations(n, eSum))
          yield c;
      }
    }
  }
  return false;
}



console.log(getCombinations(5));

var x = [1, 9, 2, [8, 3, [7], 4], 6, [5], [5,[6,[4, 7,[3]]]]];

//generator function
function *flatten(l){
  if (Array.isArray(l))
    for (var e of l)
      yield *flatten(e);
  else
      yield l;
}
result = Array.from(flatten(x))
console.log(result);
