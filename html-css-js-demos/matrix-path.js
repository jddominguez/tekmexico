function nPaths(x1, y1, x2, y2){

  n = x2 - x1;
  m = y2 - y1;

  var matrix = new Array();

  for (var i = 0; i < n; i++) {
    matrix[i] = new Array();
    for (var j = 0; j < m; j++) {
      matrix[i][j]=0;
    }
  }

  var paths = new Array();

  for (var i = 0; i < n; i++) {
    paths[i] = new Array();
    for (var j = 0; j < m; j++) {
      paths[i][j]=0;
    }
  }

  for (var i = 0; i < matrix.length; i++)
    matrix[i][0] = 1;
  for (var j = 0; j < matrix.length; j++)
    matrix[0][j] = 1;
  for (var i = 1; i < n; i++) {
    for (var j = 1; j < m; j++)
      matrix[i][j] = matrix[i-1][j] + matrix[i][j-1];
  }

  console.log("Number of paths = "+matrix[n-1][m-1]);
}

function printPaths(paths, i, j, m, n, matrix){

  var result = paths + " >" + matrix[i][j];
  if(i == m-1 && j == n-1)
    console.log(result);
  if(j+1 < n)
    printPaths(result, i, j + 1, m, n, matrix);
  if(i + 1 < m)
    printPaths(result, i + 1, j, m, n, matrix)
}

var matrix = new Array();

matrix = [["A","B","C"],["D","E","F"],["G","H","I"]];
console.log(matrix[0]);
console.log(matrix[1]);
console.log(matrix[2]);

var paths = new Array();

nPaths(0,0,3,3);
printPaths(paths, 0, 0, 3, 3, matrix);
