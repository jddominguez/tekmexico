//Invocation as a function, global context, window object.
function myFunction(){
  console.log("My function.");
}
myFunction();

var anotherFunction = function(){
  console.log("Another function.");
}
anotherFunction();


//Invocation as a method, the object becomes the function context, available via the "this" parameter
var coolFunction = {}

coolFunction.jump = function(){
  console.log("This function jumps.");
}
coolFunction.jump();

function vehicle(){
  console.log("Vehicle!");
}

var car = vehicle;
car();

var truck = {
  type: vehicle
};

truck.type();

//Invocation as a contructor, we use the "new" keyword.

function Animal(){
  this.run = function(){
    console.log(this);
  };
}

var lion = new Animal();
var tiger = new Animal();

lion.run();
tiger.run();


//Invocation using the call and apply methods.
var tvShow = {};

function characters(){
  this.x = arguments;
}

characters.call(tvShow, "Lisa", "Bart");
//characters.apply(tvShow, ["Homer", "Marge"]);

console.log(tvShow.x);



//Arrow functions
