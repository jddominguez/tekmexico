var operations = {


  insert: function() {
    var n=0;
    for (var i = 0; i < arguments.length; i++) {
      n=arguments[i]+n;
    }
    return n;
  },
  multi: function() {
    var n=1;
    for (var i = 0; i < arguments.length; i++) {
      n=arguments[i]*n;
    }
    return n;
  },
  fact: function(x) {
    var n=1;
    for (var i = 1; i <= x; i++) {
      n=n*i;
    }
    return n;
  }
};

var x = operations.suma(1,2);
var y = operations.multi(1,1,0);
var z = operations.fact(8);

console.log(x);
console.log(y);
console.log(z);
