//Arabic to Romans
var romans = ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"];
var decimals = [1000,900,500,400,100,90,50,40,10,9,5,4,1];

function toRoman(dec){
  if(dec < 1 || dec > 5000)
    return false;
  else
    return decToRoman(dec, "", decimals, romans)
}

function decToRoman(num, s, dec, rom){
  if(dec[0])
    if(num < dec[0])
      return decToRoman(num, s, dec.slice(1), rom.slice(1));
    else
      return decToRoman(num - dec[0], s + rom[0], dec, rom);
  else
    return s;
}

console.log(toRoman(5));
