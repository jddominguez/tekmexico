var ul = document.createElement("ul");
ul.id = "results";
document.body.appendChild(ul);

function assert(value, desc) {
  var li = document.createElement("li");
  li.className = value ? "pass" : "fail";
  li.appendChild(document.createTextNode(desc));
  document.getElementById("results").appendChild(li);
}

function addMethod(object, name, fn){
  var old = object[name];
  object[name] = function(){
    if(fn.length == arguments.length)
      return fn.apply(this, arguments);
    else if(typeof old == "function")
      return old.apply(this, arguments);
  };
}

Function.prototype.partial = function(){
  var fn = this, args = Array.prototype.slice.call(arguments);

  return function(){
    var arg = 0;

    for (var i = 0; i < args.length && arg < arguments.length; i++) {
      if(args[i] === undefined ){
        args[i] = arguments[arg++];
      }
    }
    return fn.apply(this, args);
  };
};

Function.prototype.curry = function(){
  var fn = this, args = Array.prototype.slice.call(arguments);
  return function(){
    return fn.apply(this, args.concat(Array.prototype.slice.call(arguments)));
  };
};


function bind(context, name){
  return function(){
    return context[name].apply(context, arguments);
  };
}

function globalEval(data){
  data = data.replace(/^\s*|\s*$/g, "");

  if(data){
    var head = document.getElementsByTagName("head")[0] || document.documentElement,
    script = document.createElement("script");

    script.type = "text/javascript";
    script.text = data;

    head.appendChild(script);
    head.removeChild(script);
  }
}


if(document.addEventListener){
  this.addEvent = function(elem, type, fn){
    elem.addEventListener(type, fn, false);
    return fn;
  };
  this.removeEvent = function(elem, type, fn){
    elem.removeEventListener(type, fn, false);
  };
}
else if(document.attachEvent){
  this.addEvent = function(elem, type, fn){
    var bound = function(){
      return fn.apply(elem, arguments);
    };
    elem.attachEvent("on" + type, bound);
    return bound;
  };
  this.removeEvent = function(elem, type, fn){
      elem.detachEvent("on" + type, fn);
  };
}

function isEventSupported(eventName) {
  var element = document.createElement('div'),
  isSupported;
  eventName = 'on' + eventName;
  isSupported = (eventName in element);

  if (!isSupported) {
    element.setAttribute(eventName, 'return;');
    isSupported = typeof element[eventName] == 'function';
  }
  element = null;
  return isSupported;
}


function triggerEvent(elem, event) {
  var elemData = getData(elem), parent = elem.parentNode || elem.ownerDocument;
  if (typeof event === "string") {
    event = { type:event, target:elem };
  }
  event = fixEvent(event);

  if (elemData.dispatcher) {
    elemData.dispatcher.call(elem, event);
  }

  if (parent && !event.isPropagationStopped()) {
    triggerEvent(parent, event);
  }

  else if (!parent && !event.isDefaultPrevented()) {
    var targetData = getData(event.target);

    if (event.target[event.type]) {
      targetData.disabled = true;
      event.target[event.type]();

      targetData.disabled = false;
    }
  }
}


(function () {

  var cache = {},
      guidCounter = 1,
      expando = "data" + (new Date).getTime();

  this.getData = function (elem) {
    var guid = elem[expando];
    if (!guid) {
      guid = elem[expando] = guidCounter++;
      cache[guid] = {};
    }
    return cache[guid];
  };

  this.removeData = function (elem) {
    var guid = elem[expando];
    if (!guid) return;
    delete cache[guid];
    try {
      delete elem[expando];
    }
    catch (e) {
      if (elem.removeAttribute) {
        elem.removeAttribute(expando);
      }
    }
  };

})();

function fixEvent(event) {

  function returnTrue() { return true; }
  function returnFalse() { return false; }

  if (!event || !event.stopPropagation) {
    var old = event || window.event;

    // Clone the old object so that we can modify the values
    event = {};

    for (var prop in old) {
      event[prop] = old[prop];
    }

    // The event occurred on this element
    if (!event.target) {
      event.target = event.srcElement || document;
    }

    // Handle which other element the event is related to
    event.relatedTarget = event.fromElement === event.target ?
        event.toElement :
        event.fromElement;

    // Stop the default browser action
    event.preventDefault = function () {
      event.returnValue = false;
      event.isDefaultPrevented = returnTrue;
    };

    event.isDefaultPrevented = returnFalse;

    // Stop the event from bubbling
    event.stopPropagation = function () {
      event.cancelBubble = true;
      event.isPropagationStopped = returnTrue;
    };

    event.isPropagationStopped = returnFalse;

    // Stop the event from bubbling and executing other handlers
    event.stopImmediatePropagation = function () {
      this.isImmediatePropagationStopped = returnTrue;
      this.stopPropagation();
    };

    event.isImmediatePropagationStopped = returnFalse;

    // Handle mouse position
    if (event.clientX != null) {
      var doc = document.documentElement, body = document.body;

      event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
      event.pageY = event.clientY +
          (doc && doc.scrollTop || body && body.scrollTop || 0) -
          (doc && doc.clientTop || body && body.clientTop || 0);
    }

    // Handle key presses
    event.which = event.charCode || event.keyCode;

    // Fix button for mouse clicks:
    // 0 == left; 1 == middle; 2 == right
    if (event.button != null) {
      event.button = (event.button & 1 ? 0 :
          (event.button & 4 ? 1 :
              (event.button & 2 ? 2 : 0)));
    }
  }

  return event;

}


(function() {

  var nextGuid = 1;

  this.addEvent = function (elem, type, fn) {

    var data = getData(elem);                           //#1

    if (!data.handlers) data.handlers = {};             //#2

    if (!data.handlers[type])                           //#3
      data.handlers[type] = [];                         //#3

    if (!fn.guid) fn.guid = nextGuid++;                 //#4

    data.handlers[type].push(fn);                       //#5

    if (!data.dispatcher) {                             //#6
      data.disabled = false;
      data.dispatcher = function (event) {

        if (data.disabled) return;
        event = fixEvent(event);

        var handlers = data.handlers[event.type];       //#7
        if (handlers) {
          for (var n = 0; n < handlers.length; n++) {   //#7
            handlers[n].call(elem, event);              //#7
          }
        }
      };
    }

    if (data.handlers[type].length == 1) {              //#8
      if (document.addEventListener) {
        elem.addEventListener(type, data.dispatcher, false);
      }
      else if (document.attachEvent) {
        elem.attachEvent("on" + type, data.dispatcher);
      }
    }

  };

  function tidyUp(elem, type) {

    function isEmpty(object) {                          //#1
      for (var prop in object) {
        return false;
      }
      return true;
    }

    var data = getData(elem);

    if (data.handlers[type].length === 0) {             //#2

      delete data.handlers[type];

      if (document.removeEventListener) {
        elem.removeEventListener(type, data.dispatcher, false);
      }
      else if (document.detachEvent) {
        elem.detachEvent("on" + type, data.dispatcher);
      }
    }

    if (isEmpty(data.handlers)) {                        //#3 no types at all left?
      delete data.handlers;
      delete data.dispatcher;
    }

    if (isEmpty(data)) {                                 //#4 no handlers at all?
      removeData(elem);
    }
  }

  this.removeEvent = function (elem, type, fn) {       //#1 variable length argument list

    var data = getData(elem);                          //#2 fetch data

    if (!data.handlers) return;                        //#3 no handlers!

    var removeType = function(t){                      //#4 utility function
      data.handlers[t] = [];
      tidyUp(elem,t);
    };

    if (!type) {                                       //#5 remove all types
      for (var t in data.handlers) removeType(t);
      return;
    }

    var handlers = data.handlers[type];                 //#6 get handlers for type
    if (!handlers) return;

    if (!fn) {                                          //#7 remove all of type
      removeType(type);
      return;
    }

    if (fn.guid) {                                      //#8 remove one bound function?
      for (var n = 0; n < handlers.length; n++) {
        if (handlers[n].guid === fn.guid) {
          handlers.splice(n--, 1);
        }
      }
    }
    tidyUp(elem, type);

  };

  this.proxy = function (context, fn) {
    if (!fn.guid) {
      fn.guid = nextGuid++;
    }
    var ret = function () {
      return fn.apply(context, arguments);
    };
    ret.guid = fn.guid;
    return ret;
  };

})();


function isEventSupported(eventName) {

  var element = document.createElement('div'),          //#1
      isSupported;

  eventName = 'on' + eventName;                         //#2
  isSupported = (eventName in element);                 //#2

  if (!isSupported) {                                   //#3
    element.setAttribute(eventName, 'return;');
    isSupported = typeof element[eventName] == 'function';
  }

  element = null;                                        //#4

  return isSupported;
}

function getNodes(htmlString, doc) {

  var map = {
    "<td":[3,"<table><tbody><tr>","</tr></tbody></table>"],
    "<th":[3,"<table><tbody><tr>","</tr></tbody></table>"],
    "<tr":[2,"<table><thead>","</thead></table>"],
    "<option":[1,"<select multiple='multiple'>","</select>"],
    "<optgroup":[1,"<select multiple='multiple'>","</select>"],
    "<legend":[1,"<fieldset>","</fieldset>"],
    "<thead":[1,"<table>","</table>"],
    "<tbody":[1,"<table>","</table>"],
    "<tfoot":[1,"<table>","</table>"],
    "<colgroup":[1,"<table>","</table>"],
    "<caption":[1,"<table>","</table>"],
    "<col":[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],
    "<link":[3,"<div></div><div>","</div>"]
  };

  var tagName = htmlString.match(/<\w+/), mapEntry = tagName ? map[tagName[0]] : null;
  if (!mapEntry) mapEntry = [0, " ", " "];
  var div = (doc || document).createElement("div");

  div.innerHTML = mapEntry[1] + htmlString + mapEntry[2];
  while (mapEntry[0]--) div = div.lastChild;
  return div.childNodes;
}
